Installation
============
* Create a folder "SymptomMapper" in the uppermost directory of your tablet. Then create the subfolders "Config" and "Subjects" in it.
* Copy "config.json" into "SymptomMapper/Config"
* The final folder structure should look like this::

   SymptomMapper
     Config
        config.json
     Subjects

* Copy the APK to your tablet
* Install the APK
* Grant the app access to storage. This can be done under Settings/Apps/SymptomMapper Pro/Permissions. Make sure to have notifications enabled, too (should be by default).
* Depending on your tablet, you might need to adjust font size and screen resolution.


Password
========
* The app is password protected to avoid patients tampering with it.
* The password is: "1985"


Configuration
=============
* The app can be configured by editing the "config.json" file (the parser is located in "config/ModeManager.java")
* Currently, it containts random pain descriptors that you need to review and adapt to your needs.
* In theory, you can ask your patient for any bodily symptom or sensation.


Instructions
============
* You should prepare a page of drawing instructions to reduce variability.
* You can make one yourself from our German version (export to .png without transparency).
* Replace png-files in "res/drawable" and compile the app.


Where are the data saved?
=========================
* The participant's names and personal data are stored in a database that is not accessible.
* The app will store pseudonymised data in the "Subjects" folder.
* "patient.txt" contains information about the drawing background: 0-sexless, 1-male, 2-female, 3-head
* The different visits are stored in separate folders (n1, n2, etc.).
* "records.json" contains information about the drawing (which descriptors were chosen for each symptom). It can be either opened with a normal text editor or with a json-viewer (for better readability).
* The drawings are stored using the following notation: "s0_v0" (first symptom, front), "s0_v1" (first symptom, back), "s1_v0", etc.
* You can find the backgrounds on https://figshare.com/articles/Hannover_Body_Template/7637387


Recommended hardware
====================
* Samsung Galaxy Note 10.1 SM-P600 (not produced anymore)
* Samsung Galaxy Tab S3
* Other Android-based tablets might work as well, but they need to have an inductive stylus (not a capacitive, i.e. one that emulates your finger), which is something that not many tablets have.


What is the best way to analyse PNG files?
==========================================
* We use FSL for this purpose since the analyses are similar to those in fMRI analyses. FSL requires the data to be in nifti format.
* We provide a script "SM_png2nii.py" that converts png to nifti. The intensity will be encoded from 0-100. For the script to work, you need FSL and Python-3 with some libraries (numpy, image and nibabel). You can make it executable and execute it from the terminal. It will also display a "usage" message.

package com.mhh.sensationmapper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

/**
 * Created by sbrink on 08.07.16.
 */

public class DBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "SymptomMapper.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //context.deleteDatabase(DATABASE_NAME);      //TODO remove in final version
    }
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PATIENT_TABLE = "CREATE TABLE patients ( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "dob INTEGER, " +
                "sex INTEGER )";
        String CREATE_RECORDING_TABLE = "CREATE TABLE recordings ( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "num INTEGER, "+
                "date INTEGER, "+
                "author INTEGER, "+
                "pid INTEGER, "+
                "sensation_count INTEGER )";

        db.execSQL(CREATE_PATIENT_TABLE);
        db.execSQL(CREATE_RECORDING_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL("DROP TABLE IF EXISTS patients");
        db.execSQL("DROP TABLE IF EXISTS recordings");
        onCreate(db);
    }
    //public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //    onUpgrade(db, oldVersion, newVersion);
    //}
    public Patient getPatient(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("patients", null, "_id = ?", new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor == null) {
            return null;
        }

        cursor.moveToFirst();

        Patient patient = new Patient(cursor);

        cursor.close();
        db.close();

        return patient;
    }


    public long addPatient(Patient patient) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", patient.getName());
        values.put("dob", sqlFromDate(patient.getDob()));
        values.put("sex", patient.getSex());

        long pid = db.insert("patients", null, values);
        db.close();
        return pid;
    }

/*    public void deletePatient(Patient patient) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("patients", "_id = ?", new String[] { String.valueOf(patient.getId()) });
        db.close();
    }*/

    /*public ArrayList<Patient> getAllPatients() {
        ArrayList<Patient> patients = new ArrayList<Patient>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("patients", null, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Integer _id;
                _id = Integer.parseInt(cursor.getString(0));
                String name = cursor.getString(1);
                Date dob = new Date(Integer.parseInt(cursor.getString(2)));
                Patient patient = new Patient(_id, name, dob);
                patients.add(patient);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return patients;
    }*/

        public List<Recording> getAllRecordingsBy(int pid) {
        ArrayList<Recording> recordings = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("recordings", null, "pid = ?", new String[] { String.valueOf(pid) }, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Integer _id = cursor.getInt(0);
                Integer num = cursor.getInt(1);
                Date date = new Date(cursor.getLong(2));
                Integer author = cursor.getInt(3);
                Integer pid2 = cursor.getInt(4);
                Integer sensation_count = cursor.getInt(5);
                Recording recording = new Recording(_id, num, date, author, pid2, sensation_count);
                recordings.add(recording);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return recordings;
    }

    //public Entry getEntry(int id) {

    //}

    //public void deleteEntry(Entry entry) {

    //}

    //public List<Entry> getAllEntries() {

    //}

/*    public Recording getRecording(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("versions", null, "eid = ?", new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor == null) {
            return null;
        }

        cursor.moveToFirst();

        Recording recording = new Recording(cursor);

        cursor.close();
        db.close();

        return recording;
    }

    public Recording getRecording(int eid, int author) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("versions", null, "eid = ? AND author = ?",
                new String[] { String.valueOf(eid), String.valueOf(author) }, null, null, null, null);

        if (cursor == null) {
            return null;
        }

        cursor.moveToFirst();

        Recording recording = new Recording(cursor);

        cursor.close();
        db.close();

        return recording;
    }*/

    public long addRecording(Recording recording) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("num", Integer.toString(recording.getNum()));
        values.put("date", sqlFromDate(recording.getDate()));
        values.put("author", Integer.toString(recording.getAuthor()));
        values.put("pid", Long.toString(recording.getPatientID()));
        values.put("sensation_count", Integer.toString(recording.getSensationCount()));

        long id = db.insert("recordings", null, values);
        db.close();
        return id;
    }

    public Date parseDate(Context context, String str_date) {
        try {
            java.text.DateFormat df = android.text.format.DateFormat.getDateFormat(context);
            return df.parse(str_date);
        } catch (ParseException e) {
            return null;
        }
    }

    public String formatDate(Context context, Date date) {
        try {
            java.text.DateFormat df = android.text.format.DateFormat.getDateFormat(context);
            return  df.format(date);
        } catch (NullPointerException e) {
            return "";
        }
    }

    public Date sqlToDate(String str_ms) {
        try {
            long ms = Long.parseLong(str_ms);
            return new Date(ms);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public String sqlFromDate(Date date) {
        try {
            long ms = date.getTime();
            return Long.toString(ms);
        } catch (NullPointerException e) {
            return "NULL";
        }
    }

    public String formatDateTime(Context context, String str_ms) {
        long ms;
        try {
            ms = Long.parseLong(str_ms);
        } catch (NumberFormatException e) {
            return "";
        }
        // DATE
        String date_str;
        try {
            java.text.DateFormat df = android.text.format.DateFormat.getDateFormat(context);
            date_str =  df.format(new Date(ms));
        } catch (NullPointerException e) {
            date_str = "";
        }
        // TIME
        String time_str;
        try {
            time_str = DateUtils.formatDateTime(context, ms, DateUtils.FORMAT_SHOW_TIME);
        } catch (Exception e) {
            //TODO correct exception
            time_str = "";
        }
        return date_str + " " + time_str;
    }
}

/*
package com.mhh.sensationmapper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

import java.util.List;

abstract public class QuestionnaireActivity extends Activity {

    protected Patient patient;
    protected Recording recording;
    protected int num;
    ConfigManager c;
    ModeManager.Mode mode;

    protected int[] colorList;

    protected abstract int getLayoutId();
    protected abstract void setResources();
    protected abstract void prepareLayout();
    protected abstract void clearLayout();
    protected abstract Class getNextActivityClass();

    public List<String> getq1_answers() {
        return mode.getQ1_answers();
    }

    protected class GetNext {
        private int cid = -1;
        private int sid = -1;

        public int color(int[] colorList) {
            cid = (cid + 1 < colorList.length) ? cid + 1: 0;
            return colorList[cid];
        }
        public int sensationId() {
            return ++sid;
        }
    }

    protected GetNext getNext = new GetNext();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        num = 0;
        Log.d("QuestionnaireActivity", "onCreate");
        setContentView(getLayoutId());

        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");

        colorList = getResources().getIntArray(R.array.colors_list);


        setResources();

        prepareLayout();
        clearLayout();
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        int flag = intent.getIntExtra("draw_flag", -1);
        switch (flag) {
            case -1:
                break;
            case 42:
                // prepareLayoutoming back from DrawActivity
                num++;
                TextView txt = (TextView) findViewById(R.id.btn_start_drawing);
                txt.setText(getString(R.string.ContinueDrawing));
                clearLayout();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 23:
                // DrawActivity final finish
                finish();
                break;
        }
    }

    protected void startNextActivity (QuestionPatient sensation) {
        Intent intent = new Intent(getApplicationContext(), getNextActivityClass());

        intent.putExtra("sensation", sensation);
        if (num == 0) {
            intent.putExtra("patient", patient);
            intent.putExtra("recording", recording);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        startActivityForResult(intent, 0);

    }

}

*/

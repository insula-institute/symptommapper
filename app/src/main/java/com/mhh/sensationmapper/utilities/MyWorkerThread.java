package com.mhh.sensationmapper.utilities;

import android.os.HandlerThread;
import android.os.Handler;

/**
 * Created by Tawfik on 15/05/16.
 */
public class MyWorkerThread extends HandlerThread {

    private Handler mWorkerHandler;

    public MyWorkerThread(String name) {
        super(name);
    }

    public void postTask(Runnable task){
        mWorkerHandler.post(task);
    }

    public void prepareHandler(){
        mWorkerHandler = new Handler(getLooper());
    }
}
package com.mhh.sensationmapper.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by sbrink on 10.07.16.
 */
public class Recording implements Parcelable {
    private long _id;
    private int num;
    private Date date;
    private int author;
    private long pid;
    private int sensation_count;

    public Recording(){}

    public Recording(int num, Date date, int author, long pid, int sensation_count) {
        super();
        this.num = num;
        this.date = date;
        this.author = author;
        this.pid = pid;
        this.sensation_count = sensation_count;
    }

    public Recording(long _id, int num, Date date, int author, long pid, int sensation_count) {
        super();
        this._id = _id;
        this.num = num;
        this.date = date;
        this.author = author;
        this.pid = pid;
        this.sensation_count = sensation_count;
    }

    public Recording(Cursor cursor) {
        super();
        this._id = cursor.getLong(0);
        this.num = cursor.getInt(1);
        this.date = new Date(cursor.getLong(2));
        this.author = cursor.getInt(3);
        this.pid = cursor.getLong(4);
        this.sensation_count = cursor.getInt(5);
    }

    public long getId() {
        return _id;
    }
    public int getNum() {
        return num;
    }
    public Date getDate() {
        return date;
    }
    public int getAuthor() {
        return author;
    }
    public long getPatientID() {
        return pid;
    }
    public int getSensationCount() {
        return sensation_count;
    }

/*    @Override
    public String toString() {
        return "Recording [id=" + _id + ", num=" + num + " + ", date=" + date + ", author=" + author + ", pid=" + pid + ", sensation_count =" + sensation_count
                + "]";
    }*/

    /* everything below here is for implementing Parcelable */

    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(_id);
        out.writeInt(num);
        out.writeLong(date.getTime());
        out.writeInt(author);
        out.writeLong(pid);
        out.writeInt(sensation_count);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Recording> CREATOR = new Parcelable.Creator<Recording>() {
        public Recording createFromParcel(Parcel in) {
            return new Recording(in);
        }

        public Recording[] newArray(int size) {
            return new Recording[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Recording(Parcel in) {
        _id = in.readLong();
        num = in.readInt();
        date = new Date(in.readLong());
        author = in.readInt();
        pid = in.readLong();
        sensation_count = in.readInt();
    }
}

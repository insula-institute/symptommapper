package com.mhh.sensationmapper.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by sbrink on 08.07.16.
 */

public class Patient implements Parcelable{
    private long _id;
    private String name;
    private Date dob;
    private int sex;

    public Patient(){}

    public Patient(String name, Date dob, int sex) {
        super();
        this.name = name;
        this.dob = dob;
        this.sex = sex;
    }

    public Patient(long id, String name, Date dob, int sex) {
        super();
        this._id = id;
        this.name = name;
        this.dob = dob;
        this.sex = sex;
    }

    public Patient(Cursor cursor) {
        this._id = cursor.getLong(0);
        this.name = cursor.getString(1);
        this.dob = new Date(cursor.getLong(2));
        this.sex = cursor.getInt(3);
    }

    public long getId() {
        return _id;
    }
    public String getName() {
        return name;
    }
    public Date getDob() {
        return dob;
    }
    public int getSex() {
        return sex;
    }

/*
    @Override
    public String toString() {
        return "Patient [id=" + _id + ", name=" + name + ", date=" + dob + ", sex=" + sex
                + "]";
    }
*/

    /* everything below here is for implementing Parcelable */

    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(_id);
        out.writeLong(dob.getTime());
        out.writeString(name);
        out.writeInt(sex);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Patient> CREATOR = new Parcelable.Creator<Patient>() {
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private Patient(Parcel in) {
        _id = in.readLong();
        dob = new Date(in.readLong());
        name = in.readString();
        sex = in.readInt();
    }

}

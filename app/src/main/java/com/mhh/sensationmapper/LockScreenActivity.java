package com.mhh.sensationmapper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by sbrink on 10.02.17.
 */
//@Lennard: Minor priority: Add a possibility to change the password.
public class LockScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen);
        setTitle(R.string.app_name);

        final EditText pw = (EditText) findViewById(R.id.txtLockScreenPassword);
        final Button b = (Button) findViewById(R.id.txtLockScreenPasswordOk);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pw.getText().toString().equals("1985")) {
                    pw.setText("");
                    startActivity(new Intent(LockScreenActivity.this, MainActivity.class));

                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        // do nothing
    }
}

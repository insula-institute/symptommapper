package com.mhh.sensationmapper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.FloatRange;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.SensationManager;
import com.mhh.sensationmapper.utilities.MyWorkerThread;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import java.util.List;


public class BodyDrawingView extends View {

    //
    // STEPS
    //

    private List<Step> steps = new ArrayList<>();

    private class Step {

        Brush brush;
        Path path;
        PointF point;
        int intensity;

        Step() {
            steps.add(this);
        }

    }

    //
    // DRAWING RELATED VARIABLES
    //

    Paint maskPaint = new Paint();

    //
    // PAN AND ZOOM
    //

    private GestureDetector mGestureDetector;
    private ScaleGestureDetector mScaleDetector;

    private Point mBGSizeZoomed;
    private Rect mBGRect = new Rect(), mBGZoomedRect;
    private Matrix mZoomingMatrix, mInvertMatrix;
    private float mZoomingScale = 1.0f, minZoomingScale = 1.0f, maxZoomingScale = 7.0f;

    //
    // STORAGE/THREAD RELATED VARIABLES
    //

    //private MyWorkerThread mStorageThread;

    //
    // STATIC INTENSITY WEIGHTED COLOR METHOD
    //

    private static int getIntensityWeightedColor(int intColor, int intensity) {
        float[] hsvColor = new float[3];
        Color.RGBToHSV((intColor >> 16) & 0xFF, (intColor >> 8) & 0xFF, (intColor >> 0) & 0xFF, hsvColor);
        hsvColor[1] = 0.10f + (0.01f * intensity * 0.9f);
        return Color.HSVToColor(hsvColor);
    }

    //
    // CONSTRUCTORS
    //

    DrawActivity activity;

    public BodyDrawingView(Context context) {
        this(context, null);
    }

    public BodyDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);

        activity = (DrawActivity)getContext();
        noIntensitySelected = activity.getNoIntensitySelected();


/*        mStorageThread = new MyWorkerThread("mStorageThread");
        mStorageThread.start();
        mStorageThread.prepareHandler();*/

        mGestureDetector = new GestureDetector(context, new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                return false;
            }


            @Override
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                moveXY(-v, -v1);
                return true;
            }

            @Override
            public void onLongPress(MotionEvent motionEvent) {

            }

            @Override
            public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return false;
            }
        });

        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {}

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) { return true; }

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                float tmpZoomingScale = mZoomingScale * detector.getScaleFactor();
                //TODO  fix the real cause
                if( tmpZoomingScale >= minZoomingScale && tmpZoomingScale <= maxZoomingScale ) {
                    mZoomingMatrix.postScale(detector.getScaleFactor(), detector.getScaleFactor(),
                            detector.getFocusX(), detector.getFocusY());
                }
                setBGImageZoomingLocation();
                invalidate();
                return true;
            }
        });

        maskPaint.setAntiAlias(false);     // TODO care about edge antialiasing
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        //snapshotPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
/*
        float colorMatrix[] = {1, 0, 0, 0, 0,
                               0, 1, 0, 0, 0,
                               0, 0, 1, 0, 0,
                               0, 0, 0, 0, 255};
        maskPaint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
*/

    }

    //
    // ON_SIZE_CHANGED AND ON_DRAW
    //

    @Override
    protected void onSizeChanged(int w, int h, int old_w, int old_h) {

        if (isInEditMode()) {
            super.onSizeChanged(w, h, old_w, old_h);
            return;
        }

        if (w > 0 & h > 0) {
            setBGImageZooming();
            setZoomingBounds();
        }
    }

    private Bitmap freshSnapshot = null;
    private Paint snapshotPaint = new Paint();
    private Path freshPath = null;
    private Paint freshPaint = null;

    @Override
    protected void onDraw(Canvas canvas) {

        if (isInEditMode()) {
            super.onDraw(canvas);
            return;
        }


        /*
         *  Normal drawing
         */

        // draw background
        if (backgroundImage != null) {
            canvas.drawBitmap(backgroundImage, mBGRect, mBGZoomedRect, null);
        }
        // draw previous sensations snapshots
        if (snapshots != null) {
            for (int i=0; i < sensationIndex; i++) {
                Bitmap snapshot = snapshots.get(i);
                if (snapshot != null) {
                    canvas.drawBitmap(snapshot, mBGRect, mBGZoomedRect, snapshotPaint);
                }

            }
        }
        // draw freshSnapshot and path currently being drawn
/*        if (brush.type.equals("erase")) {
            if (drawImageCanvas != null && freshPath != null) {
                drawImageCanvas.drawPath(freshPath, freshPaint);
            }
            if (freshSnapshot != null) {
                canvas.drawBitmap(freshSnapshot, mBGRect, mBGZoomedRect, null);
            }
        } else {*/
            if (freshSnapshot != null) {
                canvas.drawBitmap(freshSnapshot, mBGRect, mBGZoomedRect, null);
            }
            if (freshPath != null) {
                canvas.drawPath(freshPath, freshPaint);
            }
        //}

    }

    //
    // after lifting the pen this method draws the step to the snapshot
    // and clips everything drawn outside the body away
    //

    Canvas drawImageCanvas;

    private void drawStep(Step step){


        if (freshSnapshot == null) {
            freshSnapshot = Bitmap.createBitmap(mBGRect.width(), mBGRect.height(), Bitmap.Config.ARGB_8888);
            freshSnapshot.setDensity(Bitmap.DENSITY_NONE);
        }

        drawImageCanvas = new Canvas(freshSnapshot);

        Paint paint = new Paint(step.brush.paint);
        paint.setStrokeWidth(step.brush.thickness);
        //paint.setAntiAlias(true);     // TODO antialias everything?

        if (step.brush.type.equals("erase")) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        } else {
            paint.setColor(getIntensityWeightedColor(color, intensity));
            paint.setXfermode(null);
        }

        if(step.path != null) {
            drawImageCanvas.drawPath(step.path, paint);
        }

        if(step.point != null) {
            drawImageCanvas.drawPoint(step.point.x, step.point.y, paint);
        }

        // TODO do not clip on multiple draws
        if (maskImage != null) {
            drawImageCanvas.drawBitmap(maskImage, 0, 0, maskPaint);
        }

        snapshots.put(sensationIndex, freshSnapshot);

    }

    //
    // PUBLIC INTERFACE
    //

    private Bitmap backgroundImage = null;

    public void setBGImage(Bitmap backgroundImage) {
        this.backgroundImage = backgroundImage;         // TODO remove this variable
        //freshSnapshot = backgroundImage.copy(Bitmap.Config.ARGB_8888, true);
        //freshSnapshot.setDensity(Bitmap.DENSITY_NONE);
    }

    private Bitmap maskImage = null;

    public void setMaskImage(Bitmap maskImage) {
        this.maskImage = maskImage;
    }

    private SparseArray<Bitmap> snapshots = null;

/*
    public Bitmap getSnapshot() {
        return freshSnapshot;
    }
*/

    public void setSnapshots(SparseArray<Bitmap> snapshots) {
        this.snapshots = snapshots;
    }

    private int sensationIndex;

    public void setSensationIndex(int index) {
        this.sensationIndex = index;
    }

    private int color = -1;

    public void setColor(int color) {
        this.color = color;
    }

    private int intensity = -1;

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    private Brush brush = null;

    public void setBrush(Brush brush) {
        this.brush = brush;
    }

    private String noIntensitySelected = "Please select an intensity first.";

    private String noDrawingOnOverviewMessage = "To start your drawing please add (or select) a sensation first";

    public void setNoDrawingOnOverviewMessage(String noDrawingOnOverviewMessage) {
        this.noDrawingOnOverviewMessage = noDrawingOnOverviewMessage;
    }

    public void undoLastStep() {
        if (steps.size() > 0) {
            steps.remove(steps.size() - 1);

            if (freshSnapshot != null) {
                freshSnapshot.recycle();
                freshSnapshot = null;
            }

            for (Step step : steps) {
                drawStep(step);
            }
            invalidate();
        }
    }

    private void setBGImageZooming() {

        int imgW = backgroundImage.getWidth();
        int imgH = backgroundImage.getHeight();
        RectF imgRect = new RectF(0,0, imgW, imgH);

        int viewW = this.getWidth();
        int viewH = this.getHeight();
        RectF viewRect = new RectF(0,0, viewW, viewH);

        imgRect.round(mBGRect);

        Matrix matrix = new Matrix();
        matrix.setRectToRect(imgRect, viewRect, Matrix.ScaleToFit.CENTER);
        mZoomingMatrix = matrix;

        setBGImageZoomingLocation();


        if(mBGSizeZoomed.x>0 & mBGSizeZoomed.y>0) {
            invalidate();
        }

    }


    private void setBGImageZoomingLocation(){

        Matrix matrix = mZoomingMatrix;
        mInvertMatrix = new Matrix();
        matrix.invert(mInvertMatrix);

        int imgW = backgroundImage.getWidth();
        int imgH = backgroundImage.getHeight();
        mZoomingScale = mZoomingMatrix.mapRadius(1f) - mZoomingMatrix.mapRadius(0f);

        mBGSizeZoomed = new Point((int)(imgW * mZoomingScale), (int)(imgH * mZoomingScale));

        RectF BGZoomedRect = new RectF();
        matrix.mapRect(BGZoomedRect, new RectF(mBGRect));
        mBGZoomedRect = new Rect();
        BGZoomedRect.round(mBGZoomedRect);

    }

    private void setZoomingBounds() {

        int imgW = backgroundImage.getWidth();
        int imgH = backgroundImage.getHeight();
        RectF imgRect = new RectF(0,0, imgW, imgH);

        int viewW = this.getWidth();
        int viewH = this.getHeight();
        RectF viewRect = new RectF(0,0, viewW, viewH);

        Matrix matrix = new Matrix();
        matrix.setRectToRect(imgRect, viewRect, Matrix.ScaleToFit.CENTER);

        minZoomingScale = matrix.mapRadius(1f) - matrix.mapRadius(0f);
        maxZoomingScale = 7 * minZoomingScale;
    }


/*    private void saveStep(final Step step){

        mStepsList.get(mCurrentBodyfigureView).add(step);

        //create image for the step and save it

*//*
        //check for edges
        if(step.brush.name.equals(mDrawActivity.getString(R.string.brush_region))) {
            Set<String> uniqueEdgesList = new HashSet<String>();

            RectF boundsRectF = new RectF();
            step.path.computeBounds(boundsRectF, true);

            Rect boundsRect = new Rect();
            boundsRectF.round(boundsRect);

            for (int pX = boundsRect.left; pX <= boundsRect.right; pX++) {
                for (int pY = boundsRect.top; pY <= boundsRect.bottom; pY++) {
                    if (drawBitmap.getPixel(pX, pY) != 0) {
                        //remove alpha pixel color
                        String RGBColor = Integer.toHexString(mEdgesMaskImage.getPixel(pX, pY));
                        if (RGBColor.length() > 6) {
                            RGBColor = RGBColor.substring(2);
                        }
                        uniqueEdgesList.add(RGBColor);
                    }
                }
            }
            if (uniqueEdgesList.size() > 0) {
                startLightingEdges(mDrawActivity.getEdgesImages(new ArrayList<String>(uniqueEdgesList)), 0);
            }
        }
*//*


        Bitmap newBitmap = getLastDisplayBitmap(mCurrentBodyfigureView);
        if (newBitmap == null) {
            newBitmap = Bitmap.createBitmap(mBGRect.width(), mBGRect.height(), Bitmap.Config.ARGB_8888);
            newBitmap.setDensity(Bitmap.DENSITY_NONE);
            setLastDisplayBitmap (newBitmap);
        }

        drawStep(step, newBitmap, mMaskImage);


        invalidate();

        final String path = String.format("%s/Drawing_Steps/Step%s/%d_%d.png", mDrawActivity.getRecordingPath(),
                step.sensation.getId(), mCurrentBodyfigureView, mStepsList.get(mCurrentBodyfigureView).indexOf(step));
        final Bitmap maskImage = mMaskImage;

        //save new images
        mStorageThread.postTask(new Runnable() {
            public void run() {

                final Bitmap drawBitmap;
                drawBitmap = Bitmap.createBitmap(mBGRect.width(), mBGRect.height(), Bitmap.Config.RGB_565);
                drawBitmap.setDensity(Bitmap.DENSITY_NONE);
                drawStep(step, drawBitmap, maskImage);

                String extStorage = Environment.getExternalStorageDirectory().toString();
                File file = new File(extStorage, path);

                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }

                FileOutputStream fOut;
                try {
                    fOut = new FileOutputStream(file);
                    drawBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                    drawBitmap.recycle();
                } catch (Exception e) {
                    Log.e("Exception", "Error writing step image: " + e.toString());
                }

            }
        });
    }*/

/*    public void saveSteps(){

        //save new images
        String s = mDrawActivity.getRecordingPath();
        final SensationManager.Question sensation = mDrawActivity.sensation;
        final Resources res = getResources();
        for (int i=0; i < mDrawActivity.getBodyfigureViewsSum(); i++) {
            final List<Step> steps = new ArrayList<>(mStepsList.get(i));
            final String path =  String.format("%s/Drawing_Steps_Merged/%d_%d.png", s, i, sensation.getId());
            final int resId = mDrawActivity.getMaskResourceId(i);
            final Rect rect = mDrawActivity.mBGRectList[i];
            mStorageThread.postTask(new Runnable() {
                public void run() {

                    if (steps.isEmpty()) {
                        sensation.addAreaRatio(0d);
                        sensation.addIntensityMean(null);
                        sensation.addIntensitySigma(null);
                        return;
                    }

                    // load maskImage
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    //options.inPreferredConfig = Bitmap.Config.ALPHA_8;
                    options.inScaled = false;

                    final Bitmap maskImage;
                    maskImage = BitmapFactory.decodeResource(res, resId, options);

                    // calculate area of mask
                    float base_area = 0;
                    for (int x = 0; x < maskImage.getWidth(); x++) {
                        for (int y = 0; y < maskImage.getHeight(); y++) {
                            int c = maskImage.getPixel(x, y);
                            if (c != Color.TRANSPARENT) {
                                base_area++;
                            }
                        }
                    }
                    float base_area_ratio = base_area / (maskImage.getHeight() * maskImage.getWidth());

                    // merge Steps
                    final Bitmap drawBitmap;
                    drawBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
                    drawBitmap.setDensity(Bitmap.DENSITY_NONE);
                    for (Step step : steps) {
                        drawStep(step, drawBitmap, maskImage);
                    }

                    // calculate stuff of image
                    float draw_area = 0;
                    float intensity = 0;
                    for (int x = 0; x < drawBitmap.getWidth(); x++) {
                        for (int y = 0; y < drawBitmap.getHeight(); y++) {
                            int c = drawBitmap.getPixel(x, y);
                            if (c != Color.TRANSPARENT) {
                                float[] hsv = new float[3];
                                Color.colorToHSV(c, hsv);
                                draw_area++;
                                intensity += (hsv[1] - 0.1f)/0.9f;
                            }
                        }
                    }
                    float draw_area_ratio = draw_area / (drawBitmap.getHeight() * drawBitmap.getWidth());
                    double area_ratio = draw_area/base_area;
                    double intensity_mean = intensity/draw_area;

                    // standard deviation
                    float intensity_sigma1 = 0;
                    for (int x = 0; x < drawBitmap.getWidth(); x++) {
                        for (int y = 0; y < drawBitmap.getHeight(); y++) {
                            int c = drawBitmap.getPixel(x, y);
                            if (c != Color.TRANSPARENT) {
                                float[] hsv = new float[3];
                                Color.colorToHSV(c, hsv);
                                intensity_sigma1 += Math.pow((intensity_mean - (hsv[1] - 0.1f)/0.9f), 2);
                            }
                        }
                    }
                    double intensity_sigma = Math.sqrt(intensity_sigma1/draw_area);

                    if (Double.isNaN(area_ratio)) {
                       area_ratio = 0f;
                    }
                    if (Double.isNaN(intensity_mean)) {
                        intensity_mean = 0f;
                    }
                    if (Double.isNaN(intensity_sigma)) {
                        intensity_sigma = 0f;
                    }
                    Log.e("addAreaRatio", String.valueOf(area_ratio));
                    Log.e("addIntensityMean", String.valueOf(intensity_mean));
                    Log.e("addIntensitySigma", String.valueOf(intensity_sigma));

                    // save final values to sensation object
                    sensation.addAreaRatio(area_ratio);
                    sensation.addIntensityMean(intensity_mean);
                    sensation.addIntensitySigma(intensity_sigma);

                    String extStorage = Environment.getExternalStorageDirectory().toString();
                    File file = new File(extStorage, path);

                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }

                    FileOutputStream fOut;
                    try {
                        fOut = new FileOutputStream(file);
                        drawBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                        fOut.flush();
                        fOut.close();
                        drawBitmap.recycle();
                    } catch (Exception e) {
                        Log.e("Exception", "Error writing combined step image: " + e.toString());
                    }

                }
            });
        }
    }*/



/*
    public void saveSensations(final long rid, final String path){
        final SensationMapper app = (SensationMapper) mDrawActivity.getApplicationContext();
        final ConfigManager c = mDrawActivity.c;

        mStorageThread.postTask(new Runnable() {
            public void run() {
                c.toFile(path, false);
                Log.d("records.json", "written");
                app.setRunning(rid, false);
            }
        });
        mStorageThread.quitSafely();
    }
*/



/*
    public void undo(){
        ArrayList<Step> stepList = mStepsList.get(mCurrentBodyfigureView);

        // only remove steps of current sensation
        if (stepList.size() > 0  && stepList.get(stepList.size() - 1).sensation == mDrawActivity.sensation) {

            // remove last step
            stepList.remove(stepList.size() - 1);
            Log.v("steplistsize", "is: " + stepList.size());

            // reset somethingDrawn
            if (stepList.size() == 0 || (stepList.size() > 0 && stepList.get(stepList.size() - 1).sensation != mDrawActivity.sensation)) {
                mDrawActivity.somethingDrawn = false;
            }

            // load the merged image of previous sensation
            Bitmap newBitmap = null;
            if (mDrawActivity.sensation.getId() > 0) {
                String extStorage = Environment.getExternalStorageDirectory().toString();

                File file = new File(extStorage, ((DrawActivityOld) getContext()).getRecordingPath() + "/Drawing_Merged/" +
                        mCurrentBodyfigureView + "_" +
                        String.valueOf(((DrawActivityOld) getContext()).sensation.getId() -1) + ".png");

                if(file.exists()) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = getLastDisplayBitmap(mCurrentBodyfigureView).getConfig();
                    try {
                        newBitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options)
                                .copy(Bitmap.Config.ARGB_8888, true);
                        newBitmap.setDensity(Bitmap.DENSITY_NONE);
                    } catch (FileNotFoundException e) {
                        Log.e("Exception", "Error loading merged image: " + e.toString());
                    }
                }
            }

            if (newBitmap == null) {
                newBitmap = Bitmap.createBitmap(mBGRect.width(), mBGRect.height(), Bitmap.Config.ARGB_8888);
                newBitmap.setDensity(Bitmap.DENSITY_NONE);
            }

            setLastDisplayBitmap(newBitmap);
            for (Step step : mStepsList.get(mCurrentBodyfigureView)) {
                drawStep(step, newBitmap, mMaskImage);
            }

        }

        invalidate();

    }
*/

    //
    // PAN AND ZOOM
    // TODO understand this
    //

    private void moveXY(float transX, float transY){
        float newX = mBGZoomedRect.left + transX;
        float newY = mBGZoomedRect.top + transY;

        boolean widthBiggerThanScreen = mBGZoomedRect.width()>getWidth();
        boolean heightBiggerThanScreen = mBGZoomedRect.height()>getHeight();

        boolean passLeftEdge = (newX > 0 && widthBiggerThanScreen) || (newX < 0 && !widthBiggerThanScreen);
        boolean passRightEdge = (newX + mBGZoomedRect.width() < getWidth() && widthBiggerThanScreen) ||
                (newX + mBGZoomedRect.width() > getWidth() && !widthBiggerThanScreen);

        boolean passTopEdge = (newY > 0 && heightBiggerThanScreen) || (newY < 0 && !heightBiggerThanScreen);
        boolean passBottomEdge = (newY + mBGZoomedRect.height() < getHeight() && heightBiggerThanScreen) ||
                (newY + mBGZoomedRect.height() > getHeight() && !heightBiggerThanScreen);

        if( passLeftEdge && passRightEdge ) {
            transX = 0;
        }else if( passLeftEdge ) {
            transX = -mBGZoomedRect.left;
        }else if ( passRightEdge ){
            transX = getWidth() - ( mBGZoomedRect.left + mBGZoomedRect.width() );
        }

        if( passTopEdge && passBottomEdge ) {
            transY = 0;
        }else if( passTopEdge ) {
            transY = -mBGZoomedRect.top;
        }else if ( passBottomEdge ){
            transY = getHeight() - ( mBGZoomedRect.top + mBGZoomedRect.height() );
        }

        mZoomingMatrix.postTranslate(transX, transY);
        setBGImageZoomingLocation();
        invalidate();
    }

    // EVENT HANDLER, handles zooming, panning, drawing

    Toast t;//, t2;

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //DrawActivity activity = (DrawActivity)getContext();
        //getNoIntensitySelected

        boolean isPen = activity.isSPenEnabled() && event.getToolType(0) == MotionEvent.TOOL_TYPE_STYLUS;

        if(!isPen){
            mScaleDetector.onTouchEvent(event);
            if(!mScaleDetector.isInProgress()) {
                mGestureDetector.onTouchEvent(event);
            }
            return true;
        }

/*        if (snapshots != null) {
            if (t2 == null) {
                Point screenSize = new Point();
                activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
                t2 = Toast.makeText(getContext(), noDrawingOnOverviewMessage, Toast.LENGTH_SHORT);
                t2.setGravity(Gravity.CENTER, 0, screenSize.y / 10);
                t2.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        t2 = null;
                    }
                }, 5000);
            }

            return true;
        }*/

        if (intensity < 0f) {
            if (t == null) {
                //Point screenSize = new Point();
                //activity.getWindowManager().getDefaultDisplay().getSize(screenSize);
                t = Toast.makeText(getContext(), noIntensitySelected, Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                TextView v = (TextView) t.getView().findViewById(android.R.id.message);
                if( v != null) v.setGravity(Gravity.CENTER);
                t.show();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        t = null;
                    }
                }, 3000);
            }

            return true;
        }

        // TODO does this ever happen?
        if(event.getAction() != MotionEvent.ACTION_DOWN &&
                event.getAction() != MotionEvent.ACTION_MOVE &&
                event.getAction() != MotionEvent.ACTION_UP){

            freshPath = null;
            // TODO freshPaint ?
            invalidate();

            return false;
        }


        float x = event.getX();
        float y = event.getY();

        if (brush.drawByMove) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                freshPath = new Path();
                freshPath.moveTo(x, y);
                freshPath.lineTo(x, y);

                freshPaint = new Paint(brush.paint);
                freshPaint.setStyle(Paint.Style.STROKE);
                freshPaint.setStrokeWidth(mZoomingScale * brush.thickness);
                freshPaint.setColor(brush.type.equals("erase") ? Color.WHITE : getIntensityWeightedColor(color, intensity));

            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                // get all points from event.getHistoricalX/Y for a smoother draw;
                int histPointsAmount = event.getHistorySize();
                for (int i = 0; i < histPointsAmount; i++) {
                    freshPath.lineTo(event.getHistoricalX(i), event.getHistoricalY(i));
                }
                freshPath.lineTo(x, y);

            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                freshPath.lineTo(x, y);

                //PathMeasure pm = new PathMeasure(freshPath, false);
                //if(pm.getLength()>1) {
                    //mPaint.setStyle(mBrush.getPaint().getStyle());

                    Step step = new Step();
                    step.intensity = intensity;
                    step.brush = brush;
                    step.path = new Path(freshPath);
                    freshPath = null;
                    step.path.transform(mInvertMatrix);

                    drawStep(step);
                //}

            }
        } else {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                float[] pts = new float[]{x, y};
                mInvertMatrix.mapPoints(pts);

                Step step = new Step();
                step.intensity = intensity;
                step.brush = brush;
                step.point = new PointF(pts[0], pts[1]);

                drawStep(step);
            }
        }

        invalidate();

        return true;
    }

}


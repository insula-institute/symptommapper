/*
package com.mhh.sensationmapper;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.transition.Scene;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.mhh.sensationmapper.comboseekbar.ComboSeekBar;
import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;
import com.mhh.sensationmapper.config.SensationManager;
import com.mhh.sensationmapper.utilities.XMLDOMParser;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pen.Spen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import eu.davidea.flipview.FlipView;

public class DrawActivityOld extends Activity {

    float dp2px(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    //region bodyfigure xml constants
    public static final String NODE_BODYFIGURE = "bodyfigure";
    public static final String NODE_BODYFIGURE_THUMBNAIL = "thumbnail";
    public static final String NODE_BODYFIGURE_IMAGE = "image";
    public static final String NODE_BODYFIGURE_MASK = "maskimage";
    public static final String NODE_BODYFIGURE_EDGEMASK = "maskedge";
    public static final String NODE_BODYFIGURE_REALWIDTH = "realwidth";
    public static final String NODE_BODYFIGURE_REALHEIGHT = "realheight";
    public static final String NODE_BODYFIGURE_PIXELWIDTH = "pixelwidth";
    public static final String NODE_BODYFIGURE_PIXELHEIGHT = "pixelheight";
    public static final String NODE_BODYFIGURE_PARTS = "parts";
    public static final String NODE_BODYFIGURE_PARTS_PART = "part";
    public static final String NODE_BODYFIGURE_PARTS_ID = "id";
    public static final String NODE_BODYFIGURE_PARTS_NAME = "name";
    public static final String NODE_BODYFIGURE_PARTS_IMAGE = "image";
    public static final String NODE_BODYFIGURE_PARTS_MASKCOLOR = "maskcolor";
    public static final String NODE_BODYFIGURE_PARTS_LEFT = "left";
    public static final String NODE_BODYFIGURE_PARTS_RIGHT = "right";
    public static final String NODE_BODYFIGURE_PARTS_TOP = "top";
    public static final String NODE_BODYFIGURE_PARTS_BOTTOM = "bottom";
    public static final String NODE_BODYFIGURE_EDGES_EDGE = "edge";
    public static final String NODE_BODYFIGURE_EDGES_IMAGE = "image";
    public static final String NODE_BODYFIGURE_EDGES_MASKCOLOR = "maskcolor";
    public static final String NODE_BODYFIGURE_VIEW = "view";
    */
/**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     *//*

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public enum AttrBrushArray {
        title,
        icon,
        fill_style,
        draw_by_move,
        thickness,
        type
    }

    private List<Brush> mBrushes;
    public int mSelectedBrush;
    public int eraserId;
    public int lastBrushId;

    public SensationManager.Question sensation;
    protected Patient patient;
    protected Recording recording;
    protected ModeManager.Mode mode;

    public ComboSeekBar intensityScale, depthScale;
    final ArrayList<ImageButton> toolsBtns = new ArrayList<>();
    public boolean choiceSelected = false, intensitySelected = false, depthSelected = false, somethingDrawn = false, viewSwitched = false;

    ValueAnimator choicesAnimation, intensityAnimation, depthAnimation, bodyAnimation;

    private int mCurrentBodyfigureView = 0;

    public Rect mBGRectList[];

    protected BodyDrawingView mDrawingView;

    private Button btnNextView, btnPreviousView;

    private Element mBodyfigureElement;

    public int mSelectedBodyfigure;

    public boolean isSpenFeatureEnabled = false;

    public int getmCurrentBodyfigureView() {
        return mCurrentBodyfigureView;
    }

    public void setmCurrentBodyfigureView(int mCurrentBodyfigureView) {
        this.mCurrentBodyfigureView = mCurrentBodyfigureView;
        //updateFigureViewButtons();
        mDrawingView.updateCurrentBodyfigureView();
        mDrawingView.setBGImageZooming(null);

    }

    protected String getPatientPath() {
        return String.format("SymptomMapper/Subjects/%d", patient.getId());
    }

    protected String getRecordingPath() {
        return String.format("%s/%d_%d", getPatientPath(), recording.getNum(), recording.getAuthor());
    }

    public ConfigManager c;

    private ViewGroup sceneRoot;
    private Scene choiceScene, intensityScene, drawScene;

    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");
        String modeName = intent.getStringExtra("mode");

        c = new ConfigManager("SymptomMapper/Config/config.json");
        mode = c.getModeManager().getMode(modeName);

        mSelectedBodyfigure = patient.getSex();

        // get brushes, sensations, bodyfigures from their xml resources
        getAppBrushes();
        getAppBodyfigures();
        mBGRectList = new Rect[getBodyfigureViewsSum()];

        setContentView(R.layout.fragment_draw_detail);
        setTitle(R.string.app_name);

        mDrawingView = (BodyDrawingView) findViewById(R.id.drawingView);
        mDrawingView.setVisibility(View.VISIBLE);

        sceneRoot = (ViewGroup) findViewById(R.id.drawContainer);

        createToolbar();
        createChoiceScene();

        // bodyFlipView
        final FlipView bodyFlipView = (FlipView) findViewById(R.id.bodyFlipView);
        bodyFlipView.setFrontImage(getBGResourceId(1));
        bodyFlipView.setRearImage(getBGResourceId(0));

        bodyFlipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBGRectList[mCurrentBodyfigureView] = mDrawingView.getBGRect();
                bodyFlipView.flipSilently((getmCurrentBodyfigureView() + 1)%2);
                setmCurrentBodyfigureView((getmCurrentBodyfigureView() + 1)%2);
                viewSwitched = true;
            }
        });

        intensityScale = (ComboSeekBar) findViewById(R.id.intensityScale);

        // init everything

        TextView t;
        t = (TextView) findViewById(R.id.txtIntensityScaleMin);
        t.setText(mode.getIntensityScaleMin());
        t = (TextView) findViewById(R.id.txtIntensityScaleMax);
        t.setText(mode.getIntensityScaleMax());

        setmCurrentBodyfigureView(0);

        mSelectedBrush = 0;
        toolsBtns.get(0).setPressed(true);
        updateDrawingBrush();

        // automatically create first sensation
        newSensationInit();

        //s-pen
        Spen spenPackage = new Spen();
        try {
            spenPackage.initialize(this);
            isSpenFeatureEnabled = spenPackage.isFeatureEnabled(Spen.DEVICE_PEN);
        } catch (SsdkUnsupportedException e1) {
            Toast.makeText(this, R.string.no_spen, Toast.LENGTH_SHORT).show();
            e1.printStackTrace();
            finish();
        } catch (Exception e2) {
            Toast.makeText(this, R.string.spen_error, Toast.LENGTH_SHORT).show();
            e2.printStackTrace();
            finish();
        }
    }

    private void createToolbar() {
        LinearLayout toolContainer = (LinearLayout) findViewById(R.id.toolsContainer);

        int marginSmall = Math.round(dp2px(4));
        int marginLarge = Math.round(dp2px(24));
        int width = Math.round(dp2px(80));
        int height = Math.round(dp2px(102));

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(width, height);
        lp1.setMargins(marginSmall, 0, marginSmall, 0);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(width, height);
        lp2.setMargins(marginLarge, 0, marginSmall, 0);

        // tool buttons
        View.OnTouchListener keepSelectedListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                for (ImageButton ib : toolsBtns) {
                    if (v.getId() == ib.getId()) {
                        ib.setPressed(true);
                    } else {
                        ib.setPressed(false);
                    }
                    if (v.getId() != eraserId) {
                        lastBrushId = v.getId();
                    }
                    mSelectedBrush = v.getId();
                    updateDrawingBrush();
                }
                return true;
            }
        };

        for (Brush brush : mBrushes) {
            ImageButton btn = new ImageButton(this);
            btn.setBackground(getResources().getDrawable(R.drawable.listitem_selector));
            btn.setImageDrawable(brush.icon);
            btn.setCropToPadding(false);
            btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
            btn.setId(mBrushes.indexOf(brush));
            //btn.setPadding(padding1, 0, padding1, 0);
            btn.setOnTouchListener(keepSelectedListener);
            toolContainer.addView(btn, lp1);
            toolsBtns.add(btn);
            if (brush.name.equals(getString(R.string.brush_erase))) {
                eraserId = mBrushes.indexOf(brush);
            }
        }

        // undo button
        ImageButton btnUndo = new ImageButton(this);
        btnUndo.setBackground(getResources().getDrawable(R.drawable.listitem_selector));
        btnUndo.setImageDrawable(getResources().getDrawable(R.drawable.icon_undo));
        btnUndo.setCropToPadding(false);
        btnUndo.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btnUndo.setId(mBrushes.size());
        //btnUndo.setPadding(padding2, 0, padding2, 0);
        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawingView.undo();
            }
        });
        toolContainer.addView(btnUndo, lp2);

        // done button
        ImageButton btnDone = new ImageButton(this);
        btnDone.setBackground(getResources().getDrawable(R.drawable.listitem_selector));
        btnDone.setImageDrawable(getResources().getDrawable(R.drawable.icon_done));
        btnDone.setCropToPadding(false);
        btnDone.setScaleType(ImageView.ScaleType.FIT_CENTER);
        btnDone.setId(mBrushes.size() + 1);
        //btnDone.setPadding(padding2, 0, padding1, 0);       // padding1 to counteract first buttons left padding
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDoneClicked();
            }
        });
        toolContainer.addView(btnDone, lp2);
    }

    HashMap<String, Button> choiceButtonMap = new HashMap<>();

    private void createChoiceScene() {

        FrameLayout choiceSceneRoot = new FrameLayout(this);
        choiceSceneRoot.setBackgroundColor(Color.argb(128, 0, 0, 0));

        // container
        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);
        FrameLayout.LayoutParams flp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        flp.gravity = Gravity.CENTER;
        choiceSceneRoot.addView(container, flp);

        // choices grid
        GridLayout choicesGrid = new GridLayout(this);
        choicesGrid.setColumnCount(4);
        container.addView(choicesGrid);

        // fill grid with choices
        List<String> choices = mode.getChoices();
        ColorStateList csl = new ColorStateList(new int[][] {new int[] { android.R.attr.state_pressed}}, new int[] {sensation.getColor()});
        for (String choice : choices) {
            FrameLayout f = new FrameLayout(this);
            ToggleButton b = new ToggleButton(this);
            b.setBackground(getDrawable(R.drawable.custom_radio));
            choiceButtonMap.put(choice, b);
            //b.setId(choices.indexOf(choice));
            b.setTextOn(choice);
            b.setTextOff(choice);
            b.setBackgroundTintList(csl);
            //b.setPadding(padding, padding, padding, padding);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToggleButton b = new ToggleButton(this);
                    b.setBackground(getDrawable(R.drawable.custom_radio));
                    choiceButtonMap.put(choice, b);
                    //b.setId(choices.indexOf(choice));
                    b.setTextOn(choice);
                    b.setTextOff(choice);
                    b.setBackgroundTintList(csl);

                }
            });
            f.addView(b);
            choicesGrid.addView(f);
        }

        // done button
        Button btnDone = new Button(this);
        btnDone.setText(mode.getChoicesSceneDoneButton());
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChoicesSceneDone();
            }
        });
        container.addView(btnDone);

        choiceScene = new Scene(sceneRoot, (View)choiceSceneRoot);
    }

    private void onChoicesSceneDone() {
        createChoiceBar();
        //start animation
    }

    private void createChoiceBar() {


        // left choice buttons
        int padding = Math.round(dp2px(8));
        int margin = Math.round(dp2px(4));

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, margin, 0, margin);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.setMargins(margin, margin, margin, margin);

        LinearLayout sensationBar = (LinearLayout) findViewById(R.id.sensationBar);
        sensationBar.setPadding(padding, padding, padding, padding);
        choicesContainer = (LinearLayout) findViewById(R.id.choicesContainer);

        // create new sensation button
        Button btnNewSensation = new Button(this);
        btnNewSensation.setText(mode.getAddSymptomButton());
        btnNewSensation.setPadding(padding, padding, padding, padding);
        btnNewSensation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNewSensationClicked();
            }
        });
        sensationBar.addView(btnNewSensation, 0, lp1);

        // create choice buttons

        View.OnClickListener choiceClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToggleButton b = (ToggleButton)v;
                sensation.toggleChoice(mode.getChoices().get(b.getId()));
                choiceSelected = sensation.getChoices().size() > 0;
            }
        };

        List<String> choices = mode.getChoices();
        for (String choice : choices) {
            ToggleButton b = new ToggleButton(this);
            b.setBackground(getResources().getDrawable(R.drawable.custom_radio));
            b.setId(choices.indexOf(choice));
            b.setTextOn(choice);
            b.setTextOff(choice);
            b.setPadding(padding, padding, padding, padding);
            b.setOnClickListener(choiceClickListener);
            choicesContainer.addView(b, lp2);
        }


    }

    private void onDoneClicked() {

        if (somethingDrawn && !canFinishSensationDrawing(true)) {
            return;
        }

        String confirm, positive, negative;

        if (mode.getSensationManager().getSensations().size() == 1 && !somethingDrawn) {
            confirm = mode.getConfirmNothingDrawn();
            positive = mode.getConfirmNothingDrawnPositive();
            negative = mode.getConfirmNothingDrawnNegative();
        } else if (!viewSwitched) {
            confirm = mode.getConfirmNoViewSwitch();
            positive = mode.getConfirmNoViewSwitchPositive();
            negative = mode.getConfirmNoViewSwitchNegative();
        } else if (mode.getSensationManager().getSensations().size() == 1) {
            confirm = mode.getConfirmOnlyOneSymptom();
            positive = mode.getConfirmOnlyOneSymptomPositive();
            negative = mode.getConfirmOnlyOneSymptomNegative();
        } else {
            confirm = mode.getConfirmRegular();
            positive = mode.getConfirmRegularPositive();
            negative = mode.getConfirmRegularNegative();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml(confirm))
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                        onDonePositiveClicked();
                    }
                 })
                .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                         toolsBtns.get(mSelectedBrush).setPressed(true);
                    }
                 });

        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void newSensationInit() {
        somethingDrawn = false;
        // create new sensation
        sensation = mode.getSensationManager().createSensation(mode.getColors());

        // unToggle all buttons
        for (int i=0; i < choicesContainer.getChildCount(); i++) {
            ToggleButton b = (ToggleButton) choicesContainer.getChildAt(i);
            b.setChecked(false);
        }
        choiceSelected = false;
        // init

        // init scales
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);        // TODO: WHY
        // intensity scale
        intensityScale.setThumb(transparentDrawable);
        intensityScale.setTag(R.id.TAG_SEEKBAR_SELECTED, "false");
        intensitySelected = false;

        int color = sensation.getColor();
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] = 0.1f;
        int color1 = Color.HSVToColor(hsv);
        hsv[1] = 1.0f;
        int color2 = Color.HSVToColor(hsv);
        int[] gcolor = {color1, color2};
        intensityScale.setColors(gcolor);

        intensityScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBar.setThumb(getResources().getDrawable(R.drawable.text_cursor));
                seekBar.setTag(R.id.TAG_SEEKBAR_SELECTED, "selected");
                intensitySelected = true;
                if (mSelectedBrush != lastBrushId) {
                    toolsBtns.get(eraserId).setPressed(false);
                    toolsBtns.get(lastBrushId).setPressed(true);
                    mSelectedBrush = lastBrushId;
                    updateDrawingBrush();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // depth scale
        depthScale.setThumb(transparentDrawable);
        depthScale.setTag(R.id.TAG_SEEKBAR_SELECTED, "false");
        depthSelected = false;

        depthScale.setColors(new int[]{Color.WHITE, Color.BLACK});

        depthScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBar.setThumb(getResources().getDrawable(R.drawable.text_cursor));
                seekBar.setTag(R.id.TAG_SEEKBAR_SELECTED, "selected");
                depthSelected = true;
                sensation.setDepth(seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }

    private void onNewSensationClicked() {

        // prevent new sensation double clicking
        if (!canFinishSensationDrawing(false)) {
            return;
        }

        newSensationInit();

    }

    private void onDonePositiveClicked() {

        SensationMapper app = (SensationMapper) getApplicationContext();

        // TODO weird
        mBGRectList[getmCurrentBodyfigureView()] = mDrawingView.getBGRect();

        // save to db
        long rid = saveRecordingToDB();
        app.setRunning(rid, true);

        // save drawings
        saveMerged();
        mDrawingView.saveSteps();  // this calculates the area and attaches it to the sensation object

        // save sensations AFTER saveSteps
        mDrawingView.saveSensations(rid, getRecordingPath() + "/records.json");
        // save sex
        savePatientInformation();

        // load lock screen
        finish();
    }

    public boolean canStartSensationDrawing(boolean actionDown) {
        if (!intensitySelected) {
            if (actionDown) {
                // highlight intensity
                intensityAnimation.start();
                Toast.makeText(this, mode.getHintStartSensationDrawingAttemptWithoutIntensitySelected(), Toast.LENGTH_SHORT).show();
            }
            return false;
        } else {
            return true;
        }
    }

    // save current sensation to file

    // TODO
*/
/*
        mBGRectList[getmCurrentBodyfigureView()] = mDrawingView.getBGRect();
        saveMerged();
        mDrawingView.saveSteps();
*//*



    public boolean canFinishSensationDrawing(boolean skipSomethingDrawn) {
        if (!choiceSelected) {
            // highlight choice
            choicesAnimation.start();
            Toast.makeText(this, mode.getHintFinishSensationDrawingAttemptWithoutChoiceSelected(), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!depthSelected) {
            // highlight depth
            depthAnimation.start();
            Toast.makeText(this, mode.getHintFinishSensationDrawingAttemptWithoutDepthSelected(), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!skipSomethingDrawn && !somethingDrawn) {
            if (!intensitySelected) {
                // highlight intensity
                intensityAnimation.start();
            } else{
                // highlight body
                bodyAnimation.start();
            }
            Toast.makeText(this, mode.getHintFinishSensationDrawingAttemptWithoutSomethingDrawn(), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    //region Listeners
*/
/*    final View.OnClickListener mOnZoomListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mDrawingView.zoomInOut(Float.parseFloat(view.getTag().toString()));
        }
    };*//*


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = super.onKeyDown(keyCode, event);

        // Eat the long press event so nothing comes up.
        if (keyCode == KeyEvent.KEYCODE_MENU */
/*&& event.isLongPress()*//*
) {
            return true;
        }

        return handled;
    }


    private void getAppBrushes() {
        List<Brush> brushes = new ArrayList<>();

        Resources resources = getResources();
        TypedArray brushes_list = resources.obtainTypedArray(R.array.brushes_list);

        for (int i = 0; i < brushes_list.length(); i++) {
            int id = brushes_list.getResourceId(i, 0);
            TypedArray brush_array = resources.obtainTypedArray(id);

            Brush tempBrush = new Brush();

            tempBrush.name = brush_array.getString(AttrBrushArray.title.ordinal());
            tempBrush.type = brush_array.getString(AttrBrushArray.type.ordinal());
            tempBrush.icon = (brush_array.getDrawable(AttrBrushArray.icon.ordinal()));
            tempBrush.drawByMove = (brush_array.getBoolean(AttrBrushArray.draw_by_move.ordinal(), true));
            tempBrush.thickness = (brush_array.getInteger(AttrBrushArray.thickness.ordinal(), 1));

            Paint tempPaint = new Paint();
            tempPaint.setDither(false);
            tempPaint.setColor(0xFF33B5E5);
            tempPaint.setStrokeJoin(Paint.Join.ROUND);
            tempPaint.setStrokeCap(Paint.Cap.ROUND);
            tempPaint.setPathEffect(new CornerPathEffect(10));

            tempPaint.setStyle(Paint.Style.valueOf(brush_array.getString(AttrBrushArray.fill_style.ordinal())));
            tempPaint.setStrokeWidth(tempBrush.thickness);

            tempBrush.paint = tempPaint;

            brushes.add(tempBrush);

        }

        mBrushes = brushes;
    }


    private void getAppBodyfigures() {
        String locale = new String(Locale.getDefault().getLanguage().toString());
        String extStorage = Environment.getExternalStorageDirectory().toString();
        File extFile = new File(extStorage, "SymptomMapper/Config/" + locale + "/bodyfigures.xml");

        XMLDOMParser parser = new XMLDOMParser();
        InputStream stream;
        try {
            stream = new FileInputStream(extFile);
        } catch (FileNotFoundException ex) {
            stream = getResources().openRawResource(R.raw.bodyfigures);
        }
        //InputStream stream = getResources().openRawResource(R.raw.bodyfigures);

        Document document = parser.getDocument(stream);

        NodeList nodeList = document.getElementsByTagName(NODE_BODYFIGURE);
        mBodyfigureElement = (Element) nodeList.item(mSelectedBodyfigure);
    }

    public int getBodyfigureViewsSum() {
        return mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).getLength();

    }

    public String getBGImageName(int viewID) {
        return ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                .getElementsByTagName(NODE_BODYFIGURE_IMAGE)
                .item(0).getTextContent();

    }

    public int getBGResourceId(int viewID) {
        return getResources().getIdentifier(
                ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                        .getElementsByTagName(NODE_BODYFIGURE_IMAGE)
                        .item(0).getTextContent(),
                "drawable", getPackageName()
        );

    }

    public String getMaskImageName(int viewID) {
        return ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                .getElementsByTagName(NODE_BODYFIGURE_MASK)
                .item(0).getTextContent();

    }

    public int getMaskResourceId(int viewID) {
        return getResources().getIdentifier(
                ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                        .getElementsByTagName(NODE_BODYFIGURE_MASK)
                        .item(0).getTextContent(),
                "drawable", getPackageName()
        );

    }

    public int getEdgesMaskResourceId(int viewID) {
        return getResources().getIdentifier(
                ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                        .getElementsByTagName(NODE_BODYFIGURE_EDGEMASK)
                        .item(0).getTextContent(),
                "drawable", getPackageName()
        );
    }

    private void updateDrawingBrush() {
        final Brush brush = mBrushes.get(mSelectedBrush);
        mDrawingView.setBrush(brush);
    }

    protected void saveMerged() {

        String extStorage = Environment.getExternalStorageDirectory().toString();

        for (int i = 0; i < getBodyfigureViewsSum(); i++) {
            if (mDrawingView.getLastDisplayBitmap(i) != null) {
                File file = new File(extStorage, getRecordingPath() + "/Drawing_Merged/" +
                        i + "_" + String.valueOf(sensation.getId()) + ".png");
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }

                try {
                    FileOutputStream fOut = new FileOutputStream(file);
                    mDrawingView.getLastDisplayBitmap(i).compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (Exception e) {
                    Log.e("Exception", "Error writing merged image: " + e.toString());
                }
            }
        }

    }

    protected long saveRecordingToDB() {
        DBHelper dbh = ((SensationMapper) this.getApplication()).getDBHelper();

        int author = recording.getAuthor();
        long pid = recording.getPatientID();
        int num = recording.getNum();
        int count = sensation.getId() + 1;
        Date date = new Date();
        Recording r = new Recording(num, date, author, pid, count);
        return dbh.addRecording(r);
    }

    protected void savePatientInformation() {
        String extStorage = Environment.getExternalStorageDirectory().toString();

        File file = new File(extStorage, getPatientPath() + "/patient.txt");
        if (file.exists()) {
            return;
        }
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter writer = new OutputStreamWriter(fOut);
            BufferedWriter fbw = new BufferedWriter(writer);
            fbw.write(String.valueOf(patient.getSex()));
            fbw.newLine();
            fbw.close();
        } catch (IOException e) {
            Log.e("Exception", "Error writing patient information: " + e.toString());
        }

    }
}*/

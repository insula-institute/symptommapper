package com.mhh.sensationmapper;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.config.SensationManager;
import com.mhh.sensationmapper.config.ServerConnection;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;
import com.mhh.sensationmapper.utilities.MyWorkerThread;
import com.mhh.sensationmapper.utilities.XMLDOMParser;
import com.samsung.android.sdk.SsdkUnsupportedException;
import com.samsung.android.sdk.pen.Spen;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by sbrink on 10.03.17.
 */

public class DrawActivity extends AppCompatActivity implements View.OnClickListener {

    // do not share
    Patient patient;
    Recording recording;
    ConfigManager c;
    ModeManager.Mode mode;
    String serverURL;
    String uploadResponseString;
    int sex = 0;

    List<Brush> brushes;

    FragmentTabHost mTabHost;

    List<Integer> colors = null;
    //ColorScroller colorScroller = null;
    Button btnNewSensation = null;

    public SparseArray<Bitmap> snapshotsFront = new SparseArray<>();
    public SparseArray<Bitmap> snapshotsBack = new SparseArray<>();

    private static int dampen(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] = 0.55f;
        return Color.HSVToColor(hsv);
    }

    private MyWorkerThread mStorageThread;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mStorageThread = new MyWorkerThread("mStorageThread");
        mStorageThread.start();
        mStorageThread.prepareHandler();


        setContentView(R.layout.activity_draw);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarDrawActivity);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");
        String modeName = intent.getStringExtra("mode");

        c = new ConfigManager("SymptomMapper/Config/config.json");
        mode = c.getModeManager().getMode(modeName);

        colors = mode.getColors();
        sex = patient.getSex();

        initBrushes();
        initBodyFigures();
        initSPen();

        // init
        TextView txtEmptyTabs = (TextView)findViewById(R.id.txtEmptyTabs);
        txtEmptyTabs.setText(mode.getEmptyTabs());

        // init new Sensation Button
        btnNewSensation = (Button)findViewById(R.id.btnNewSensation);
        btnNewSensation.setText(mode.getNewSensationButton());
        btnNewSensation.setOnClickListener(this);

        // init color scroller
        //colorScroller = (ColorScroller) findViewById(R.id.colorScroller);
        //colorScroller.setLeftOffset(btnNewSensation.getWidth());
        //colorScroller.setColors(mode.getColors());

        //colorScroller.setText("Neuer Befund");
        //colorScroller.setOnColorClickListener(this);

        // init tabs
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        //mTabHost.getTabWidget().setDividerDrawable(new ColorDrawable(Color.TRANSPARENT));

/*
        FrameLayout tabContainer = (FrameLayout) findViewById(android.R.id.tabcontent);
        LayoutAnimationController controller = new LayoutAnimationController(new AlphaAnimation(0.0f, 1.0f));
        tabContainer.setLayoutAnimation(controller);
        LayoutTransition transition = new LayoutTransition();
        tabContainer.setLayoutTransition(transition);
*/

/*
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId)
            {
                View currentView = mTabHost.getCurrentView();
                if (mTabHost.getCurrentTab() > currentTab)
                {
                    currentView.setAnimation( inFromRightAnimation() );
                }
                else
                {
                    currentView.setAnimation( outToRightAnimation() );
                }

                currentTab = mTabHost.getCurrentTab();
            }
        });
*/

        // init Done Button
        Button btnDone = (Button)findViewById(R.id.btnDrawingDone);
        btnDone.setText(mode.getDrawingDoneButton());
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDrawingDoneDialog();
            }
        });

/*
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // basic init
            Intent intent = getIntent();
            patient = intent.getParcelableExtra("patient");
            recording = intent.getParcelableExtra("recording");
            String modeName = intent.getStringExtra("mode");

            c = new ConfigManager("SymptomMapper/Config/config.json");
            mode = c.getModeManager().getMode(modeName);

            initBrushes();
            initBodyFigures();
            initSPen();

            // create fragments
            DrawOverviewFragment overviewFragment = new DrawOverviewFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            //fragment.setArguments(getIntent().getExtras());

            // Add the overviewFragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, overviewFragment, "overview")
                    .commit();

        }
*/
    //Get server URL
        try {
            serverURL = mode.getServerURL();
            Log.i("serverURL: ", serverURL);
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("serverURL Error: " + e.getMessage());
        }



    }

    @Override
    public void onClick(View v) {

        int newTabIndex = mTabHost.getTabWidget().getTabCount();
        int color = colors.get(newTabIndex%colors.size());
        //int newColor = colors.get((newTabIndex+1)%colors.size());


        if (newTabIndex == 0) {
            ((FrameLayout)findViewById(android.R.id.tabcontent)).removeAllViews();
        }

        TextView tabText = new TextView(this);
        tabText.setText(String.format(mode.getSensationText(), newTabIndex + 1));
        //tabText.setBackgroundTintList(ColorStateList.valueOf(dampen(color)));
        tabText.setTextColor(Color.BLACK);
        tabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22f);
        tabText.setTypeface(Typeface.DEFAULT_BOLD);
        Drawable d = getDrawable(R.drawable.tab_indicator);
        d.setColorFilter(dampen(color), PorterDuff.Mode.SRC_ATOP);
        tabText.setBackground(d);

        Bundle b = new Bundle();
        b.putInt("tabIndex", newTabIndex);
        b.putInt("color", color);

        mTabHost.addTab(
                mTabHost.newTabSpec(String.format("tab%d", newTabIndex)).setIndicator(tabText),
                DrawDetailFragment.class, b
        );

        //btnNewSensation.setBackgroundTintList(ColorStateList.valueOf(newColor));
       // colorScroller.shift();
        mTabHost.setCurrentTab(newTabIndex);
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    //
    // TAKE SCREENSHOTS
    //

/*
    public static Bitmap takeScreenshot(View v) {

        int width = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, v.getResources().getDisplayMetrics()));
        int height = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 256, v.getResources().getDisplayMetrics()));

        Bitmap bm = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bm);
        c.save();
        c.scale(.2f, .2f);
        v.draw(c);
        c.restore();

        return bm;
    }
*/

    //
    // BRUSHES
    //

    public enum AttrBrushArray {
        title,
        icon,
        fill_style,
        draw_by_move,
        thickness,
        type
    }

    private void initBrushes() {
        brushes = new ArrayList<>();

        Resources resources = getResources();
        TypedArray brushes_list = resources.obtainTypedArray(R.array.brushes_list);

        for (int i = 0; i < brushes_list.length(); i++) {
            int id = brushes_list.getResourceId(i, 0);
            TypedArray brush_array = resources.obtainTypedArray(id);

            Brush tempBrush = new Brush();

            tempBrush.name = brush_array.getString(DrawActivity.AttrBrushArray.title.ordinal());
            tempBrush.type = brush_array.getString(DrawActivity.AttrBrushArray.type.ordinal());
            tempBrush.icon = (brush_array.getDrawable(DrawActivity.AttrBrushArray.icon.ordinal()));
            tempBrush.drawByMove = (brush_array.getBoolean(DrawActivity.AttrBrushArray.draw_by_move.ordinal(), true));
            tempBrush.thickness = (brush_array.getInteger(DrawActivity.AttrBrushArray.thickness.ordinal(), 1));

            Paint tempPaint = new Paint();
            tempPaint.setDither(false);
            tempPaint.setColor(0xFF33B5E5);
            tempPaint.setStrokeJoin(Paint.Join.ROUND);
            tempPaint.setStrokeCap(Paint.Cap.ROUND);
            tempPaint.setPathEffect(new CornerPathEffect(10));

            tempPaint.setStyle(Paint.Style.valueOf(brush_array.getString(DrawActivity.AttrBrushArray.fill_style.ordinal())));
            tempPaint.setStrokeWidth(tempBrush.thickness);

            tempBrush.paint = tempPaint;

            brushes.add(tempBrush);
        }

    }

    public List<Brush> getBrushes() {
        return brushes;
    }

    //
    // BODY_FIGURES and BACKGROUND
    //

    public static final String NODE_BODYFIGURE = "bodyfigure";
    public static final String NODE_BODYFIGURE_IMAGE = "image";
    public static final String NODE_BODYFIGURE_VIEW = "view";
    public static final String NODE_BODYFIGURE_MASK = "maskimage";

    Element mBodyfigureElement;

    private void initBodyFigures() {
        String locale = new String(Locale.getDefault().getLanguage().toString());
        String extStorage = Environment.getExternalStorageDirectory().toString();
        File extFile = new File(extStorage, "SymptomMapper/Config/" + locale + "/bodyfigures.xml");

        XMLDOMParser parser = new XMLDOMParser();
        InputStream stream;
        try {
            stream = new FileInputStream(extFile);
        } catch (FileNotFoundException ex) {
            stream = getResources().openRawResource(R.raw.bodyfigures);
        }
        //InputStream stream = getResources().openRawResource(R.raw.bodyfigures);

        Document document = parser.getDocument(stream);

        NodeList nodeList = document.getElementsByTagName(NODE_BODYFIGURE);
        mBodyfigureElement = (Element) nodeList.item(sex);    // TODO
    }


    public int getBodyFigureCount() {
        return mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).getLength();

    }

    private SparseArray<Bitmap> backgroundImages = new SparseArray<>();

    public Bitmap getBackgroundImage(int viewID) {
        Bitmap bitmap = backgroundImages.get(viewID);
        if (bitmap == null) {
            int rid = getResources().getIdentifier(
                    ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                            .getElementsByTagName(NODE_BODYFIGURE_IMAGE)
                            .item(0).getTextContent(),
                    "drawable", getPackageName()
            );
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            bitmap = BitmapFactory.decodeResource(getResources(), rid, options);
            backgroundImages.put(viewID, bitmap);
        }
        return bitmap;
    }

    private SparseArray<Bitmap> maskImages = new SparseArray<>();

    public Bitmap getMaskImage(int viewID) {
        Bitmap bitmap = maskImages.get(viewID);
        if (bitmap == null) {
            int rid = getResources().getIdentifier(
                    ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                            .getElementsByTagName(NODE_BODYFIGURE_MASK)
                            .item(0).getTextContent(),
                    "drawable", getPackageName()
            );
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            bitmap = BitmapFactory.decodeResource(getResources(), rid, options);
            maskImages.put(viewID, bitmap);
        }
        return bitmap;
    }


    private SparseArray<Bitmap> thumbImages = new SparseArray<>();

    public Bitmap getThumbImage(int viewID) {
        Bitmap bitmap = thumbImages.get(viewID);
        if (bitmap == null) {
            int rid = getResources().getIdentifier(
                    ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                            .getElementsByTagName(NODE_BODYFIGURE_MASK)
                            .item(0).getTextContent(),
                    "drawable", getPackageName()
            );
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = true;
            bitmap = BitmapFactory.decodeResource(getResources(), rid, options);
            bitmap = Bitmap.createScaledBitmap(bitmap, 311, 440, true);
            thumbImages.put(viewID, bitmap);
        }
        return bitmap;
    }


    //
    // S-PEN
    //

    private boolean sPenEnabled = false;

    private void initSPen() {

        Spen sPenPackage = new Spen();
        try {
            sPenPackage.initialize(this);
            sPenEnabled = sPenPackage.isFeatureEnabled(Spen.DEVICE_PEN);
        } catch (SsdkUnsupportedException e1) {
            Toast.makeText(this, R.string.no_spen, Toast.LENGTH_SHORT).show();
            e1.printStackTrace();
            finish();
        } catch (Exception e2) {
            Toast.makeText(this, R.string.spen_error, Toast.LENGTH_SHORT).show();
            e2.printStackTrace();
            finish();
        }
    }

    public boolean isSPenEnabled() {
        return sPenEnabled;
    }

    //
    // Detail Getters
    //

/*
    // FIXME make this better
    public int getCurrentColor() {
        return mode.getColors().get(mode.getSensationManager().getSensations().size()%mode.getColors().size());
    }

    public int getNextColor() {
        return mode.getColors().get((mode.getSensationManager().getSensations().size() + 1)%mode.getColors().size());
    }
*/

    public List<String> getChoices() {
        return mode.getChoices();
    }
    public String getIntensityScaleMin() {
        return mode.getIntensityScaleMin();
    }

//    public String getInstructionsSensation() {return mode.getInstructionsSensation(); }

    public String getIntensityScaleMax() {
        return mode.getIntensityScaleMax();
    }

    public String getTagButton() {
        return mode.getTagButton();
    }

    public String getDrawButton() {
        return mode.getDrawButton();
    }

    public String getNoChoiceSelected() {
        return mode.getNoChoiceSelected();
    }

    public String getNoIntensitySelected() {
        return mode.getNoIntensitySelected();
    }

    public String getEmptyTags() {
        return mode.getEmptyTags();
    }
    //
    // SENSATION
    //

    public SensationManager.Sensation createSensation() {
        return mode.getSensationManager().createSensation(mode.getColors());
    }

    //
    // FILE PATH INFO
    //

    private String getPatientPath() {
        return String.format("SymptomMapper/Subjects/p%d", patient.getId());
    }

    private String getRecordingPath() {
        return String.format("%s/n%d_a%d", getPatientPath(), recording.getNum(), recording.getAuthor());
    }

    //
    // Drawing Done Dialog
    //

    public void showDrawingDoneDialog() {

/*
        if (somethingDrawn && !canFinishSensationDrawing(true)) {
            return;
        }
*/

        String confirm, positive, negative;

        // TODO expand
        confirm = mode.getConfirmRegular();
        positive = mode.getConfirmRegularPositive();
        negative = mode.getConfirmRegularNegative();


/*
        if (mode.getSensationManager().getSensations().size() == 1 && !somethingDrawn) {
            confirm = mode.getConfirmNothingDrawn();
            positive = mode.getConfirmNothingDrawnPositive();
            negative = mode.getConfirmNothingDrawnNegative();
        } else if (!viewSwitched) {
            confirm = mode.getConfirmNoViewSwitch();
            positive = mode.getConfirmNoViewSwitchPositive();
            negative = mode.getConfirmNoViewSwitchNegative();
        } else if (mode.getSensationManager().getSensations().size() == 1) {
            confirm = mode.getConfirmOnlyOneSymptom();
            positive = mode.getConfirmOnlyOneSymptomPositive();
            negative = mode.getConfirmOnlyOneSymptomNegative();
        } else {
            confirm = mode.getConfirmRegular();
            positive = mode.getConfirmRegularPositive();
            negative = mode.getConfirmRegularNegative();
        }
*/

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml(confirm))
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onDonePositiveClicked();
                    }
                })
                .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        // toolsBtns.get(mSelectedBrush).setPressed(true);
                    }
                });

        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    // save stuff and leave screen

    private class MyThread implements Runnable {

        List<SensationManager.Sensation> sensations;
        Integer index;
        Integer bodyView;
        String path;
        Bitmap bm;
        Bitmap mask;

        MyThread(List<SensationManager.Sensation> sensations, Integer index, Integer bodyView, String path, Bitmap bm, Bitmap mask) {
            this.sensations = sensations;
            this.index = index;
            this.bodyView = bodyView;
            this.path = path;
            this.bm = bm;
            this.mask = mask;
        }

        private void removePartialAlpha(Bitmap b) {
            for (int x = 0; x < b.getWidth(); x++) {
                for (int y = 0; y < b.getHeight(); y++) {
                    int c = b.getPixel(x, y);
                    int a = Color.alpha(c);
                    if (0 < a && a < 255) {
                        b.setPixel(x, y, ColorUtils.setAlphaComponent(c, 0));
                    }
                }
            }
        }

        /*private void calc() {

            // calculate area of mask
            float base_area = 0;
            for (int x = 0; x < mask.getWidth(); x++) {
                for (int y = 0; y < mask.getHeight(); y++) {
                    int c = mask.getPixel(x, y);
                    if (Color.alpha(c) == 255) {
                        base_area++;
                    }
                }
            }
            //float base_area_ratio = base_area / (mask.getHeight() * mask.getWidth());

            // calculate stuff of image
            float draw_area = 0;
            float intensity = 0;
            for (int x = 0; x < bm.getWidth(); x++) {
                for (int y = 0; y < bm.getHeight(); y++) {
                    int c = bm.getPixel(x, y);
                    if (Color.alpha(c) == 255) {
                        float[] hsv = new float[3];
                        Color.colorToHSV(c, hsv);
                        draw_area++;
                        intensity += (hsv[1] - 0.1f)/0.9f;
                    }
                }
            }
            //float draw_area_ratio = draw_area / (bm.getHeight() * bm.getWidth());
            double area_ratio = draw_area/base_area;
            double intensity_mean = intensity/draw_area;

            // standard deviation
            float intensity_sigma1 = 0;
            for (int x = 0; x < bm.getWidth(); x++) {
                for (int y = 0; y < bm.getHeight(); y++) {
                    int c = bm.getPixel(x, y);
                    if (Color.alpha(c) == 255) {
                        float[] hsv = new float[3];
                        Color.colorToHSV(c, hsv);
                        intensity_sigma1 += Math.pow((intensity_mean - (hsv[1] - 0.1f)/0.9f), 2);
                    }
                }
            }
            double intensity_sigma = Math.sqrt(intensity_sigma1/draw_area);

            if (Double.isNaN(area_ratio)) {
                area_ratio = 0f;
            }
            if (Double.isNaN(intensity_mean)) {
                intensity_mean = 0f;
            }
            if (Double.isNaN(intensity_sigma)) {
                intensity_sigma = 0f;
            }

            // save final values to sensation object
            SensationManager.Sensation sensation = sensations.get(index);
            sensation.addAreaRatio(area_ratio);
            sensation.addIntensityMean(intensity_mean);
            sensation.addIntensitySigma(intensity_sigma);
        }*/

        public void run() {
            String extStorage = Environment.getExternalStorageDirectory().toString();
            File file = new File(extStorage, String.format("%s/s%d_v%d.png", path, index, bodyView));

            File parent = file.getParentFile();
            if (!parent.exists() && !parent.mkdirs()) {
                return;
            }

            // remove partial alpha
            //removePartialAlpha(mask);
            removePartialAlpha(bm);

            // calc
            //calc();

            // save
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                bm.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }

            MediaScannerConnection.scanFile(getApplicationContext(),
                    new String[] { file.toString() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        }
    }

    private class MyThread2 implements Runnable {

        List<SensationManager.Sensation> sensations;
        Integer index;
        Integer bodyView;

        MyThread2(List<SensationManager.Sensation> sensations, Integer index, Integer bodyView) {
            this.sensations = sensations;
            this.index = index;
            this.bodyView = bodyView;
        }

        private void calc() {


            // save final values to sensation object
            SensationManager.Sensation sensation = sensations.get(index);
            sensation.addAreaRatio(0d);
            sensation.addIntensityMean(null);
            sensation.addIntensitySigma(null);
        }

        public void run() {
            // calc
            calc();
        }
    }

    public class FinalThread implements Runnable {

        String path;
        ConfigManager cm;
        SensationMapper app;
        long rid;

        public FinalThread(String path, ConfigManager cm, SensationMapper app, long rid) {
            this.path = path;
            this.path = path;
            this.cm = cm;
            this.app = app;
            this.rid = rid;
        }

        //TODO: This does not seem to be working! Probably beacause the data takes a while to be saved.
        public void run() {
            cm.toFile(String.format("%s/records.json", path), false);
            Log.d("sensations", "written");
            //Upload files
            /*
            if (serverURL != null) {
                ServerConnection serverConnection = new ServerConnection(serverURL);
                serverConnection.uploadFiles(patient, getRecordingPath());
                uploadResponseString = serverConnection.getResponseString();
                try{
                    Log.d("uploadResponseString", uploadResponseString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Upload", "Procedure finished");
                app.setRunning(rid, false);
            }else {
                Log.d("Upload", "No serverURL");
            }
             */
        }
    }

    private void saveImages(SensationMapper app, long rid) {

        String path = getRecordingPath();
        List<SensationManager.Sensation> sensations = mode.getSensationManager().getSensations();

        // signal running threads
        app.setRunning(rid, true);

        // queue front images
        for (int i=0; i<sensations.size(); i++) {
            Bitmap bm = snapshotsFront.get(i);
            if (bm != null) {
                mStorageThread.postTask(new MyThread(sensations, i, 0, path, bm, maskImages.get(0)));
            } else {
                mStorageThread.postTask(new MyThread2(sensations, i, 0));
            }
        }

        // queue back images
        for (int i=0; i<sensations.size(); i++) {
            Bitmap bm = snapshotsBack.get(i);
            if (bm != null) {
                mStorageThread.postTask(new MyThread(sensations, i, 1, path, bm, maskImages.get(1)));
            } else {
                mStorageThread.postTask(new MyThread2(sensations, i, 1));
            }
        }

        // queue sensations
        c.setActiveMode(mode);
        mStorageThread.postTask(new FinalThread(path, c, app, rid));
        mStorageThread.quitSafely();

    }

    private void onDonePositiveClicked() {

        SensationMapper app = (SensationMapper) getApplicationContext();

        // save recording to db and set running flag
        DBHelper dbh = app.getDBHelper();
        int author = recording.getAuthor();
        long pid = recording.getPatientID();
        int num = recording.getNum();
        int count = mode.getSensationManager().getSensations().size();
        Date date = new Date();
        Recording r = new Recording(num, date, author, pid, count);
        long rid = dbh.addRecording(r);

        saveImages(app, rid);


        // save patient info
        String extStorage = Environment.getExternalStorageDirectory().toString();

        File file = new File(extStorage, getPatientPath() + "/patient.txt");
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            try {
                FileOutputStream fOut = new FileOutputStream(file, true);
                OutputStreamWriter writer = new OutputStreamWriter(fOut);
                BufferedWriter fbw = new BufferedWriter(writer);
                fbw.write(String.valueOf(patient.getSex()));
                fbw.newLine();
                fbw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // load lock screen
        //finish();
        //Thank you toast (repeated four times to have it longer on screen (14 s))
        for (int i=0; i < 4; i++) {
            Toast toast = Toast.makeText(DrawActivity.this, R.string.ThankYou, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        Intent i=new Intent(this, LockScreenActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);

    }

}

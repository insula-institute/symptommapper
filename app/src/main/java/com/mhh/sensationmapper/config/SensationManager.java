package com.mhh.sensationmapper.config;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbrink on 23.07.16.
 *
 * SensationManager contains the context and all sensations;
 */

public class SensationManager {

    private List<Sensation> sensations = new ArrayList<>();

    public SensationManager(JSONArray a) {
        if (a != null) {
            try {
                for (int i = 0; i < a.length(); i++) {
                    sensations.add(new Sensation(a.getJSONObject(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public JSONArray toJSON() {
        JSONArray a = new JSONArray();
        for (Sensation sensation : sensations) {
            a.put(sensation.toJSON());
        }
        return a;
    }

    public Sensation createSensation(List<Integer> colors) {
        Sensation sensation = new Sensation(colors);
        sensations.add(sensation);
        return sensation;
    }

    public Sensation getSensation(int i) {
        return sensations.get(i);
    }

    public List<Sensation> getSensations() {
        return sensations;
    }

    /*
     *  SENSATION
     */

    // SENSATION
    public class Sensation {
        private int id;
        private int color;
        private long starttime;
        private double depth;
        private List<String> choices = new ArrayList<>();
        private List<Double> area_ratio = new ArrayList<>();
        private List<Double> intensity_mean = new ArrayList<>();
        private List<Double> intensity_sigma = new ArrayList<>();

        public int getId() {
            return id;
        }

        public int getColor() {
            return color;
        }

        public double getDepth() {
            return depth;
        }

        public void setDepth(double depth) {
            this.depth = depth;
        }

        public List<String> getChoices() {
            return choices;
        }

        public void toggleChoice(String choice) {
            if (choices.contains(choice)) {
                choices.remove(choice);
            } else {
                choices.add(choice);
            }
        }

        public Double getAreaRatio(int i) {
            return area_ratio.get(i);
        }

        public Double getIntensityMean(int i) {
            return intensity_mean.get(i);
        }

        public Double getIntensitySigma(int i) {
            return intensity_sigma.get(i);
        }

        public void addAreaRatio(Double area_ratio) {
            this.area_ratio.add(area_ratio);
        }

        public void addIntensityMean(Double intensity_mean) {
            this.intensity_mean.add(intensity_mean);
        }

        public void addIntensitySigma(Double intensity_sigma) {
            this.intensity_sigma.add(intensity_sigma);
        }

        public Sensation(List<Integer> colors) {
            this.id = sensations.size();
            this.color = colors.get(id%colors.size());
            this.starttime = System.currentTimeMillis();
        }

        public Sensation(JSONObject o) {
            try {
                id = o.getInt("id");
                color = o.getInt("color");
                starttime = o.getLong("starttime");
                depth = o.getDouble("depth");
                JSONArray a;
                a = o.optJSONArray("choices");
                for (int i = 0; i < a.length(); i++) {
                    choices.add(a.getString(i));
                }
                a = o.optJSONArray("area_ratio");
                for (int i = 0; i < a.length(); i++) {
                    area_ratio.add((a.isNull(i)) ? null : a.getDouble(i));
                }
                a = o.optJSONArray("intensity_mean");
                for (int i = 0; i < a.length(); i++) {
                    intensity_mean.add((a.isNull(i)) ? null : a.getDouble(i));
                }
                a = o.optJSONArray("intensity_sigma");
                for (int i = 0; i < a.length(); i++) {
                    intensity_sigma.add((a.isNull(i)) ? null : a.getDouble(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public JSONObject toJSON() {
            JSONObject o = new JSONObject();
            try {
                o.put("id", id);
                o.put("color", color);
                o.put("starttime", starttime);
                o.put("depth", depth);
                o.put("choices", new JSONArray(choices));
                o.put("area_ratio", new JSONArray(area_ratio));
                o.put("intensity_mean", new JSONArray(intensity_mean));
                o.put("intensity_sigma", new JSONArray(intensity_sigma));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return o;
        }
    }
}

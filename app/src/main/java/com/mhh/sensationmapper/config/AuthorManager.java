package com.mhh.sensationmapper.config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sbrink on 27.07.16.
 */

public class AuthorManager {
    private List<Author> authors = new ArrayList<>();
    private HashMap<String, Author> author_map = new HashMap<>();

    public class Author {
        private String name;
        private String mode;

        public Author(JSONObject o) {
            try {
                name = o.getString("name");
                mode = o.getString("mode");
                author_map.put(name, this);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public JSONObject toJSON() {
            JSONObject o = new JSONObject();
            try {
                o.put("name", name);
                o.put("mode", mode);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return o;
        }

        public String getName() {
            return name;
        }

        public String getMode() {
            return mode;
        }
    }

    public AuthorManager(JSONArray a) {
        try {
            for (int i=0; i<a.length(); i++) {
                authors.add(new Author(a.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray toJSON() {
        JSONArray a = new JSONArray();
        for (Author author : authors) {
            a.put(author.toJSON());
        }
        return a;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public Author getMode(String name) {
        return author_map.get(name);
    }
}

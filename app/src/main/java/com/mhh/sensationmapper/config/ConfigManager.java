package com.mhh.sensationmapper.config;

import android.graphics.Color;
import android.os.Environment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbrink on 27.07.16.
 */

public class ConfigManager {
    private ModeManager m;
    private AuthorManager a;

    private String activeMode;
    private List<Integer> viewingColors = new ArrayList<>();

    public ConfigManager(String filename) {

        String extStorage = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorage, filename);

        FileInputStream fIn;
        try {
            fIn = new FileInputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        InputStreamReader ir = new InputStreamReader(fIn);
        BufferedReader br = new BufferedReader(ir);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            fIn.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // parse JSON
        JSONObject o;
        try {
            o = new JSONObject(text.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        // dispatch JSON
        try {
            a = new AuthorManager(o.getJSONArray("authors"));
            m = new ModeManager(o.getJSONArray("modes"));
            activeMode = o.optString("activeMode");
            JSONArray a;
            a = o.getJSONArray("viewingColors");
            for (int i=0; i < a.length(); i++) {
                viewingColors.add(Color.parseColor(a.getString(i)));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void toFile(String filename, Boolean append) {
        JSONObject o = new JSONObject();
        try {
            o.put("authors", a.toJSON());
            o.put("modes", m.toJSON());
            o.put("activeMode", activeMode);
            List<String> hexColors = new ArrayList<>();
            for (int color: viewingColors) {
                hexColors.add(String.format("#%06X", (0xFFFFFF & color)));
            }
            o.put("viewingColors", new JSONArray(hexColors));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String extStorage = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorage, filename);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }

        try {
            FileOutputStream fOut = new FileOutputStream(file, append);
            OutputStreamWriter writer = new OutputStreamWriter(fOut);
            BufferedWriter fbw = new BufferedWriter(writer);
            fbw.write(o.toString());
            fbw.flush();
            fbw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AuthorManager getAuthorManager() {
        return a;
    }

    public ModeManager getModeManager() {
        return m;
    }

    public ModeManager.Mode getActiveMode() {
        return m.getMode(activeMode);
    }

    public void setActiveMode(ModeManager.Mode mode) {
        activeMode = mode.getName();
    }

    public List<Integer> getViewingColors() {
        return viewingColors;
    }


}

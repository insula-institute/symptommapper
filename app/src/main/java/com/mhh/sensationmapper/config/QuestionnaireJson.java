package com.mhh.sensationmapper.config;


import android.content.res.Resources;
import android.util.Log;

import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
//Creates questionnaire.json file with user answers to the questionnaire
public class QuestionnaireJson extends JSONObject {
    public JSONObject makeJSONObject(String mode, String patgroup, Object GAD_7, Object PHQ_9, Object PDI, Object VAS, Object PHQ_15, Object PHQ_Stress, Object MPSS, Object PDS_1, Object MIDAS, Object STAI_X2, Object ADS, Object SCL_27, Object Data, Object Medication, Boolean agreement) {
//  public JSONObject makeJSONObject(ArrayList ADS, String patient, String recording, String mode) {
        JSONObject obj = new JSONObject() ;
        try {
            obj.put("mode", mode);
            obj.put("patgroup", patgroup);
            obj.put("agreement", agreement);
            obj.put("GAD_7", GAD_7);
            obj.put("PHQ_9", PHQ_9);
            obj.put("PDI", PDI);
            obj.put("VAS", VAS);
            obj.put("PHQ_15", PHQ_15);
            obj.put("PHQ_Stress", PHQ_Stress);
            obj.put("MPSS", MPSS);
            obj.put("PDS_1", PDS_1);
            obj.put("MIDAS", MIDAS);
            obj.put("STAI_X2", STAI_X2);
            obj.put("ADS", ADS);
            obj.put("SCL_27", SCL_27);
            obj.put("Data", Data);
            obj.put("Medication", Medication);

//            obj.put("mode", recording);
//            obj.put("mode", mode);
//            obj.put("imgViewPath", imgView);
/*            Log.i("Data", Data.toString());
            Log.i("Data1", Data1.toString());
            Log.i("Obj", obj.toString());*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
        }
}

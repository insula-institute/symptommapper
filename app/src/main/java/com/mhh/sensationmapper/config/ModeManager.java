package com.mhh.sensationmapper.config;

import android.graphics.Color;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sbrink on 27.07.16.
 */

public class ModeManager {
    private List<Mode> modes = new ArrayList<>();
    private HashMap<String, Mode> mode_map = new HashMap<>();

    public class Mode {
        private String name;
        private SensationManager s;

        private List<Integer> colors = new ArrayList<>();
        private List<String> choices = new ArrayList<>();
//        private List<String> q1_answers = new ArrayList<>();
        //text
        //private String hintFinishSensationDrawingAttemptWithoutChoiceSelected;
        //private String hintStartSensationDrawingAttemptWithoutIntensitySelected;
        //private String hintFinishSensationDrawingAttemptWithoutDepthSelected;
        //private String hintFinishSensationDrawingAttemptWithoutSomethingDrawn;
        //private String confirmNothingDrawn;
        //private String confirmNothingDrawnPositive;
        //private String confirmNothingDrawnNegative;
        //private String confirmNoViewSwitch;
        //private String confirmNoViewSwitchPositive;
        //private String confirmNoViewSwitchNegative;
        //private String confirmOnlyOneSymptom;
        //private String confirmOnlyOneSymptomPositive;
        //private String confirmOnlyOneSymptomNegative;
        private String confirmRegular;
        private String confirmRegularPositive;
        private String confirmRegularNegative;
        //private String addSymptomButton;
        //private String drawnSymptomsText;
        private String intensityScaleMin;
        private String intensityScaleMax;
        //private String depthScaleMin;
        //private String depthScaleMax;
        private String drawingDoneButton;
        private String newSensationButton;
        private String tagButton;
        private String drawButton;
        private String noChoiceSelected;
        private String noIntensitySelected;
        private String sensationText;
        private String emptyTabs;
        private String emptyTags;
        private String serverURL;
 //       private String instructionsSensation;
//TILL HIER
        public Mode(JSONObject o) {
            try {
                name = o.getString("name");
                s = new SensationManager(o.optJSONArray("sensations"));

                JSONArray a;
                a = o.getJSONArray("colors");
                for (int i=0; i < a.length(); i++) {
                    colors.add(Color.parseColor(a.getString(i)));
                }
                a = o.getJSONArray("choices");
                for (int i=0; i < a.length(); i++) {
                    choices.add(a.getString(i));
                }
/*
                a = o.getJSONArray("q1_answers");
                for (int i=0; i < a.length(); i++) {
                    q1_answers.add(a.getString(i));
                }
*/

                // texts
                //hintFinishSensationDrawingAttemptWithoutChoiceSelected = o.getString("hintFinishSensationDrawingAttemptWithoutChoiceSelected");
                //hintStartSensationDrawingAttemptWithoutIntensitySelected = o.getString("hintStartSensationDrawingAttemptWithoutIntensitySelected");
                //hintFinishSensationDrawingAttemptWithoutDepthSelected = o.getString("hintFinishSensationDrawingAttemptWithoutDepthSelected");
                //hintFinishSensationDrawingAttemptWithoutSomethingDrawn = o.getString("hintFinishSensationDrawingAttemptWithoutSomethingDrawn");
                //confirmNothingDrawn = o.getString("confirmNothingDrawn");
                //confirmNothingDrawnPositive = o.getString("confirmNothingDrawnPositive");
                //confirmNothingDrawnNegative = o.getString("confirmNothingDrawnNegative");
                //confirmNoViewSwitch = o.getString("confirmNoViewSwitch");
                //confirmNoViewSwitchPositive = o.getString("confirmNoViewSwitchPositive");
                //confirmNoViewSwitchNegative = o.getString("confirmNoViewSwitchNegative");
                //confirmOnlyOneSymptom = o.getString("confirmOnlyOneSymptom");
                //confirmOnlyOneSymptomPositive = o.getString("confirmOnlyOneSymptomPositive");
                //confirmOnlyOneSymptomNegative = o.getString("confirmOnlyOneSymptomNegative");
                confirmRegular = o.getString("confirmRegular");
                confirmRegularPositive = o.getString("confirmRegularPositive");
                confirmRegularNegative = o.getString("confirmRegularNegative");
                //addSymptomButton = o.getString("addSymptomButton");
                //drawnSymptomsText = o.getString("drawnSymptomsText");
                intensityScaleMin = o.getString("intensityScaleMin");
                //instructionsSensation = o.getString("instructions_sensation");
                intensityScaleMax = o.getString("intensityScaleMax");
                //depthScaleMin = o.getString("depthScaleMin");
                //depthScaleMax = o.getString("depthScaleMax");
                drawingDoneButton = o.getString("drawingDoneButton");
                newSensationButton = o.getString("newSensationButton");
                tagButton = o.getString("tagButton");
                drawButton = o.getString("drawButton");
                noChoiceSelected = o.getString("noChoiceSelected");
                noIntensitySelected = o.getString("noIntensitySelected");
                sensationText = o.getString("sensationText");
                emptyTabs = o.getString("emptyTabs");
                emptyTags = o.getString("emptyTags");
                serverURL = o.getString("serverURL");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public JSONObject toJSON() {
            JSONObject o = new JSONObject();
            try {
                o.put("name", name);
                o.put("sensations", s.toJSON());

                List<String> hexColors = new ArrayList<>();
                for (int color: colors) {
                    hexColors.add(String.format("#%06X", (0xFFFFFF & color)));
                }
                o.put("colors", new JSONArray(hexColors));
                o.put("choices", new JSONArray(choices));
                //o.put("q1_answers", new JSONArray(q1_answers));
                // texts
                //o.put("hintFinishSensationDrawingAttemptWithoutChoiceSelected", hintFinishSensationDrawingAttemptWithoutChoiceSelected);
                //o.put("hintStartSensationDrawingAttemptWithoutIntensitySelected", hintStartSensationDrawingAttemptWithoutIntensitySelected);
                //o.put("hintFinishSensationDrawingAttemptWithoutDepthSelected", hintFinishSensationDrawingAttemptWithoutDepthSelected);
                //o.put("hintFinishSensationDrawingAttemptWithoutSomethingDrawn", hintFinishSensationDrawingAttemptWithoutSomethingDrawn);
                //o.put("confirmNothingDrawn", confirmNothingDrawn);
                //o.put("confirmNothingDrawnPositive", confirmNothingDrawnPositive);
                //o.put("confirmNothingDrawnNegative", confirmNothingDrawnNegative);
                //o.put("confirmNoViewSwitch", confirmNoViewSwitch);
                //o.put("confirmNoViewSwitchPositive", confirmNoViewSwitchPositive);
                //o.put("confirmNoViewSwitchNegative", confirmNoViewSwitchNegative);
                //o.put("confirmOnlyOneSymptom", confirmOnlyOneSymptom);
                //o.put("confirmOnlyOneSymptomPositive", confirmOnlyOneSymptomPositive);
                //o.put("confirmOnlyOneSymptomNegative", confirmOnlyOneSymptomNegative);
                o.put("confirmRegular", confirmRegular);
                o.put("confirmRegularPositive", confirmRegularPositive);
                o.put("confirmRegularNegative", confirmRegularNegative);
                //o.put("addSymptomButton", addSymptomButton);
                //o.put("drawnSymptomsText", drawnSymptomsText);
                o.put("intensityScaleMin", intensityScaleMin);
                o.put("intensityScaleMax", intensityScaleMax);
                //o.put("depthScaleMin", depthScaleMin);
                //o.put("depthScaleMax", depthScaleMax);
                o.put("drawingDoneButton", drawingDoneButton);
                o.put("newSensationButton", intensityScaleMax);
                o.put("tagButton", tagButton);
                o.put("drawButton", drawButton);
                o.put("noChoiceSelected", noChoiceSelected);
                o.put("noIntensitySelected", noIntensitySelected);
                o.put("sensationText", sensationText);
                o.put("emptyTabs", emptyTabs);
                o.put("emptyTags", emptyTags);
                o.put("serverURL", serverURL);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return o;
        }

        public String getName() {
            return name;
        }

        public SensationManager getSensationManager() {
            return s;
        }

        public List<Integer> getColors() {
            return colors;
        }

        public List<String> getChoices() {
            return choices;
        }
        /*public List<String> getQ1_answers() {
            return q1_answers;
        }*/
/*
        public String getHintFinishSensationDrawingAttemptWithoutChoiceSelected() {
            return hintFinishSensationDrawingAttemptWithoutChoiceSelected;
        }

        public String getHintStartSensationDrawingAttemptWithoutIntensitySelected() {
            return hintStartSensationDrawingAttemptWithoutIntensitySelected;
        }

        public String getHintFinishSensationDrawingAttemptWithoutDepthSelected() {
            return hintFinishSensationDrawingAttemptWithoutDepthSelected;
        }

        public String getHintFinishSensationDrawingAttemptWithoutSomethingDrawn() {
            return hintFinishSensationDrawingAttemptWithoutSomethingDrawn;
        }

        public String getConfirmNothingDrawn() {
            return confirmNothingDrawn;
        }

        public String getConfirmNothingDrawnPositive() {
            return confirmNothingDrawnPositive;
        }

        public String getConfirmNothingDrawnNegative() {
            return confirmNothingDrawnNegative;
        }

        public String getConfirmNoViewSwitch() {
            return confirmNoViewSwitch;
        }

        public String getConfirmNoViewSwitchPositive() {
            return confirmNoViewSwitchPositive;
        }

        public String getConfirmNoViewSwitchNegative() {
            return confirmNoViewSwitchNegative;
        }

        public String getConfirmOnlyOneSymptom() {
            return confirmOnlyOneSymptom;
        }

        public String getConfirmOnlyOneSymptomPositive() {
            return confirmOnlyOneSymptomPositive;
        }

        public String getConfirmOnlyOneSymptomNegative() {
            return confirmOnlyOneSymptomNegative;
        }
*/

        public String getConfirmRegular() {
            return confirmRegular;
        }

        public String getConfirmRegularPositive() {
            return confirmRegularPositive;
        }

        public String getConfirmRegularNegative() {
            return confirmRegularNegative;
        }

/*
        public String getAddSymptomButton() {
            return addSymptomButton;
        }

        public String getDrawnSymptomsText() {
            return drawnSymptomsText;
        }
*/

        public String getIntensityScaleMin() {
            return intensityScaleMin;
        }

 /*       public String getInstructionsSensation() {
            return instructionsSensation;
        }*/

        public String getIntensityScaleMax() {
            return intensityScaleMax;
        }

/*
        public String getDepthScaleMin() {
            return depthScaleMin;
        }

        public String getDepthScaleMax() {
            return depthScaleMax;
        }
*/

        public String getDrawingDoneButton() {
            return drawingDoneButton;
        }

        public String getNewSensationButton() {
            return newSensationButton;
        }

        public String getTagButton() {
            return tagButton;
        }

        public String getDrawButton() {
            return drawButton;
        }

        public String getNoChoiceSelected() {
            return noChoiceSelected;
        }

        public String getNoIntensitySelected() {
            return noIntensitySelected;
        }

        public String getSensationText() {
            return sensationText;
        }

        public String getEmptyTabs() {
            return emptyTabs;
        }

        public String getServerURL() {
            return serverURL;
        }

        public String getEmptyTags() {
            return emptyTags;
        }
    }

    public ModeManager(JSONArray a) {
        try {
            for (int i=0; i<a.length(); i++) {
                Mode m = new Mode(a.getJSONObject(i));
                modes.add(m);
                mode_map.put(m.name, m);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONArray toJSON() {
        JSONArray a = new JSONArray();
        for (Mode m : modes) {
            a.put(m.toJSON());
        }

        return a;
    }

    public List<Mode> getModes() {
        return modes;
    }

    public Mode getMode(String name) {
        return mode_map.get(name);
    }

}

package com.mhh.sensationmapper.config;

import android.os.Environment;
import android.util.Log;

import com.mhh.sensationmapper.model.Patient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

//This class manages the communication with the server.

public class ServerConnection {
    private String charset = "UTF-8";
    private String serverURL;
    private String extStorage = Environment.getExternalStorageDirectory().toString();
    private String responseString;
    private List<String> response;
    private Map<String, String> patients = new HashMap<String, String>();


    //Constructor
    public ServerConnection(String c) {
        serverURL = c;
    }


    //Upload finished drawings and json files.
    //TODO: Test what happens when questionnaires or drawings is empty (it should raise 500 or 400) (Probably needs empty form fields).
    public void uploadFiles(Patient patient, String RecordingPath){
        String uploadURL = serverURL + "/upload";
        if (patient != null) {
            //Get gender
            Log.d("Gender", "Nr: " + patient.getSex());
            int gender = patient.getSex();

            //Get  files
            Log.d("Files", "Path: " + RecordingPath);
            File questionnaire = new File(extStorage, RecordingPath + "/questionnaire.json");
            File info = new File(extStorage, RecordingPath + "/records.json");
            File directory = new File(extStorage, RecordingPath);
            File[] files = directory.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".png");
                }
            });
            Log.d("Files", "Size: "+ files.length);
            for (int i = 0; i < files.length; i++) {
                Log.d("Files", "FileName:" + files[i].getName());
            }

            try {
                //Prepare form
                MultipartUtility multipart = new MultipartUtility(uploadURL, charset);
                multipart.addFormField("ID", patient.getName());
                Log.d("Upload", "ID: " + patient.getName());
                multipart.addFormField("BgImage", Integer.toString(gender));
                Log.d("Upload", "BgImage: " + Integer.toString(gender));
                multipart.addFilePart("Questionnaires", questionnaire);
                Log.d("Upload", "Questionnaires: " + questionnaire);
                multipart.addFilePart("Info", info);
                Log.d("Upload", "Info: " + info);
                for (int i = 0; i < files.length; i++) {
                    multipart.addFilePart("Drawings", files[i]);
                }

                //Submit and get response
                response = multipart.finish();
                Log.e("TAG", "SERVER REPLIED:");
                for (String line : response) {
                    Log.e("TAG", "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //UPLOAD LOG
            Date currentTime = Calendar.getInstance().getTime();
            try {
                String extStorage = Environment.getExternalStorageDirectory().toString();
                File file = new File(extStorage,  "SymptomMapper/Subjects/uploadLog.txt");
                FileOutputStream fOut = new FileOutputStream(file, true);
                OutputStreamWriter OutWriter = new OutputStreamWriter(fOut);
                OutWriter.append(currentTime + " : " + patient.getName() + " : " + RecordingPath + " : " + responseString + ";\n");
                OutWriter.close();
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    //Search for matching patients in the database
    public void searchPatient(String Surname, String Forename, String PatientNumber){
        String requestURL = serverURL + "/search";
        try {
            //Prepare form
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);
            if (Surname != null) {
                multipart.addFormField("Surname", Surname);
                Log.d("Search", "Surname: " + Surname);
            } else {
                multipart.addEmptyFormField("Surname");
            }
            if (Forename != null) {
                multipart.addFormField("Forename", Forename);
                Log.d("Search", "Forename: " + Forename);
            } else {
                multipart.addEmptyFormField("Forename");
            }
            if (PatientNumber != null) {
                multipart.addFormField("PatientNumber", PatientNumber);
                Log.d("Search", "Patient number: " + PatientNumber);
            } else {
                multipart.addEmptyFormField("PatientNumber");
            }

            //POST and get response
            response = multipart.finish();
            Log.e("TAG", "SERVER REPLIED:");
            for (String line : response) {
                //Log
                Log.e("TAG", "Search patients response:::" + line);
                //Get matching patients
                if (line.contains("option value=")) {
                    //Get patient number and string
                    final String[] bigparts = line.split(">");
                    final String[] part1 = bigparts[0].split("=");
                    final String[] part2 = bigparts[1].split("<");
                    final String patientNumber = part1[1];
                    final String patientData = part2[0];
                    patients.put("" + patientNumber, "" + patientData);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //Select Patient to get ID
    public void selectPatient(String patientNumber) {;
        String requestURL = serverURL + "/select";
        try {
            //Prepare form
            MultipartUtility multipart = new MultipartUtility(requestURL, charset);
            if (patientNumber != null) {
                multipart.addFormField("PatientNumber", patientNumber);
                Log.d("Select", "Patient number: " + patientNumber);
            } else {
                multipart.addEmptyFormField("PatientNumber");
            }

            //POST and get response
            response = multipart.finish();
            Log.e("TAG", "SERVER REPLIED:");
            for (String line : response) {
                //Log
                Log.e("TAG", "Select patients response:::" + line);
                // get your server response here.
                responseString = line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public List<String> getResponse() {
        return response;
    }


    public String getResponseString() {
        return responseString;
    }


    public Map<String, String> getPatients() {
        return patients;
    }
}

package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mhh.sensationmapper.R;
import com.warkiz.tickseekbar.OnSeekChangeListener;
import com.warkiz.tickseekbar.SeekParams;
import com.warkiz.tickseekbar.TickSeekBar;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_VAS.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_VAS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_VAS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "VAS";
    private OnFragmentInteractionListener mListener;
    public ArrayList VAS_Answers = new ArrayList();
    public TickSeekBar[] VAS_Questions = new TickSeekBar[6];
    public boolean[] VAS_Questions_tag = new boolean[6];
    Boolean AnswersMissing = false;
    public Questionnaire_VAS() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_VAS.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_VAS newInstance(String param1, String param2) {
        Questionnaire_VAS fragment = new Questionnaire_VAS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_vas, container, false);

        Button VAS_save = (Button)view.findViewById(R.id.VAS_save);
        VAS_Questions[0] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q01);
        VAS_Questions[1] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q02);
        VAS_Questions[2] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q03);
        VAS_Questions[3] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q04);
        VAS_Questions[4] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q05);
        VAS_Questions[5] = (TickSeekBar) view.findViewById(R.id.Seekbar_VAS_Q06);
        prepareSeekbars();



        VAS_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 6; i++) {
                    if(VAS_Questions_tag[i] == false){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        VAS_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        VAS_Answers.clear();
                        AnswersMissing = true;
                        }
                    //Textausgabe Antwort-ID
                    else if (VAS_Questions_tag[i] == true){
                        VAS_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        VAS_Answers.add(VAS_Questions[i].getProgress());
                        }


                    //Textausgabe Antwort
//                    int VAS_QID = VAS_Questions[i].getCheckedRadioButtonId();
//                    VAS_Answers.add(((RadioButton) VAS_Questions[i].findViewById(VAS_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    VAS_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(VAS_Answers);
                onButtonPressed(tag, jsonArray);
                //int VAS_Q01_id = VAS_Q01.getCheckedRadioButtonId();
                //String VAS_Q01_A = ((RadioButton) VAS_Q01.findViewById(VAS_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object VAS) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, VAS);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


}
    public void prepareSeekbars(){

        VAS_Questions[0].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[0].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[0] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        VAS_Questions[1].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[1].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[1] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        VAS_Questions[2].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[2].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[2] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        VAS_Questions[3].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[3].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[3] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        VAS_Questions[4].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[4].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[4] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        VAS_Questions[5].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                VAS_Questions[5].thumbColor(getResources().getColor(R.color.MHH_Rot));
                VAS_Questions_tag[5] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });

        }



}

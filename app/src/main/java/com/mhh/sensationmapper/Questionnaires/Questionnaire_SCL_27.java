
package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_SCL_27.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_SCL_27#newInstance} factory method to
 * create an instance of this fragment.
 */

public class Questionnaire_SCL_27 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "SCL_27";
    private OnFragmentInteractionListener mListener;
    public ArrayList SCL_27_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_SCL_27() {
        // Required empty public constructor
    }

/*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_SCL_27.
     */

    // TODO: Rename and change types and number of parameters
    public static Questionnaire_SCL_27 newInstance(String param1, String param2) {
        Questionnaire_SCL_27 fragment = new Questionnaire_SCL_27();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_scl_27, container, false);

        Button SCL_27_save = (Button)view.findViewById(R.id.SCL_27_save);
        final RadioGroup[] SCL_27_Questions = new RadioGroup[27];
        SCL_27_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q01);
        SCL_27_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q02);
        SCL_27_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q03);
        SCL_27_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q04);
        SCL_27_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q05);
        SCL_27_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q06);
        SCL_27_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q07);
        SCL_27_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q08);
        SCL_27_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q09);
        SCL_27_Questions[9] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q10);
        SCL_27_Questions[10] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q11);
        SCL_27_Questions[11] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q12);
        SCL_27_Questions[12] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q13);
        SCL_27_Questions[13] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q14);
        SCL_27_Questions[14] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q15);
        SCL_27_Questions[15] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q16);
        SCL_27_Questions[16] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q17);
        SCL_27_Questions[17] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q18);
        SCL_27_Questions[18] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q19);
        SCL_27_Questions[19] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q20);
        SCL_27_Questions[20] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q21);
        SCL_27_Questions[21] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q22);
        SCL_27_Questions[22] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q23);
        SCL_27_Questions[23] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q24);
        SCL_27_Questions[24] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q25);
        SCL_27_Questions[25] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q26);
        SCL_27_Questions[26] = (RadioGroup)view.findViewById(R.id.Radiogroup_SCL_27_Q27);



        SCL_27_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 27; i++) {
                    if(SCL_27_Questions[i].getCheckedRadioButtonId() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        SCL_27_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        SCL_27_Answers.clear();
                        AnswersMissing = true;
                        }
                    //Textausgabe Antwort-ID
                    else if (SCL_27_Questions[i].getCheckedRadioButtonId() != -1){
                        SCL_27_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        //add radio button tag
                        int SCL_27_QID = SCL_27_Questions[i].getCheckedRadioButtonId();
                        SCL_27_Answers.add(Integer.parseInt(String.valueOf((SCL_27_Questions[i].findViewById(SCL_27_QID)).getTag())));

                        //get radio button-ID as text
                        //SCL_27_Answers.add(getResources().getResourceEntryName(SCL_27_Questions[i].getCheckedRadioButtonId()));
                        }
                    //else {SCL_27_Answers.add("-");}

                    //Textausgabe Antwort
//                    int SCL_27_QID = SCL_27_Questions[i].getCheckedRadioButtonId();
//                    SCL_27_Answers.add(((RadioButton) SCL_27_Questions[i].findViewById(SCL_27_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    SCL_27_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(SCL_27_Answers);
                onButtonPressed(tag, jsonArray);
                //int SCL_27_Q01_id = SCL_27_Q01.getCheckedRadioButtonId();
                //String SCL_27_Q01_A = ((RadioButton) SCL_27_Q01.findViewById(SCL_27_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/

                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object SCL_27) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, SCL_27);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

/*
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}


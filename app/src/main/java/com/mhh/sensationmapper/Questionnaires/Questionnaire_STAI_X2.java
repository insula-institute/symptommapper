package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_STAI_X2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_STAI_X2#newInstance} factory method to
 * create an instance of this fragment.
 */

public class Questionnaire_STAI_X2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "STAI_X2";
    private OnFragmentInteractionListener mListener;
    public ArrayList STAI_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_STAI_X2() {
        // Required empty public constructor
    }

/*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_STAI.
     */

    // TODO: Rename and change types and number of parameters
    public static Questionnaire_STAI_X2 newInstance(String param1, String param2) {
        Questionnaire_STAI_X2 fragment = new Questionnaire_STAI_X2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_stai_x2, container, false);

        Button STAI_save = (Button)view.findViewById(R.id.STAI_save);
        final RadioGroup[] STAI_Questions = new RadioGroup[20];
        STAI_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q01);
        STAI_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q02);
        STAI_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q03);
        STAI_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q04);
        STAI_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q05);
        STAI_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q06);
        STAI_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q07);
        STAI_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q08);
        STAI_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q09);
        STAI_Questions[9] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q10);
        STAI_Questions[10] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q11);
        STAI_Questions[11] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q12);
        STAI_Questions[12] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q13);
        STAI_Questions[13] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q14);
        STAI_Questions[14] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q15);
        STAI_Questions[15] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q16);
        STAI_Questions[16] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q17);
        STAI_Questions[17] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q18);
        STAI_Questions[18] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q19);
        STAI_Questions[19] = (RadioGroup)view.findViewById(R.id.Radiogroup_STAI_Q20);

        STAI_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 20; i++) {
                    if(STAI_Questions[i].getCheckedRadioButtonId() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        STAI_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        STAI_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (STAI_Questions[i].getCheckedRadioButtonId() != -1){
                        STAI_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int STAI_QID = STAI_Questions[i].getCheckedRadioButtonId();
                        STAI_Answers.add(Integer.parseInt(String.valueOf((STAI_Questions[i].findViewById(STAI_QID)).getTag())));
                        //STAI_Answers.add(getResources().getResourceEntryName(STAI_Questions[i].getCheckedRadioButtonId()));
                        }
                    //else {STAI_Answers.add("-");}

                    //Textausgabe Antwort
//                    int STAI_QID = STAI_Questions[i].getCheckedRadioButtonId();
//                    STAI_Answers.add(((RadioButton) STAI_Questions[i].findViewById(STAI_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    STAI_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(STAI_Answers);
                onButtonPressed(tag, jsonArray);
                //int STAI_Q01_id = STAI_Q01.getCheckedRadioButtonId();
                //String STAI_Q01_A = ((RadioButton) STAI_Q01.findViewById(STAI_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/

                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object STAI) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, STAI);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

/*
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}


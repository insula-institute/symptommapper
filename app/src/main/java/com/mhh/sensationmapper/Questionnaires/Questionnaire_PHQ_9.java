package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_PHQ_9.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_PHQ_9#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_PHQ_9 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "PHQ_9";
    private OnFragmentInteractionListener mListener;
    public ArrayList PHQ_9_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_PHQ_9() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_PHQ_9.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_PHQ_9 newInstance(String param1, String param2) {
        Questionnaire_PHQ_9 fragment = new Questionnaire_PHQ_9();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_phq_9, container, false);

        Button PHQ_9_save = (Button)view.findViewById(R.id.PHQ_9_save);
        final RadioGroup[] PHQ_9_Questions = new RadioGroup[20];
        PHQ_9_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q01);
        PHQ_9_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q02);
        PHQ_9_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q03);
        PHQ_9_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q04);
        PHQ_9_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q05);
        PHQ_9_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q06);
        PHQ_9_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q07);
        PHQ_9_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q08);
        PHQ_9_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_PHQ_9_Q09);


        PHQ_9_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 9; i++) {
                    if(PHQ_9_Questions[i].getCheckedRadioButtonId() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        PHQ_9_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        PHQ_9_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (PHQ_9_Questions[i].getCheckedRadioButtonId() != -1){
                        PHQ_9_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int QID = PHQ_9_Questions[i].getCheckedRadioButtonId();
                        PHQ_9_Answers.add(Integer.parseInt(String.valueOf((PHQ_9_Questions[i].findViewById(QID)).getTag())));
                        //PHQ_9_Answers.add(getResources().getResourceEntryName(PHQ_9_Questions[i].getCheckedRadioButtonId()));
                        }

                    //else {PHQ_9_Answers.add("-");}

                    //Textausgabe Antwort
//                    int PHQ_9_QID = PHQ_9_Questions[i].getCheckedRadioButtonId();
//                    PHQ_9_Answers.add(((RadioButton) PHQ_9_Questions[i].findViewById(PHQ_9_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    PHQ_9_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(PHQ_9_Answers);
                onButtonPressed(tag, jsonArray);
                //int PHQ_9_Q01_id = PHQ_9_Q01.getCheckedRadioButtonId();
                //String PHQ_9_Q01_A = ((RadioButton) PHQ_9_Q01.findViewById(PHQ_9_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object PHQ_9) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, PHQ_9);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}

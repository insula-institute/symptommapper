
package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_ADS.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_ADS#newInstance} factory method to
 * create an instance of this fragment.
 */

public class Questionnaire_ADS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "ADS";
    private OnFragmentInteractionListener mListener;
    public ArrayList ADS_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_ADS() {
        // Required empty public constructor
    }

/*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_ADS.
     */

    // TODO: Rename and change types and number of parameters
    public static Questionnaire_ADS newInstance(String param1, String param2) {
        Questionnaire_ADS fragment = new Questionnaire_ADS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_ads, container, false);

        Button ADS_save = (Button)view.findViewById(R.id.ADS_save);
        final RadioGroup[] ADS_Questions = new RadioGroup[20];
        ADS_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q01);
        ADS_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q02);
        ADS_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q03);
        ADS_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q04);
        ADS_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q05);
        ADS_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q06);
        ADS_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q07);
        ADS_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q08);
        ADS_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q09);
        ADS_Questions[9] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q10);
        ADS_Questions[10] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q11);
        ADS_Questions[11] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q12);
        ADS_Questions[12] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q13);
        ADS_Questions[13] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q14);
        ADS_Questions[14] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q15);
        ADS_Questions[15] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q16);
        ADS_Questions[16] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q17);
        ADS_Questions[17] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q18);
        ADS_Questions[18] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q19);
        ADS_Questions[19] = (RadioGroup)view.findViewById(R.id.Radiogroup_ADS_Q20);

        ADS_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 20; i++) {
                    if(ADS_Questions[i].getCheckedRadioButtonId() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        ADS_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        ADS_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (ADS_Questions[i].getCheckedRadioButtonId() != -1){
                        ADS_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int QID = ADS_Questions[i].getCheckedRadioButtonId();
                        ADS_Answers.add(Integer.parseInt(String.valueOf((ADS_Questions[i].findViewById(QID)).getTag())));
                        //ADS_Answers.add(getResources().getResourceEntryName(ADS_Questions[i].getCheckedRadioButtonId()));
                        }

                    //else {ADS_Answers.add("-");}

                    //Textausgabe Antwort
//                    int ADS_QID = ADS_Questions[i].getCheckedRadioButtonId();
//                    ADS_Answers.add(((RadioButton) ADS_Questions[i].findViewById(ADS_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    ADS_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(ADS_Answers);
                onButtonPressed(tag, jsonArray);
                //int ADS_Q01_id = ADS_Q01.getCheckedRadioButtonId();
                //String ADS_Q01_A = ((RadioButton) ADS_Q01.findViewById(ADS_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/

                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object ADS) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, ADS);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

/*
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}


package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.graphics.Color;

import com.mhh.sensationmapper.R;
import com.warkiz.tickseekbar.OnSeekChangeListener;
import com.warkiz.tickseekbar.SeekParams;
import com.warkiz.tickseekbar.TickSeekBar;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_PDI.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_PDI#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_PDI extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "PDI";
    private OnFragmentInteractionListener mListener;
    public ArrayList PDI_Answers = new ArrayList();
    public TickSeekBar[] PDI_Questions = new TickSeekBar[7];
    public boolean[] PDI_Questions_tag = new boolean[7];
    Boolean AnswersMissing = false;
    public Questionnaire_PDI() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_PDI.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_PDI newInstance(String param1, String param2) {
        Questionnaire_PDI fragment = new Questionnaire_PDI();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_pdi, container, false);

        Button PDI_save = (Button)view.findViewById(R.id.PDI_save);
        PDI_Questions[0] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q01);
        PDI_Questions[1] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q02);
        PDI_Questions[2] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q03);
        PDI_Questions[3] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q04);
        PDI_Questions[4] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q05);
        PDI_Questions[5] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q06);
        PDI_Questions[6] = (TickSeekBar) view.findViewById(R.id.Seekbar_PDI_Q07);
        prepareSeekbars();



        PDI_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 7; i++) {
                    if(PDI_Questions_tag[i] == false){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        PDI_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        PDI_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (PDI_Questions_tag[i] == true){
                        PDI_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        PDI_Answers.add(Integer.parseInt(String.valueOf(PDI_Questions[i].getProgress()/10)));
                        }

                    //Textausgabe Antwort
//                    int PDI_QID = PDI_Questions[i].getCheckedRadioButtonId();
//                    PDI_Answers.add(((RadioButton) PDI_Questions[i].findViewById(PDI_QID)).getText().toString());
                }
                //int PDI_Q01_id = PDI_Q01.getCheckedRadioButtonId();
                //String PDI_Q01_A = ((RadioButton) PDI_Q01.findViewById(PDI_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    PDI_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(PDI_Answers);
                onButtonPressed(tag, jsonArray);
            }
        });



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object PDI) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, PDI);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


}
    public void prepareSeekbars(){

        PDI_Questions[0].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[0].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[0] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[1].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[1].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[1] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[2].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[2].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[2] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[3].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[3].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[3] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[4].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[4].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[4] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[5].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[5].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[5] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        PDI_Questions[6].setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {}
            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
                PDI_Questions[6].thumbColor(getResources().getColor(R.color.MHH_Rot));
                PDI_Questions_tag[6] = true;
            }
            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }
        });
        }



}

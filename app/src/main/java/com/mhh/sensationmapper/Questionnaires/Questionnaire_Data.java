package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.mhh.sensationmapper.R;
import com.mhh.sensationmapper.config.MedicationAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_Data.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_Data#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_Data extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    /*private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;*/
    public MedicationAdapter adapter;


    private String mParam1;
    private String mParam2;
    private String tag = "Data";
    private OnFragmentInteractionListener mListener;
    public ArrayList Data_Answers = new ArrayList();
    public NumberPicker[] Data_Questions = new NumberPicker[3];
    public Questionnaire_Data() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_Data.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_Data newInstance(String param1, String param2) {
        Questionnaire_Data fragment = new Questionnaire_Data();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_data, container, false);



        Button Data_save = (Button)view.findViewById(R.id.Data_save);
        Data_Questions[0] = (NumberPicker) view.findViewById(R.id.numberPicker_Data_Q01);
        Data_Questions[1] = (NumberPicker) view.findViewById(R.id.numberPicker_Data_Q02);
        Data_Questions[2] = (NumberPicker) view.findViewById(R.id.numberPicker_Data_Q03);
        prepareNumberPickers();

        Data_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < 3; i++) {
                    /*if(Data_Questions[i].getValue() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        Data_Answers.clear();
                        return;}*/
                    //Textausgabe Antwort-ID
//                    else if (Data_Questions[i].getValue() != -1){
                        Data_Answers.add(Data_Questions[i].getValue());
//                        }

                    //else {Data_Answers.add("-");}

                    //Textausgabe Antwort
//                    int Data_QID = Data_Questions[i].getCheckedRadioButtonId();
//                    Data_Answers.add(((RadioButton) Data_Questions[i].findViewById(Data_QID)).getText().toString());
                }
                JSONArray jsonArray = new JSONArray(Data_Answers);
                //Log.i("JsonArray", jsonArray.toString());
                onButtonPressed(tag, jsonArray);

                //int Data_Q01_id = Data_Q01.getCheckedRadioButtonId();
                //String Data_Q01_A = ((RadioButton) Data_Q01.findViewById(Data_Q01_id)).getText().toString();
/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

                //hide keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object Data) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, Data);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }

    public void prepareNumberPickers(){
        Data_Questions[0].setMaxValue(250);
        Data_Questions[0].setMinValue(20);
        Data_Questions[0].setValue(75);
        Data_Questions[0].setWrapSelectorWheel(true);
        setDividerColor(Data_Questions[0], ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
        Data_Questions[1].setMaxValue(250);
        Data_Questions[1].setMinValue(100);
        Data_Questions[1].setValue(170);
        Data_Questions[1].setWrapSelectorWheel(true);
        setDividerColor(Data_Questions[1], ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
        Data_Questions[2].setMaxValue(120);
        Data_Questions[2].setMinValue(1);
        Data_Questions[2].setValue(30);
        Data_Questions[2].setWrapSelectorWheel(true);
        setDividerColor(Data_Questions[2], ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
    }
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}

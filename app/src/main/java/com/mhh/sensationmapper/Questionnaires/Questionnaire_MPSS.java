package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_MPSS.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_MPSS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_MPSS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "MPSS";
    private OnFragmentInteractionListener mListener;
    public ArrayList MPSS_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_MPSS() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_MPSS.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_MPSS newInstance(String param1, String param2) {
        Questionnaire_MPSS fragment = new Questionnaire_MPSS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_mpss, container, false);

        Button MPSS_save = (Button)view.findViewById(R.id.MPSS_save);
        final RadioGroup[] MPSS_Questions = new RadioGroup[11];
        MPSS_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q01);
        MPSS_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q02);
        MPSS_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q03);
        MPSS_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q04);
        MPSS_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q05);
        MPSS_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q06);
        MPSS_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q07);
        MPSS_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q08);
        MPSS_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q09);
        MPSS_Questions[9] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q10);
        MPSS_Questions[10] = (RadioGroup)view.findViewById(R.id.Radiogroup_MPSS_Q11);


        MPSS_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 11; i++) {
                    if(MPSS_Questions[i].getCheckedRadioButtonId() == -1){
                        MPSS_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        MPSS_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (MPSS_Questions[i].getCheckedRadioButtonId() != -1){
                        MPSS_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int MPSS_QID = MPSS_Questions[i].getCheckedRadioButtonId();
                        MPSS_Answers.add(Integer.parseInt(String.valueOf((MPSS_Questions[i].findViewById(MPSS_QID)).getTag())));
                        //MPSS_Answers.add(getResources().getResourceEntryName(MPSS_Questions[i].getCheckedRadioButtonId()));
                        }

                    //else {MPSS_Answers.add("-");}

                    //Textausgabe Antwort
//                    int MPSS_QID = MPSS_Questions[i].getCheckedRadioButtonId();
//                    MPSS_Answers.add(((RadioButton) MPSS_Questions[i].findViewById(MPSS_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    MPSS_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(MPSS_Answers);
                onButtonPressed(tag, jsonArray);
                //int MPSS_Q01_id = MPSS_Q01.getCheckedRadioButtonId();
                //String MPSS_Q01_A = ((RadioButton) MPSS_Q01.findViewById(MPSS_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object MPSS) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, MPSS);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}

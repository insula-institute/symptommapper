package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_GAD_7.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_GAD_7#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_GAD_7 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "GAD_7";
    private OnFragmentInteractionListener mListener;
    public ArrayList GAD_7_Answers = new ArrayList();
    Boolean AnswersMissing = false;
    public Questionnaire_GAD_7() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_GAD_7.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_GAD_7 newInstance(String param1, String param2) {
        Questionnaire_GAD_7 fragment = new Questionnaire_GAD_7();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_gad_7, container, false);

        Button GAD_7_save = (Button)view.findViewById(R.id.GAD_7_save);
        final RadioGroup[] GAD_7_Questions = new RadioGroup[20];
        GAD_7_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q01);
        GAD_7_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q02);
        GAD_7_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q03);
        GAD_7_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q04);
        GAD_7_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q05);
        GAD_7_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q06);
        GAD_7_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_GAD_7_Q07);



        GAD_7_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 7; i++) {
                    if(GAD_7_Questions[i].getCheckedRadioButtonId() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        GAD_7_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        GAD_7_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (GAD_7_Questions[i].getCheckedRadioButtonId() != -1){
                        GAD_7_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int QID = GAD_7_Questions[i].getCheckedRadioButtonId();
                        GAD_7_Answers.add(Integer.parseInt(String.valueOf((GAD_7_Questions[i].findViewById(QID)).getTag())));
                        //GAD_7_Answers.add(getResources().getResourceEntryName(GAD_7_Questions[i].getCheckedRadioButtonId()));
                        }

                    //else {GAD_7_Answers.add("-");}

                    //Textausgabe Antwort
//                    int GAD_7_QID = GAD_7_Questions[i].getCheckedRadioButtonId();
//                    GAD_7_Answers.add(((RadioButton) GAD_7_Questions[i].findViewById(GAD_7_QID)).getText().toString());
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    GAD_7_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(GAD_7_Answers);
                onButtonPressed(tag, jsonArray);
                //int GAD_7_Q01_id = GAD_7_Q01.getCheckedRadioButtonId();
                //String GAD_7_Q01_A = ((RadioButton) GAD_7_Q01.findViewById(GAD_7_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object GAD_7) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, GAD_7);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    // get sex

}

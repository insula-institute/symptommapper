package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_PDS_1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_PDS_1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_PDS_1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "PDS_1";
    public int NumberofYes;
    private OnFragmentInteractionListener mListener;
    public ArrayList PDS_1_Answers = new ArrayList();
    public RadioGroup[] PDS_1_Questions = new RadioGroup[13];

    Boolean AnswersMissing = false;
    public Questionnaire_PDS_1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_PDS_1.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_PDS_1 newInstance(String param1, String param2) {
        Questionnaire_PDS_1 fragment = new Questionnaire_PDS_1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_pds_1, container, false);
        Button PDS_1_save = (Button)view.findViewById(R.id.PDS_1_save);
        PDS_1_Questions[0] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q01);
        PDS_1_Questions[1] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q02);
        PDS_1_Questions[2] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q03);
        PDS_1_Questions[3] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q04);
        PDS_1_Questions[4] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q05);
        PDS_1_Questions[5] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q06);
        PDS_1_Questions[6] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q07);
        PDS_1_Questions[7] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q08);
        PDS_1_Questions[8] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q09);
        PDS_1_Questions[9] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q10);
        PDS_1_Questions[10] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q11);
        PDS_1_Questions[11] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q12);
        PDS_1_Questions[12] = (RadioGroup)view.findViewById(R.id.Radiogroup_PDS_1_Q13);
        final LinearLayout Placeholder_Q12_01 =(LinearLayout)view.findViewById(R.id.LinearLayout_PDS_1_Q12_01);
        final LinearLayout Placeholder_Q13 =(LinearLayout)view.findViewById(R.id.LinearLayout_PDS_1_Q13);
        prepareRadioButtons(view, Placeholder_Q12_01, Placeholder_Q13);
        /*rb_Q11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Placeholder_Q11_01.setVisibility(view.VISIBLE);
            }
        });*/

        PDS_1_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 13; i++) {
                    if((PDS_1_Questions[i].getCheckedRadioButtonId() == -1)&&(i<12)){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        PDS_1_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        PDS_1_Answers.clear();
                        AnswersMissing = true;}
                    if((PDS_1_Questions[i].getCheckedRadioButtonId() == -1)&&(i==12)&&(Placeholder_Q13.getVisibility() == View.VISIBLE)){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        PDS_1_Questions[i].setBackgroundColor(Color.parseColor("#f79c87"));
                        PDS_1_Answers.clear();
                        AnswersMissing = true;}
                    //Textausgabe Antwort-ID
                    else if (PDS_1_Questions[i].getCheckedRadioButtonId() != -1){
                        PDS_1_Questions[i].setBackgroundColor(Color.parseColor("#00000000"));
                        int QID = PDS_1_Questions[i].getCheckedRadioButtonId();
                        PDS_1_Answers.add(Integer.parseInt(String.valueOf((PDS_1_Questions[i].findViewById(QID)).getTag())));
                        //get Radiobutton-id
                        //PDS_1_Answers.add(getResources().getResourceEntryName(PDS_1_Questions[i].getCheckedRadioButtonId()));
                        /*String radiovalue = ((RadioButton)view.findViewById(PDS_1_Questions[i].getCheckedRadioButtonId())).getText().toString();
                        LinearLayout Placeholder_Q12 =(LinearLayout)view.findViewById(R.id.LinearLayout_PDS_1_Q12);
                        if (radiovalue.equals("ja")) {
                            NumberofYes++;
                            if ((NumberofYes>1) && (Placeholder_Q12.getVisibility() == View.GONE)){
                                Placeholder_Q12.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), R.string.PDS_1_I02, Toast.LENGTH_LONG).show();
                            return;}
                        }*/

                    }

                    else {PDS_1_Answers.add("");}

                    //Textausgabe Antwort
//                    int PDS_1_QID = PDS_1_Questions[i].getCheckedRadioButtonId();
//                    PDS_1_Answers.add(((RadioButton) PDS_1_Questions[i].findViewById(PDS_1_QID)).getText().toString());
                }
                LinearLayout Placeholder_Q12_01 =(LinearLayout)view.findViewById(R.id.LinearLayout_PDS_1_Q12_01);
                EditText PDS_1_Q12_01_text = (EditText)view.findViewById(R.id.PDS_1_Q12_01_text);
                String usertext = PDS_1_Q12_01_text.getText().toString();
                if (Placeholder_Q12_01.getVisibility() == View.VISIBLE){
                    PDS_1_Answers.add(usertext);
                } else {
                    PDS_1_Answers.add("");
                }
                if (AnswersMissing == true){
                    AnswersMissing = false;
                    PDS_1_Answers.clear();
                    return;
                }
                JSONArray jsonArray = new JSONArray(PDS_1_Answers);
                onButtonPressed(tag, jsonArray);
                //int PDS_1_Q01_id = PDS_1_Q01.getCheckedRadioButtonId();
                //String PDS_1_Q01_A = ((RadioButton) PDS_1_Q01.findViewById(PDS_1_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object PDS_1) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, PDS_1);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    public void prepareRadioButtons(View view, final LinearLayout Placeholder_Q12_01, final LinearLayout Placeholder_Q13){

        final RadioButton PDS_1_Q13_01 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_01);
        final RadioButton PDS_1_Q13_02 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_02);
        final RadioButton PDS_1_Q13_03 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_03);
        final RadioButton PDS_1_Q13_04 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_04);
        final RadioButton PDS_1_Q13_05 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_05);
        final RadioButton PDS_1_Q13_06 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_06);
        final RadioButton PDS_1_Q13_07 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_07);
        final RadioButton PDS_1_Q13_08 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_08);
        final RadioButton PDS_1_Q13_09 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_09);
        final RadioButton PDS_1_Q13_10 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_10);
        final RadioButton PDS_1_Q13_11 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_11);
        final RadioButton PDS_1_Q13_12 =  (RadioButton)view.findViewById(R.id.PDS_1_Q13_12);
        for (int i = 0; i < 13; i++) {
            PDS_1_Questions[i].setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId) {
                        case R.id.PDS_1_Q01_01:
                            NumberofYes++;
                            PDS_1_Q13_01.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q01_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_01.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_01.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q02_01:
                            NumberofYes++;
                            PDS_1_Q13_02.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q02_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_02.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_02.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q03_01:
                            NumberofYes++;
                            PDS_1_Q13_03.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q03_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_03.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_03.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q04_01:
                            NumberofYes++;
                            PDS_1_Q13_04.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q04_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_04.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_04.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q05_01:
                            NumberofYes++;
                            PDS_1_Q13_05.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q05_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_05.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_05.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q06_01:
                            NumberofYes++;
                            PDS_1_Q13_06.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q06_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_06.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_06.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q07_01:
                            NumberofYes++;
                            PDS_1_Q13_07.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q07_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_07.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_07.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q08_01:
                            NumberofYes++;
                            PDS_1_Q13_08.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q08_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_08.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_08.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q09_01:
                            NumberofYes++;
                            PDS_1_Q13_09.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q09_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_09.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_09.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q10_01:
                            NumberofYes++;
                            PDS_1_Q13_10.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q10_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_10.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_10.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q11_01:
                            NumberofYes++;
                            PDS_1_Q13_11.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q11_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_11.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            PDS_1_Q13_11.setVisibility(View.GONE);
                            break;
                        case R.id.PDS_1_Q12_01:
                            NumberofYes++;
                            Placeholder_Q12_01.setVisibility(View.VISIBLE);
                            PDS_1_Q13_12.setVisibility(View.VISIBLE);
                            break;
                        case R.id.PDS_1_Q12_02:
                            if ((NumberofYes>0)&&(PDS_1_Q13_12.getVisibility() == View.VISIBLE)){NumberofYes--;};
                            Placeholder_Q12_01.setVisibility(View.GONE);
                            PDS_1_Q13_12.setVisibility(View.GONE);
                            break;
                    }
                    if ((NumberofYes>1) && (Placeholder_Q13.getVisibility() == View.GONE)){
                        Placeholder_Q13.setVisibility(View.VISIBLE);
                    }
                    if ((NumberofYes<=1) && (Placeholder_Q13.getVisibility() == View.VISIBLE)){
                        Placeholder_Q13.setVisibility(View.GONE);
                    }
                }
            });

        }
        }
}

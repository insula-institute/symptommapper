package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.Placeholder;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mhh.sensationmapper.QuestionnairePainActivity;
import com.mhh.sensationmapper.R;
import com.mhh.sensationmapper.config.MedicationAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_Medication.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_Medication#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_Medication extends Fragment implements MedicationAdapter.ItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    /*private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;*/
    public MedicationAdapter adapter;


    private String mParam1;
    private String mParam2;
    private String tag = "Medication";
    private OnFragmentInteractionListener mListener;
    Button Med_add;
    Button Med_save;
    AutoCompleteTextView MedName;
    EditText MedDose;
    EditText MedNote;
    NumberPicker MedMorning;
    NumberPicker MedNoon;
    NumberPicker MedEvening;
    NumberPicker MedNight;
    RadioGroup Medication_Q01;
    // Medication to populate the RecyclerView with
    public ArrayList<String> Medication = new ArrayList<>();
    //List<List<String>> MedDB = new ArrayList<List<String>>();
    //List<String> Med = new ArrayList<String>();
    //public ArrayList Med = new ArrayList();
    public JSONArray Med = new JSONArray();
    //public ArrayList Medication_Answers = new ArrayList();
    public JSONArray Medication_Answers = new JSONArray();
    String item;
    int insertIndex = 0;
    String[] NumberpickerValues = {"0", "0.5", "1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10"};
    public Questionnaire_Medication() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_Medication.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_Medication newInstance(String param1, String param2) {
        Questionnaire_Medication fragment = new Questionnaire_Medication();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_medication, container, false);


        //RecyclerView for medication
/*        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_medication);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new MedicationAdapter(myMedicationset);
        mRecyclerView.setAdapter(mAdapter);*/



        // set up the RecyclerView
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_medication);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MedicationAdapter(getActivity(), Medication);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.HORIZONTAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        Med_add = (Button)view.findViewById(R.id.btn_Med_add);
        Med_save = (Button)view.findViewById(R.id.btn_Med_save);
        // Get a reference to the AutoCompleteTextView in the layout
        MedName = (AutoCompleteTextView)view.findViewById(R.id.MedName);
        MedDose= (EditText)view.findViewById(R.id.MedDose);;
        MedNote= (EditText)view.findViewById(R.id.MedNote);;
        MedMorning= (NumberPicker)view.findViewById(R.id.MedMorning);
        MedNoon=(NumberPicker)view.findViewById(R.id.MedNoon);
        MedEvening=(NumberPicker)view.findViewById(R.id.MedEvening);
        MedNight=(NumberPicker)view.findViewById(R.id.MedNight);
        prepareNumberPickers();
        final CardView NewMedication = (CardView)view.findViewById(R.id.NewMedication);
        final TextView textView_Medication_Q01 = (TextView)view.findViewById(R.id.textView_Medication_Q01);
        String patgroup = QuestionnairePainActivity.getPatgroup();
        if (patgroup.equals("btn_pgrp_r")) {
            textView_Medication_Q01.setText(R.string.Medication_Q01r);
        }else {
            textView_Medication_Q01.setText(R.string.Medication_Q01n);
        }


        // Get the string array
        String[] medicationMHH = getResources().getStringArray(R.array.medication_array);
        // Create the adapter and set it to the AutoCompleteTextView
        ArrayAdapter<String> MHHadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, medicationMHH);
        MedName.setAdapter(MHHadapter);
       /* //prepare YES/NO Medi Question and visibility
        Medication_Q01 = (RadioGroup)view.findViewById(R.id.Radiogroup_Medication_Q01);
        final LinearLayout placeholder_MedList = (LinearLayout)view.findViewById(R.id.placeholder_MedList);
        Medication_Q01.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                switch (checkedId) {
                    case R.id.Medication_Q01_01:
                        placeholder_MedList.setVisibility(View.VISIBLE);
                        break;
                    case R.id.Medication_Q01_02:
                        placeholder_MedList.setVisibility(View.INVISIBLE);
                        break;
                }}});*/
        //Add medi
        Med_add.setOnClickListener(new View.OnClickListener() {
            @Override
                    public void onClick(View v) {
                    NewMedication.setVisibility(View.VISIBLE);
                    //show keyboard and focus on EditText MedName
                    MedName.requestFocus();
                    MedName.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                    //disable MedAdd Button
                    Med_add.setEnabled(false);

        }});
        //Save medi
        Med_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewMedication.setVisibility(View.GONE);
                //hide keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(MedName.getWindowToken(), 0);
                //enable MedAdd Button
                Med_add.setEnabled(true);
                //put values into DB
                getMedValues();
                insertIndex = Medication.size();
                Medication.add(insertIndex, item);
                adapter.notifyItemInserted(insertIndex);
            }});

        //Save button for questionnaire
        Button Medication_save = (Button)view.findViewById(R.id.Medication_save);
        Medication_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Medication_Answers.add(MedDB);
                //JSONArray jsonArray = new JSONArray(Medication_Answers);
                onButtonPressed(tag, Medication_Answers);


            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object Medication) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, Medication);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object Medication);


    }
@Override
    public void onItemClick(View view, int position) {
        //Toast.makeText(getActivity(), "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

    public void onBtnClick(View view, int position) {
        //Toast.makeText(getActivity(), "You want to delete " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
        //delete med from recycler list
        Medication.remove(position);
        //delete med from json-Arraylist
        Medication_Answers.remove(position);
        adapter.notifyItemRemoved(position);
    }
    public void getMedValues(){
        Med.put(MedName.getText().toString());
        Med.put(MedDose.getText().toString());
        Med.put(Float.valueOf(String.valueOf(NumberpickerValues[MedMorning.getValue()])));
        Med.put(Float.valueOf(String.valueOf(NumberpickerValues[MedNoon.getValue()])));
        Med.put(Float.valueOf(String.valueOf(NumberpickerValues[MedEvening.getValue()])));
        Med.put(Float.valueOf(String.valueOf(NumberpickerValues[MedNight.getValue()])));
        Med.put(MedNote.getText().toString());
        Medication_Answers.put(Med);
        //Medication_Answers.add(Med.toString());

        item =  MedName.getText() + " " + MedDose.getText() + ": " + NumberpickerValues[MedMorning.getValue()] + " - " + NumberpickerValues[MedNoon.getValue()]+ " - " + NumberpickerValues[MedEvening.getValue()]+ " - " + NumberpickerValues[MedNight.getValue()] + " | " + MedNote.getText();
        //delete temp values
        //Med.clear();
        Med = new JSONArray();
        MedName.getText().clear();
        MedDose.getText().clear();
        MedNote.getText().clear();
        MedMorning.setValue(0);
        MedNoon.setValue(0);
        MedEvening.setValue(0);
        MedNight.setValue(0);
    };

    public void prepareNumberPickers(){
        MedMorning.setMinValue(0);
        MedMorning.setMaxValue(NumberpickerValues.length - 1);
        MedMorning.setDisplayedValues(NumberpickerValues);
        MedMorning.setWrapSelectorWheel(true);
        setDividerColor(MedMorning, ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
        MedNoon.setMinValue(0);
        MedNoon.setMaxValue(NumberpickerValues.length - 1);
        MedNoon.setDisplayedValues(NumberpickerValues);
        MedNoon.setWrapSelectorWheel(true);
        setDividerColor(MedNoon, ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
        MedEvening.setMinValue(0);
        MedEvening.setMaxValue(NumberpickerValues.length - 1);
        MedEvening.setDisplayedValues(NumberpickerValues);
        MedEvening.setWrapSelectorWheel(true);
        setDividerColor(MedEvening, ContextCompat.getColor(getActivity(), R.color.MHH_Rot));
        MedNight.setMinValue(0);
        MedNight.setMaxValue(NumberpickerValues.length - 1);
        MedNight.setDisplayedValues(NumberpickerValues);
        MedNight.setWrapSelectorWheel(true);
        setDividerColor(MedNight, ContextCompat.getColor(getActivity(), R.color.MHH_Rot));


    }
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}

package com.mhh.sensationmapper.Questionnaires;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.R;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Questionnaire_MIDAS.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Questionnaire_MIDAS#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Questionnaire_MIDAS extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String tag = "MIDAS";
    private OnFragmentInteractionListener mListener;
    public ArrayList MIDAS_Answers = new ArrayList();
    public NumberPicker[] MIDAS_Questions = new NumberPicker[7];
    public Questionnaire_MIDAS() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Questionnaire_MIDAS.
     */
    // TODO: Rename and change types and number of parameters
    public static Questionnaire_MIDAS newInstance(String param1, String param2) {
        Questionnaire_MIDAS fragment = new Questionnaire_MIDAS();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_questionnaire_midas, container, false);

        Button MIDAS_save = (Button)view.findViewById(R.id.MIDAS_save);

        MIDAS_Questions[0] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q01);
        MIDAS_Questions[1] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q02);
        MIDAS_Questions[2] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q03);
        MIDAS_Questions[3] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q04);
        MIDAS_Questions[4] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q05);
        MIDAS_Questions[5] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q06);
        MIDAS_Questions[6] = (NumberPicker) view.findViewById(R.id.numberPicker_MIDAS_Q07);
        prepareNumberPickers();


        MIDAS_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < 7; i++) {
                    /*if(MIDAS_Questions[i].getValue() == -1){
                        Toast.makeText(getActivity(), R.string.err_answer_required, Toast.LENGTH_LONG).show();
                        MIDAS_Answers.clear();
                        return;}*/
                    //Textausgabe Antwort-ID
//                    else if (MIDAS_Questions[i].getValue() != -1){
                        MIDAS_Answers.add(MIDAS_Questions[i].getValue());
//                        }

                    //else {MIDAS_Answers.add("-");}

                    //Textausgabe Antwort
//                    int MIDAS_QID = MIDAS_Questions[i].getCheckedRadioButtonId();
//                    MIDAS_Answers.add(((RadioButton) MIDAS_Questions[i].findViewById(MIDAS_QID)).getText().toString());
                }
                JSONArray jsonArray = new JSONArray(MIDAS_Answers);
                onButtonPressed(tag, jsonArray);
                //hide keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //int MIDAS_Q01_id = MIDAS_Q01.getCheckedRadioButtonId();
                //String MIDAS_Q01_A = ((RadioButton) MIDAS_Q01.findViewById(MIDAS_Q01_id)).getText().toString();


/*                if(userInput.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "User input value must be filled", Toast.LENGTH_LONG).show();
                    return;
                }*/
                //userData = userInput.getText().toString();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String tag, Object MIDAS) {
        if (mListener != null) {
            mListener.onFragmentInteraction(tag, MIDAS);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String tag, Object data);


    }
    public void prepareNumberPickers(){
        for (int i = 0; i < 6; i++) {
            MIDAS_Questions[i].setMaxValue(90);
            MIDAS_Questions[i].setMinValue(0);
            MIDAS_Questions[i].setWrapSelectorWheel(true);
            setDividerColor(MIDAS_Questions[i], ContextCompat.getColor(getActivity(), R.color.MHH_Rot));

        }
        MIDAS_Questions[6].setMaxValue(10);
        MIDAS_Questions[6].setMinValue(0);
        MIDAS_Questions[6].setWrapSelectorWheel(true);
        setDividerColor(MIDAS_Questions[6], ContextCompat.getColor(getActivity(), R.color.MHH_Rot));

    }
    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}

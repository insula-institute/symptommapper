/*
package com.mhh.sensationmapper;


import android.content.Context;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

*/
/**
 * Created by Tofeeq on 30/07/14.
 *//*


abstract public class Question implements Parcelable {

    abstract public int getColorIntensity();
    abstract public String getDescription(Context context);

    private int id;
    private int color;
    private long starttime;
    private int type;

    public Question(int id, int color, long starttime) {
        this.id = id;
        this.color = color;
        this.starttime = starttime;
    }

    public int getId() {
        return id;
    }
    public int getColor() {
        return color;
    }
    public long getStarttime() {
        return starttime;
    }

    public JSONObject toJSON(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("color", color);
            jsonObject.put("starttime", starttime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public Question(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            color = jsonObject.getInt("color");
            starttime = jsonObject.getLong("starttime");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return String.format("%d;%d;%d;", id, color, starttime);
    }

    public static String readStringFromFile(File file) {
        FileInputStream fIn;
        try {
            fIn = new FileInputStream(file);
        } catch (IOException e) {
            return "";
        }

        InputStreamReader ir = new InputStreamReader(fIn);
        BufferedReader br = new BufferedReader(ir);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            fIn.close();
        } catch (IOException e) {
            return "";
        }
        return text.toString();
    }

    public static void writeStringToFile(String s, File file, Boolean append) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            FileOutputStream fOut = new FileOutputStream(file, append);
            OutputStreamWriter writer = new OutputStreamWriter(fOut);
            BufferedWriter fbw = new BufferedWriter(writer);
            fbw.write(s);
            fbw.flush();
            fbw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendToFile(String filename) {

        String extStorage = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorage, filename);
        String content = readStringFromFile(file);

        JSONArray a;
        try {
            a = new JSONArray(content);
        } catch (JSONException e) {
            a = new JSONArray();
        }

        a.put(this.toJSON());

        writeStringToFile(a.toString(), file, false);
    }

        */
/* everything below here is for implementing Parcelable *//*


    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(color);
        out.writeLong(starttime);
    }

    // example constructor that takes a Parcel and gives you an object populated with it's values
    protected Question(Parcel in) {
        id = in.readInt();
        color = in.readInt();
        starttime = in.readLong();
    }
}

*/

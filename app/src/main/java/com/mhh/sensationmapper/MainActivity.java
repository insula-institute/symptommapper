package com.mhh.sensationmapper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ResourceCursorAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mhh.sensationmapper.config.AuthorManager;
import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.config.MultipartUtility;
import com.mhh.sensationmapper.config.ServerConnection;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;
import com.warkiz.tickseekbar.TickSeekBar;
import com.mhh.sensationmapper.utilities.HashMapAdapter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity{
    DBHelper dbh;
    ConfigManager c;
    ListView patientLst;
    EditText txtFilter;
    ListView recordingLst;
    int sex;
    String[] sex_list;
    String[] author_list;
    String responseString;
    String PatientPath;
    String RecordingPath;
    String serverURL;

    //private ArrayAdapter listAdapter;
    class PatientCursorAdapter extends ResourceCursorAdapter {

        public PatientCursorAdapter(Context context, int layout, Cursor c, int flags) {
            super(context, layout, c, flags);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView txtName = (TextView) view.findViewById(R.id.txtName);
            txtName.setText(cursor.getString(1));

            TextView txtDob = (TextView) view.findViewById(R.id.txtDob);
            Date dob = dbh.sqlToDate(cursor.getString(2));
            txtDob.setText(dbh.formatDate(context, dob));

            TextView txtSex = (TextView) view.findViewById(R.id.txtSex);
            int sex_id = cursor.getInt(3);
            if (sex_id < sex_list.length) {
                txtSex.setText(sex_list[sex_id]);
            }
        }
    }

    class RecordingCursorAdapter extends ResourceCursorAdapter {

        public RecordingCursorAdapter(Context context, int layout, Cursor c, int flags) {
            super(context, layout, c, flags);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView txtNum = (TextView) view.findViewById(R.id.txtNum);
            txtNum.setText(String.format(getString(R.string.recording_num), cursor.getInt(1) + 1));

            TextView txtInfo = (TextView) view.findViewById(R.id.txtInfo);
            String datetime = dbh.formatDateTime(context, cursor.getString(2));
            int author_id = cursor.getInt(3);
            Log.d("Upload", "Author: " + author_id);

            String author = c.getAuthorManager().getAuthors().get(author_id).getName();
            Log.d("Upload", "Authorn: " + author);

            //String author = (author_id < author_list.length) ? author_list[author_id] : getString(R.string.unknown);

            //int count = cursor.getInt(5);
            //switch (author_id) {
            //    case 0:
            //        fmt = getString((count == 1) ? R.string.recording_info_patient : R.string.recording_info_patient_pl); break;
            //    case 1:
            //        fmt = getString((count == 1) ? R.string.recording_info_doctor : R.string.recording_info_doctor_pl); break;
            //    default:
            //        fmt = getString(R.string.recording_info_general); break;
            //}
            txtInfo.setText(String.format("von %s am %s", author, datetime));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.app_name);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMainActivity);
        setSupportActionBar(toolbar);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        // exit button
/*        ImageButton exitBtn = (ImageButton) findViewById(R.id.btn_exit);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.exit(0);
            }
        });*/

        // app
        SensationMapper app = (SensationMapper) this.getApplication();

        // init db
        dbh = app.getDBHelper();
        SQLiteDatabase db = dbh.getReadableDatabase();

        // read config
        c = new ConfigManager("SymptomMapper/Config/config.json");
        app.setConfigManager(c);

        // get resources
        sex_list = getResources().getStringArray(R.array.sex_list);
        author_list = getResources().getStringArray(R.array.author_list);


        // patient list
        patientLst = (ListView) findViewById(R.id.patient_list);
        Cursor curPatient = db.query("patients", null, null, null, null, null, null, null);
        PatientCursorAdapter patientAdapter = new PatientCursorAdapter(this, R.layout.listrow_patient, curPatient, 0);
        patientLst.setAdapter(patientAdapter);

        // patient list filter
        txtFilter = (EditText) findViewById(R.id.txtFilter);
        txtFilter.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                ((PatientCursorAdapter) patientLst.getAdapter()).getFilter().filter(s.toString());
            }
        });
        patientAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                String value = "%" + constraint.toString() + "%";
                SQLiteDatabase db = dbh.getReadableDatabase();
                return db.query("patients", null, "name LIKE ? ", new String[]{value}, null, null, null, null);
            }
        });

        // on patient click
        patientLst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // get patient row
                Cursor c = ((PatientCursorAdapter) parent.getAdapter()).getCursor();
                c.moveToPosition(position);

                // update recording list
                SQLiteDatabase db = dbh.getReadableDatabase();
                Cursor cursor = db.query("recordings", null, " pid = ?", new String[] { c.getString(0) }, null, null, null, null);
                ((RecordingCursorAdapter) recordingLst.getAdapter()).changeCursor(cursor);
                db.close();

                // remove focus from filter text field
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtFilter.getWindowToken(), 0);
                txtFilter.clearFocus();
            }
        });

        // recording list
        Patient p = getCheckedPatient();
        String pid = (p == null) ? "null" : String.valueOf(p.getId());
        recordingLst = (ListView) findViewById(R.id.entry_list);
        Cursor curRecording = db.query("recordings", null, " pid = ?", new String[] { pid }, null, null, null, null);
        RecordingCursorAdapter recordingAdapter = new RecordingCursorAdapter(this, R.layout.listrow_entry, curRecording, 0);
        recordingLst.setAdapter(recordingAdapter);

        db.close();

        // on recording click
        recordingLst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // maximum of three selectable items
                if (getCheckedRecordings().size() > 2) {
                    recordingLst.setItemChecked(position, false);
                }
            }
        });


        // new patient button
        Button btnNewPatient = (Button) findViewById(R.id.btn_new_patient);
        btnNewPatient.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showPatientDialog();
            }
        });

        // search patient button
        /*
        Button btnSearchPatient = (Button) findViewById(R.id.btn_search_patient);
        btnSearchPatient.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showSearchPatientDialog();
            }
        });
         */

        // start new recording
        Button btnNewEntry = (Button) findViewById(R.id.btn_new_entry);
        btnNewEntry.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Patient patient = getCheckedPatient();
 
                if (patient != null) {
                    List<AuthorManager.Author> authors = c.getAuthorManager().getAuthors();
                    if (authors.size() == 1) {
                        // get num
                        int num = recordingLst.getAdapter().getCount();
                        // get mode
                        String mode = authors.get(0).getMode();
                        // prelim non db recording
                        Recording recording = new Recording(num, new Date(), 0, patient.getId(), 0);

                        startDrawActivity(patient, recording, mode);
                    } else {
                        showEntryDialog(patient);
                    }
                }
            }
        });

        // start questionnaire
        Button btnQuestionnaire = (Button) findViewById(R.id.btn_questionnaire);
        btnQuestionnaire.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Patient patient = getCheckedPatient();

                if (patient != null) {
/*                    List<AuthorManager.Author> authors = c.getAuthorManager().getAuthors();
                    if (authors.size() == 1) {
                        // get num
                        int num = recordingLst.getAdapter().getCount();
                        // get mode
                        String mode = authors.get(0).getMode();
                        // prelim non db recording
                        Recording recording = new Recording(num, new Date(), 0, patient.getId(), 0);

                        startQuestionnaireActivity (patient, recording, mode);
                    } else {*/
                        showEntryDialog(patient);
                    }
                }
           }
        //}
        );


        final String charset = "UTF-8";

        //final ArrayList myFormDataArray = new ArrayList();
        /*        private String getPatientPath() {
            return String.format("SymptomMapper/Subjects/p%d", patient.getId());
        }

        private String getRecordingPath() {
            return String.format("%s/n%d_a%d", getPatientPath(), recording.getNum(), recording.getAuthor());
        }*/
        final String extStorage = Environment.getExternalStorageDirectory().toString();


        // start fileupload
        /*
        Button btn_fileupload = (Button) findViewById(R.id.btn_upload_files);
        btn_fileupload.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                   Patient patient = getCheckedPatient();
                   PatientPath = String.format("SymptomMapper/Subjects/p%d", patient.getId());
                   ArrayList<Recording> recordings = getCheckedRecordings();

                   if (patient != null && recordings != null && !recordings.isEmpty()) {
                        Recording recording = recordings.get(0);
                        RecordingPath = String.format("%s/n%d_a%d", PatientPath, recording.getNum(), recording.getAuthor());
                        int author_id = recording.getAuthor();
                        String modename = c.getAuthorManager().getAuthors().get(author_id).getMode();
                        Log.d("Upload", "Mode_name: " + modename);
                        ModeManager.Mode mode = c.getModeManager().getMode(modename);
                        try {
                            serverURL = mode.getServerURL();
                            Log.d("Upload", "serverURL: " + serverURL);
                        } catch (Exception e) {
                            // This will catch any exception, because they are all descended from Exception
                            System.out.println("serverURL Error: " + e.getMessage());
                        }
                        if (serverURL != null) {
                            try {
                                ServerConnection serverConnection = new ServerConnection(serverURL);
                                Toast.makeText(MainActivity.this, R.string.uploadInProgress, Toast.LENGTH_LONG).show();
                                serverConnection.uploadFiles(patient, RecordingPath);
                                String response = serverConnection.getResponseString();
                                if (response.equals("Data uploaded successfully.")) {
                                    Toast.makeText(MainActivity.this, R.string.upload_success, Toast.LENGTH_LONG).show();
                                } else {
                                   Toast.makeText(MainActivity.this, R.string.upload_false, Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                // This will catch any exception, because they are all descended from Exception
                                System.out.println("serverConnection Error: " + e.getMessage());
                                Toast.makeText(MainActivity.this, R.string.upload_false, Toast.LENGTH_LONG).show();
                            }
                        }
                   } else{
                       Toast.makeText(MainActivity.this, R.string.err_recording_selection, Toast.LENGTH_LONG).show();
                   }
                }
        });
        */


        // View recordings button
        Button btnView = (Button) findViewById(R.id.btn_view_entry);
        btnView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Patient patient = getCheckedPatient();
                ArrayList<Recording> recordings = getCheckedRecordings();

                if (patient != null && recordings != null && !recordings.isEmpty()) {
                    startViewActivity(patient, recordings);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /*case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;*/

            case R.id.action_exit:
                System.exit(0);
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                return true;
            case R.id.action_about:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle(R.string.licenses_title);
                //alert.setIcon()
                alert.setView(LayoutInflater.from(this).inflate(R.layout.dialog_about,null));
                alert.setPositiveButton("OK",null);
                alert.show();
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    public Patient getCheckedPatient() {
        int pos = patientLst.getCheckedItemPosition();
        if (pos > -1) {
            Cursor c = ((PatientCursorAdapter) patientLst.getAdapter()).getCursor();
            c.moveToPosition(pos);
            return new Patient(c);
        } else {
            return null;
        }
    }

    public ArrayList<Recording> getCheckedRecordings() {
        SparseBooleanArray checked = recordingLst.getCheckedItemPositions();
        ArrayList<Recording> recordings = new ArrayList<>();
        Cursor c = ((RecordingCursorAdapter) recordingLst.getAdapter()).getCursor();

        for (int i = 0; i < recordingLst.getAdapter().getCount(); i++) {
            if (checked.get(i)) {
                c.moveToPosition(i);
                recordings.add(new Recording(c));
            }
        }

        return recordings;
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onRestart(){
        super.onRestart();

        if (patientLst != null) {
            int position = patientLst.getCheckedItemPosition();
            if (position > -1) {
                patientLst.performItemClick(patientLst.getAdapter().getView(position, null, null),
                        position, patientLst.getAdapter().getItemId(position));
            }
        }

    }

    @Override
    public void onStop(){
        super.onStop();
    }



    private void showPatientDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View popupView = inflater.inflate(R.layout.dialog_new_patient, null);
        builder.setView(popupView);

        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // get name
                EditText txtPatientName = (EditText) popupView.findViewById(R.id.patientName);
                String name = txtPatientName.getText().toString();

                // get date
                EditText txtPatientDate = (EditText) popupView.findViewById(R.id.patientDate);
                Context context = ((Dialog) dialog).getContext();
                Date date = dbh.parseDate(context, txtPatientDate.getText().toString());

                // get sex
                RadioGroup radioGrp = (RadioGroup) popupView.findViewById(R.id.rgrp_sex);
                int btnID = radioGrp.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) radioGrp.findViewById(btnID);
                int sex = radioGrp.indexOfChild(radioButton);
                Log.d("Sex-Nr", Integer.toString(sex));

                // insert patient
                Patient patient = new Patient(name, date, sex);
                dbh.addPatient(patient);

                //update view
                SQLiteDatabase db = dbh.getReadableDatabase();
                Cursor cursor = db.query("patients", null, null, null, null, null, null, null);
                ((PatientCursorAdapter) patientLst.getAdapter()).changeCursor(cursor);
                txtFilter.getText().clear();
                //txtFilter.setEnabled(false);
                txtFilter.clearFocus();
                ((PatientCursorAdapter) patientLst.getAdapter()).notifyDataSetChanged();

                // select new item
                ((Dialog) dialog).setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        // select new patient
                        int position = patientLst.getCount() - 1;
                        patientLst.setItemChecked(position, true);
                        patientLst.smoothScrollToPosition(position);
                        patientLst.performItemClick(patientLst.getAdapter().getView(position, null, null),
                                position, patientLst.getAdapter().getItemId(position));
                    }
                });

            }

        });
        builder.setNegativeButton(R.string.Cancel, null);


        AlertDialog ad = builder.create();
        ad.show();
    }

    private void showSearchPatientDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View popupView = inflater.inflate(R.layout.dialog_search_patient, null);
        builder.setView(popupView);

        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Get surname, forename and patient number
                EditText txtSurname = (EditText) popupView.findViewById(R.id.patientSurname);
                String Surname = txtSurname.getText().toString();
                if (Surname.equals("")) {
                    Surname = null;
                }
                EditText txtForename = (EditText) popupView.findViewById(R.id.patientForename);
                String Forename = txtForename.getText().toString();
                if (Forename.equals("")) {
                    Forename = null;
                }
                EditText txtPatientNumber = (EditText) popupView.findViewById(R.id.patientNumber);
                String PatientNumber = txtPatientNumber.getText().toString();
                if (PatientNumber.equals("")) {
                    PatientNumber = null;
                }

                //Send data to server and get response
                //TODO Correct this! See upload button for a better solution.
                /*
                String modename = c.getAuthorManager().getAuthors().get(0).getMode();
                ModeManager.Mode mode = c.getModeManager().getMode(modename);
                try {
                    serverURL = mode.getServerURL();
                    Log.d("Upload", "serverURL: " + serverURL);
                } catch (Exception e) {
                    // This will catch any exception, because they are all descended from Exception
                    System.out.println("serverURL Error: " + e.getMessage());
                }
                if (serverURL != null) {
                    ServerConnection serverConnection = new ServerConnection(serverURL);
                    serverConnection.searchPatient(Surname, Forename, PatientNumber);
                    final Map<String, String> patients = serverConnection.getPatients();

                    //For Ddebugging without internet connection
                    //Map<String, String> patients = new HashMap<String, String>();
                    //patients.put("1234567890", "Tester, Toni *1900-01-01");
                    //patients.put("2345678901", "Tester, Tonina *1910-01-01");

                    //Generate selection page with possible matches
                    showSelectPatientDialog(patients);
                } else {
                Toast.makeText(MainActivity.this, R.string.err_recording_selection, Toast.LENGTH_LONG).show();
                }
                 */
            }

        });
        builder.setNegativeButton(R.string.Cancel, null);

        AlertDialog ad = builder.create();
        ad.show();
    }

    private void showSelectPatientDialog(final Map<String, String> patients) {
        final String[] PatientNumber = new String[1];
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View popupView = inflater.inflate(R.layout.dialog_select_patient, null);

        //Populate ListView with entries of HashMap
        ListView selectPatientLst = (ListView) popupView.findViewById(R.id.select_patient_list);
        HashMapAdapter adapter = new HashMapAdapter(patients);
        selectPatientLst.setAdapter(adapter);

        builder.setView(popupView);

        // On patient click
        selectPatientLst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get patient number
                PatientNumber[0] = ((HashMapAdapter) parent.getAdapter()).getItem(position).getKey();
            }
        });

        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Get Background image
                //TODO: remove predefined background image. Prevent action if none is selected.
                RadioGroup radioGrp = (RadioGroup) popupView.findViewById(R.id.rgrp_bgimage);
                int btnID = radioGrp.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) radioGrp.findViewById(btnID);
                int bgImage = radioGrp.indexOfChild(radioButton);
                Log.d("Sex-Nr", Integer.toString(bgImage));

                //Get server URL
                //TODO Correct this! See upload button for a better solution.
                String modename = c.getAuthorManager().getAuthors().get(0).getMode();
                ModeManager.Mode mode = c.getModeManager().getMode(modename);
                try {
                    serverURL = mode.getServerURL();
                    Log.d("Upload", "serverURL: " + serverURL);
                } catch (Exception e) {
                    // This will catch any exception, because they are all descended from Exception
                    System.out.println("serverURL Error: " + e.getMessage());
                }

                //Send data to server and get response (ID)
                //TODO: Check if there is connection to the server (Maybe in ServerConnection).
                if (serverURL != null && PatientNumber[0] != null) {
                    ServerConnection serverConnection = new ServerConnection(serverURL);
                    serverConnection.selectPatient(PatientNumber[0]);
                    final String ID = serverConnection.getResponseString();

                    //Insert patient into database (without birth date)
                    Context context = ((Dialog) dialog).getContext();
                    Date date = dbh.parseDate(context, "42"); // To avoid putting any date in the database
                    Patient patient = new Patient(ID, date, bgImage);
                    dbh.addPatient(patient);

                    //Update view
                    SQLiteDatabase db = dbh.getReadableDatabase();
                    Cursor cursor = db.query("patients", null, null, null, null, null, null, null);
                    ((PatientCursorAdapter) patientLst.getAdapter()).changeCursor(cursor);
                    txtFilter.getText().clear();
                    txtFilter.clearFocus();
                    ((PatientCursorAdapter) patientLst.getAdapter()).notifyDataSetChanged();

                    //Select newly created item
                    ((Dialog) dialog).setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            // select new patient
                            int position = patientLst.getCount() - 1;
                            patientLst.setItemChecked(position, true);
                            patientLst.smoothScrollToPosition(position);
                            patientLst.performItemClick(patientLst.getAdapter().getView(position, null, null),
                                    position, patientLst.getAdapter().getItemId(position));
                        }
                    });

                    //Generate selection page with possible matches
                    //showSelectPatientDialog(patients);
                } else {
                    Toast.makeText(MainActivity.this, R.string.err_recording_selection, Toast.LENGTH_LONG).show();
                }
            }

        });

        builder.setNegativeButton(R.string.Cancel, null);

        AlertDialog ad = builder.create();
        ad.show();
    }



    private void showEntryDialog(final Patient patient) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupView = inflater.inflate(R.layout.dialog_new_entry, null);
        builder.setView(popupView);
        //TILL checkbox direktanzeige
/*        CheckBox checkBox = (CheckBox) popupView.findViewById(R.id.checkBox_q1);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getApplicationContext(), "Checkbox 1 checked", Toast.LENGTH_LONG).show();
            }
        });*/
        builder.setNegativeButton(R.string.Cancel, null);
        final AlertDialog ad = builder.create();

        LinearLayout layout = (LinearLayout) popupView.findViewById(R.id.layout_new_entry);

        final List<AuthorManager.Author> authors = c.getAuthorManager().getAuthors();
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        p.weight = 1;
        p.gravity = Gravity.CENTER_HORIZONTAL;
        final RadioGroup radioGrp = (RadioGroup) popupView.findViewById(R.id.rgrp_pgrp);
        final LinearLayout QuestionnaireSelection = (LinearLayout) popupView.findViewById(R.id.Questionnaire_selection);
        final TextView textQuestionnaireSelection = (TextView) popupView.findViewById(R.id.textView_selectQuestionnaires);
        radioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                switch (checkedId) {
                    case R.id.btn_pgrp_s:
                        textQuestionnaireSelection.setVisibility(View.VISIBLE);
                        QuestionnaireSelection.setVisibility(View.VISIBLE);
                        break;
                    default:
                        textQuestionnaireSelection.setVisibility(View.GONE);
                        QuestionnaireSelection.setVisibility(View.GONE);
                        break;
                }}});

        for (int i=0; i<authors.size(); i++) {
            final int aid = i;
            Button b = new Button(this);
            b.setLayoutParams(p);
            b.setText(authors.get(i).getName());
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // get num
                    int num = recordingLst.getAdapter().getCount();
                    // get mode
                    String mode = authors.get(aid).getMode();

                    // get questionnaires (TILL) checkbox auf ok
                    int btnID = radioGrp.getCheckedRadioButtonId();
                    String patgroup = getResources().getResourceEntryName(btnID);

                    //individual questionnaire checkboxes
                    CheckBox checkBox_Data = (CheckBox) popupView.findViewById(R.id.checkBox_Data);
                    CheckBox checkBox_Medication = (CheckBox) popupView.findViewById(R.id.checkBox_Medication);
                    //CheckBox checkBox_ADS = (CheckBox) popupView.findViewById(R.id.checkBox_ADS);
                    CheckBox checkBox_PHQ_9 = (CheckBox) popupView.findViewById(R.id.checkBox_PHQ_9);
                    CheckBox checkBox_MIDAS = (CheckBox) popupView.findViewById(R.id.checkBox_MIDAS);
                    CheckBox checkBox_MPSS = (CheckBox) popupView.findViewById(R.id.checkBox_MPSS);
                    CheckBox checkBox_PDI = (CheckBox) popupView.findViewById(R.id.checkBox_PDI);
                    CheckBox checkBox_PDS_1 = (CheckBox) popupView.findViewById(R.id.checkBox_PDS_1);
                    //CheckBox checkBox_SCL_27 = (CheckBox) popupView.findViewById(R.id.checkBox_SCL_27);
                    CheckBox checkBox_PHQ_15 = (CheckBox) popupView.findViewById(R.id.checkBox_PHQ_15);
                    CheckBox checkBox_PHQ_Stress = (CheckBox) popupView.findViewById(R.id.checkBox_PHQ_Stress);
                    //CheckBox checkBox_STAI_X2 = (CheckBox) popupView.findViewById(R.id.checkBox_STAI_X2);
                    CheckBox checkBox_GAD_7 = (CheckBox) popupView.findViewById(R.id.checkBox_GAD_7);
                    CheckBox checkBox_VAS = (CheckBox) popupView.findViewById(R.id.checkBox_VAS);

                    ArrayList Questionnaires = new ArrayList();
                    /*Fragment fragment_STAI_X2 = new Questionnaire_STAI_X2();
                    Fragment fragment_ADS = new Questionnaire_ADS();
                    Fragment fragment_PDI = new Questionnaire_PDI();
                    Fragment fragment_VAS = new Questionnaire_VAS();
                    Fragment fragment_SCL_27 = new Questionnaire_SCL_27();
                    Fragment fragment_MPSS = new Questionnaire_MPSS();
                    Fragment fragment_PDS_1 = new Questionnaire_PDS_1();
                    Fragment fragment_MIDAS = new Questionnaire_MIDAS();
                    Fragment fragment_Data = new Questionnaire_Data();
                    Fragment fragment_Medication = new Questionnaire_Medication();*/
                    sex = patient.getSex();
                    switch (btnID) {
                        case R.id.btn_pgrp_n:
                            if (sex == 3){Questionnaires.addAll(Arrays.asList(0, 2, 4, 5, 6, 7, 8, 9, 10, 3));}
                            else{Questionnaires.addAll(Arrays.asList(0, 2, 4, 5, 6, 7, 8, 9, 10));}
                            break;
                        case R.id.btn_pgrp_r:
                            if(sex == 3){Questionnaires.addAll(Arrays.asList(0, 2, 5, 7, 8, 9, 10,  3));}
                            else{Questionnaires.addAll(Arrays.asList(0, 2, 5, 7, 8, 9, 10));}
                            break;
                        case R.id.btn_pgrp_s:
                            if (checkBox_Data.isChecked()){
                                Questionnaires.add(0); }
                            if (checkBox_Medication.isChecked()){
                                Questionnaires.add(1);}
                            if (checkBox_PHQ_9.isChecked()){
                                Questionnaires.add(2);}
                            if (checkBox_MIDAS.isChecked()){
                                Questionnaires.add(3);}
                            if (checkBox_MPSS.isChecked()){
                                Questionnaires.add(4);}
                            if (checkBox_PDI.isChecked()){
                                Questionnaires.add(5);}
                            if (checkBox_PDS_1.isChecked()){
                                Questionnaires.add(6);}
                            if (checkBox_PHQ_15.isChecked()){
                                Questionnaires.add(7);}
                            if (checkBox_PHQ_Stress.isChecked()){
                                Questionnaires.add(8);}
                            if (checkBox_GAD_7.isChecked()){
                                Questionnaires.add(9);}
                            if (checkBox_VAS.isChecked()){
                                Questionnaires.add(10);}
                            /*if (checkBox_STAI_X2.isChecked()){
                                Questionnaires.add(11);}
                            if (checkBox_ADS.isChecked()){
                                Questionnaires.add(12);}
                            if (checkBox_SCL_27.isChecked()){
                                Questionnaires.add(13);}
                            */
                            break;
                    };
                    Log.i("sex", String.valueOf(sex));
                    Log.i("QuestArray praeintent", Questionnaires.toString());

                    // prelim non db recording
                    Recording recording = new Recording(num, new Date(), aid, patient.getId(), 0);

//                    if(!patgroup.equals("btn_pgrp_s")){
                    startAgreementActivity(patient, recording, mode, patgroup, Questionnaires);
//                    } else {
                        //startDrawActivity(patient, recording, mode);
                    //}

                    // old for checkboxes
/*                    if(cb_1.isChecked() || cb_2.isChecked() || cb_3.isChecked()){
                        startQuestionnaireActivity(patient, recording, mode, q1, q2, q3);
                    } else {

                        startDrawActivity(patient, recording, mode);}*/
                    ad.dismiss();
                }
            });

            layout.addView(b);
        }

        ad.show();
    }

    private void startViewActivity (Patient patient, ArrayList<Recording> recordings) {
        Intent intent = new Intent(this, ViewActivity.class);

        intent.putExtra("patient", patient);
        intent.putParcelableArrayListExtra("recordings", recordings);

        startActivity(intent);
    }

    private void startDrawActivity (Patient patient, Recording recording, String mode) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, InstructionActivity_new.class);
        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);

        startActivity(intent);
    }

    private void startQuestionnaireActivity (Patient patient, Recording recording, String mode, String patgroup, ArrayList questionnaires) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, QuestionnairePainActivity.class);

        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);
        intent.putExtra("patgroup", patgroup);
        intent.putParcelableArrayListExtra("questionnaires", questionnaires);
//        intent.putExtra("q1", q1);
//        intent.putExtra("q2", q2);
//        intent.putExtra("q3", q3);
        startActivity(intent);
    }
    private void startAgreementActivity (Patient patient, Recording recording, String mode, String patgroup, ArrayList questionnaires) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, AgreementActivity.class);

        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);
        intent.putExtra("patgroup", patgroup);
        intent.putParcelableArrayListExtra("questionnaires", questionnaires);
//        intent.putExtra("q1", q1);
//        intent.putExtra("q2", q2);
//        intent.putExtra("q3", q3);
        startActivity(intent);
    }
}

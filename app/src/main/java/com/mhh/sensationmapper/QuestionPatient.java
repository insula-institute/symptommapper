/*
package com.mhh.sensationmapper;

import android.content.Context;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by sbrink on 13.07.16.
 *//*


public class QuestionPatient extends Question {

    private List<String> sensations;
    private String customSensation;
    private int intensity;
    private List<String> depths;
    private int burden;

    public QuestionPatient(int id, int color, long starttime, List<String> sensations, String customSensation, int intensity, List<String> depths, int burden) {
        super(id, color, starttime);
        this.sensations = sensations;
        this.customSensation = customSensation;
        this.intensity = intensity;
        this.depths = depths;
        this.burden = burden;
    }

    public List<String> getSensations() {
        return this.sensations;
    }
    public String getCustomSensation() {
        return this.customSensation;
    }
    public int getIntensity() {
        return this.intensity;
    }
    public List<String> getDepths() {
        return this.depths;
    }
    public int getBurden() {
        return this.burden;
    }

    @Override
    public int getColorIntensity() {
        return this.intensity;
    }
    @Override
    public String toString() {
        return super.toString() + String.format("patient;%s;%s;%d;%s;%d;", TextUtils.join(",", sensations), customSensation, intensity, TextUtils.join(",", depths), burden);
    }

    public JSONObject toJSON(){

        JSONObject jsonObject = super.toJSON();
        try {
            JSONArray jsonArray1 = new JSONArray();
            for (int i=0; i<sensations.size(); i++) {
                jsonArray1.put(sensations.get(i));
            }
            jsonObject.put("sensations", jsonArray1);
            jsonObject.put("customSensation", customSensation);
            jsonObject.put("intensity", intensity);
            JSONArray jsonArray2 = new JSONArray();
            for (int i=0; i<depths.size(); i++) {
                jsonArray2.put(depths.get(i));
            }
            jsonObject.put("depths", jsonArray1);
            jsonObject.put("burden", burden);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public QuestionPatient(JSONObject jsonObject) {
        super(jsonObject);
        try {
            sensations = new ArrayList<>();
            JSONArray jsonArray1 = jsonObject.getJSONArray("sensations");
            for (int i=0; i<jsonArray1.length(); i++) {
                sensations.add(jsonArray1.getString(i));
            }
            customSensation = jsonObject.getString("customSensation");
            intensity = jsonObject.getInt("intensity");
            depths = new ArrayList<>();
            JSONArray jsonArray2 = jsonObject.getJSONArray("depths");
            for (int i=0; i<jsonArray2.length(); i++) {
                depths.add(jsonArray2.getString(i));
            }
            burden = jsonObject.getInt("burden");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getDescription(Context context) {
        // string fragments
        List<String> l = new ArrayList<>();
        // sensations
        String str = TextUtils.join(", ", sensations);
        // custom
        if (customSensation != null && !customSensation.isEmpty()) {
            if (str.isEmpty()) {
                str = String.format("(%s)", customSensation);
            } else {
                str += String.format(", (%s)", customSensation);
            }
        }
        l.add(str);
        // intensity
        l.add(String.format("%s: %d/100", context.getResources().getString(R.string.Intensity), intensity));
        // depth
        if (!depths.isEmpty()) {
            l.add(TextUtils.join(", ", depths));
        }
        // burden
        if (burden > -1) {
            l.add(String.format("%s: %d/100", context.getResources().getString(R.string.Burden), burden));
        }
        return TextUtils.join(" - ", l);

    }

    public static List<QuestionPatient> fromFile(String filename) {

        ArrayList<QuestionPatient> list = new ArrayList<>();

        String extStorage = Environment.getExternalStorageDirectory().toString();
        File file = new File(extStorage, filename);
        String content = readStringFromFile(file);

        JSONArray a;
        try {
            a = new JSONArray(content);
        } catch (JSONException e) {
            a = new JSONArray();
        }

        for (int i=0; i<a.length(); i++) {
            try {
                JSONObject o = a.getJSONObject(i);
                list.add(new QuestionPatient(o));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    */
/* everything below here is for implementing Parcelable *//*


    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);

        out.writeStringList(sensations);
        out.writeString(customSensation);
        out.writeInt(intensity);
        out.writeStringList(depths);
        out.writeInt(burden);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<QuestionPatient> CREATOR = new Parcelable.Creator<QuestionPatient>() {
        public QuestionPatient createFromParcel(Parcel in) {
            return new QuestionPatient(in);
        }

        public QuestionPatient[] newArray(int size) {
            return new QuestionPatient[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with it's values
    private QuestionPatient(Parcel in) {
        super(in);

        sensations = new ArrayList<>(); in.readStringList(sensations);
        customSensation = in.readString();
        intensity = in.readInt();
        depths = new ArrayList<>(); in.readStringList(depths);
        burden = in.readInt();

    }
}
*/

package com.mhh.sensationmapper;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.graphics.ColorUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

import com.mhh.sensationmapper.model.Recording;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewingView extends View {

    private int bodyfigure;
    private int count;

    //private int width = 2*351;        // double of what is in view (why?)
    //private int height = 2*498;

    private Bitmap merged = null;
    //private Canvas mergedCanvas;
    private SparseArray<Bitmap> layers = new SparseArray<>();

    class ParamBackground {
        public Resources res;
        public int id;
        public int width;
        public int height;
    }

    class BitmapWorkerTaskBackground extends AsyncTask<ParamBackground, Void, Bitmap> {
        private ParamBackground param;

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(ParamBackground... params) {
            param = params[0];

            return decodeSampledBitmapFromResource(param.res, param.id, param.width, param.height);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            merged = bitmap;
            count--;
            if (count == -1) {
                invalidate();
            }
        }
    }

    class ParamLayer {
        public String path;
        public int index;
        public int color;
        public int width;
        public int height;
    }

    class BitmapWorkerTaskLayer extends AsyncTask<ParamLayer, Void, Bitmap> {
        private ParamLayer param;

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(ParamLayer... params) {
            param = params[0];

            Bitmap b = decodeSampledBitmapFromFile(param.path, param.width, param.height);
            return (b == null) ? null : applyColorToBitmap(b, param.color);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            layers.put(param.index, bitmap);
            count--;
            if (count == -1) {
                invalidate();
            }
        }
    }

    public ViewingView(Context context) {
        this(context, null);
    }

    public ViewingView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ViewingView, 0, 0);

        try {
            bodyfigure = a.getInteger(R.styleable.ViewingView_bodyfigure, 0);
            //backgroundId = a.getResourceId(R.styleable.ViewingView_backgroundId, 0);
        } finally {
            a.recycle();
        }
    }

    public void setBackgroundLayer() {
        int sex = ((ViewActivity)getContext()).getPatient().getSex();
        int resources[][] = {{R.drawable.androgyn_left_mask, R.drawable.androgyn_front_mask, R.drawable.androgyn_back_mask, R.drawable.androgyn_right_mask},
                {R.drawable.male_left_mask, R.drawable.male_front_mask, R.drawable.male_back_mask, R.drawable.male_right_mask},
                {R.drawable.female_left_mask, R.drawable.female_front_mask, R.drawable.female_back_mask, R.drawable.female_right_mask},
                {R.drawable.androgyn_face_fb_mask, R.drawable.androgyn_face_fb, R.drawable.androgyn_face_lr, R.drawable.androgyn_face_lr_mask}};

        // load background
        BitmapWorkerTaskBackground task = new BitmapWorkerTaskBackground();
        ParamBackground param = new ParamBackground();
        param.res = getResources();
        param.id = resources[sex][bodyfigure];
        param.width = this.getWidth();
        param.height = this.getHeight();
        task.execute(param);
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void putLayer(int index, String path, int color) {
        BitmapWorkerTaskLayer task = new BitmapWorkerTaskLayer();
        ParamLayer param = new ParamLayer();
        param.path = path;
        param.index = index;
        param.color = color;
        param.width = this.getWidth();
        param.height = this.getHeight();
        task.execute(param);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (merged != null) {
            canvas.drawBitmap(merged, 0, 0, null);
        }
        for (int i=0; i<layers.size(); i++) {
            if (layers.valueAt(i) != null) {
                canvas.drawBitmap(layers.valueAt(i), 0, 0, null);
            }
        }
    }

    public static Bitmap applyColorToBitmap(Bitmap b, int color) {
        float[] hsv_new = new float[3];
        Color.colorToHSV(color, hsv_new);

        for (int i=0; i < b.getWidth(); i++) {
            for (int j=0; j < b.getHeight(); j++) {
                int c = b.getPixel(i, j);
                if (c != Color.TRANSPARENT) {
                    float[] hsv = new float[3];
                    Color.colorToHSV(c, hsv);
                    hsv_new[1] = hsv[1];
                    b.setPixel(i, j, Color.HSVToColor(175, hsv_new));
                }
            }
        }
        return b;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        Bitmap b = BitmapFactory.decodeResource(res, resId, null);
        Bitmap c = Bitmap.createScaledBitmap(b, reqWidth, reqHeight, false);
        b.recycle();
        return c;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {

        String extStorage = Environment.getExternalStorageDirectory().toString();

        File extFile = new File(extStorage, path);
        FileInputStream stream;
        try {
            stream = new FileInputStream(extFile);
        } catch (FileNotFoundException e) {
            return null;
        }

        Bitmap b = BitmapFactory.decodeStream(stream, null, null);
        Bitmap c = Bitmap.createScaledBitmap(b, reqWidth, reqHeight, false);
        b.recycle();
        return c;
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        // View is now detached, and about to be destroyed
        if (merged != null) {
            merged.recycle();
        }

        for (int i=0; i<layers.size(); i++) {
            if (layers.valueAt(i) != null) {
                layers.valueAt(i).recycle();
            }
        }
    }

}


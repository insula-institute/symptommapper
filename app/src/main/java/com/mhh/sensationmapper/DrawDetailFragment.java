package com.mhh.sensationmapper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.view.ViewPropertyAnimatorCompatSet;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.CycleInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.mhh.sensationmapper.comboseekbar.ComboSeekBar;
import com.mhh.sensationmapper.config.SensationManager;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbrink on 10.03.17.
 */

public class DrawDetailFragment extends Fragment {

    SensationManager.Sensation sensation = null;
    private int currentBrushId, eraserId, lastBrushId;
    BodyDrawingView bodyViewFront, bodyViewBack, currentBodyView, hiddenBodyView;
    private int mLongAnimationDuration;
    Brush currentBrush;
    int currentIntensity;

    int currentBodyThumbnail = 1;

    //ImageView thumbnailOverview;

    private List<String> sortedChoices = new ArrayList<>();

    private TextView txtEmptyTags;

    boolean tagsVisible = true;

    // passed down arguments
    int tabIndex;
    int color;
    int dampenedColor;

    private static int dampen(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] = 0.55f;
        return Color.HSVToColor(hsv);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getArguments();
        tabIndex = b.getInt("tabIndex");
        color = b.getInt("color");
        dampenedColor = dampen(color);

        sensation = ((DrawActivity)getActivity()).createSensation();
    }

    View detail = null;

    String strDraw = "Draw", strTags = "Tags", noChoiceSelected = "Please select a tag.";
    int [] ViewNames = {R.string.frontview, R.string.backview};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (detail != null) {
            //((ViewGroup) detail.getParent()).removeView(detail);
            return detail;
        }

        // else init everything
        final DrawActivity activity = (DrawActivity)getActivity();

        strDraw = activity.getDrawButton();
        strTags = activity.getTagButton();
        noChoiceSelected = activity.getNoChoiceSelected();

        detail = inflater.inflate(R.layout.fragment_draw_detail, container, false);

        // FIXME what?
        if (savedInstanceState != null) {
            return detail;
        }

/*
        // get color
        int color = sensation.getColor();

        // init header
        TextView txtHeader = (TextView)detail.findViewById(R.id.txtDetailHeader);
        txtHeader.setText(String.format(activity.getSensationText(), sensation.getId() + 1));
        txtHeader.setTextColor(color);
*/

        // init tagContainer
        final LinearLayout tagContainer = (LinearLayout)detail.findViewById(R.id.tagContainer);
        txtEmptyTags = (TextView)detail.findViewById(R.id.txtEmptyTags);
        txtEmptyTags.setText(activity.getEmptyTags());
        txtEmptyTags.setTextColor(Color.BLACK);

        // init BodyDrawingView
        bodyViewFront = (BodyDrawingView)detail.findViewById(R.id.drawingViewDetailFront);
        bodyViewFront.setSensationIndex(tabIndex);
        bodyViewFront.setBGImage(activity.getBackgroundImage(0));
        bodyViewFront.setMaskImage(activity.getMaskImage(0));
        bodyViewFront.setColor(color);
        bodyViewFront.setSnapshots(activity.snapshotsFront);

        bodyViewBack = (BodyDrawingView)detail.findViewById(R.id.drawingViewDetailBack);
        bodyViewBack.setSensationIndex(tabIndex);
        bodyViewBack.setBGImage(activity.getBackgroundImage(1));
        bodyViewBack.setMaskImage(activity.getMaskImage(1));
        bodyViewBack.setColor(color);
        bodyViewBack.setSnapshots(activity.snapshotsBack);

        // init grey overlay
        final LinearLayout viewA = (LinearLayout)detail.findViewById(R.id.viewA);
        //final FrameLayout viewB = (FrameLayout)detail.findViewById(R.id.viewB);
        //final View overlayA = detail.findViewById(R.id.greyOverlayA);
        final View overlayB = detail.findViewById(R.id.greyOverlayB);
        //overlayA.setVisibility(View.GONE);
        //overlayA.setAlpha(0f);
        overlayB.setClickable(true);
        overlayB.setAlpha(.618f);

        final Button btnSwitch = (Button)detail.findViewById(R.id.btnSwitch);
        btnSwitch.setText(strDraw);
        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimatorSet animSet = new AnimatorSet();
                int delta = viewA.getWidth() - btnSwitch.getWidth();
                if (tagsVisible) {

                    if (sortedChoices.size() == 0) {
                        Toast t = Toast.makeText(getContext(), noChoiceSelected, Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        TextView vt = (TextView) t.getView().findViewById(android.R.id.message);
                        if( vt != null) vt.setGravity(Gravity.CENTER);
                        t.show();
                        return;
                    }

                    btnSwitch.setText(strTags);

                    ObjectAnimator animX = ObjectAnimator.ofFloat(viewA, "x", -delta);
                    ObjectAnimator animAlpha = ObjectAnimator.ofFloat(overlayB, "alpha", 0f);
                    animSet.playTogether(animX, animAlpha);
                    overlayB.setClickable(false);
                } else {
                    btnSwitch.setText(strDraw);

                    ObjectAnimator animX = ObjectAnimator.ofFloat(viewA, "x", 0);
                    ObjectAnimator animAlpha = ObjectAnimator.ofFloat(overlayB, "alpha", .618f);
                    animSet.playTogether(animX, animAlpha);
                    overlayB.setClickable(true);
                }
                tagsVisible = !tagsVisible;
                animSet.setDuration(240);
                animSet.start();

            }
        });


        overlayB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimatorSet animSet = new AnimatorSet();
                int delta = viewA.getWidth() - btnSwitch.getWidth();
                if (tagsVisible) {

                    if (sortedChoices.size() == 0) {
                        Toast t = Toast.makeText(getContext(), noChoiceSelected, Toast.LENGTH_SHORT);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        TextView vt = (TextView) t.getView().findViewById(android.R.id.message);
                        if( vt != null) vt.setGravity(Gravity.CENTER);
                        t.show();
                        return;
                    }

                    btnSwitch.setText(strTags);

                    ObjectAnimator animX = ObjectAnimator.ofFloat(viewA, "x", -delta);
                    ObjectAnimator animAlpha = ObjectAnimator.ofFloat(overlayB, "alpha", 0f);
                    animSet.playTogether(animX, animAlpha);
                    overlayB.setClickable(false);
                }/* else {
                    btnSwitch.setText(strDraw);

                    ObjectAnimator animX = ObjectAnimator.ofFloat(viewA, "x", 0);
                    ObjectAnimator animAlpha = ObjectAnimator.ofFloat(overlayB, "alpha", .618f);
                    animSet.playTogether(animX, animAlpha);
                    overlayB.setClickable(true);
                }*/
                tagsVisible = !tagsVisible;
                animSet.setDuration(240);
                animSet.start();

            }
        });

/*
        overlayA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overlayA.setClickable(false);

                ObjectAnimator animX = ObjectAnimator.ofFloat(viewA, "x", viewB.getLeft() - viewA.getWidth()  - 2*59);
                animX.setInterpolator(new CycleInterpolator(.5f));
                ObjectAnimator animAlpha = ObjectAnimator.ofFloat(overlayA, "alpha", 0f);
                ObjectAnimator animAlpha2 = ObjectAnimator.ofFloat(overlayB, "alpha", .618f);
                ObjectAnimator animElevation = ObjectAnimator.ofFloat(viewA, "elevation", Math.round(dp2px(16)));
                ObjectAnimator animElevation2 = ObjectAnimator.ofFloat(viewB, "elevation", Math.round(dp2px(0)));

                animElevation.setInterpolator(new AccelerateDecelerateInterpolator());

                AnimatorSet animSet = new AnimatorSet();
                animSet.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        overlayB.setVisibility(View.VISIBLE);
                        overlayB.setAlpha(0f);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        overlayB.setClickable(true);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                animSet.playTogether(animX, animAlpha, animAlpha2, animElevation, animElevation2);
                animSet.setDuration(809);
                animSet.start();
            }
        });
*/


/*
        ValueAnimator anim = ValueAnimator.ofInt(v.getMeasuredWidth(), END_WIDTH);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                layoutParams.width = val;
                v.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(DURATION);
        anim.start();
*/



        // init rotate switch

        mLongAnimationDuration = getResources().getInteger(
                android.R.integer.config_longAnimTime);

        final ImageButton btnSwitchBody = (ImageButton)detail.findViewById(R.id.btnSwitchBodyView);
        final TextView textViewSwitchBody = (TextView) detail.findViewById(R.id.textView_SwitchBodyView);
        btnSwitchBody.setImageBitmap(activity.getThumbImage(1));
        btnSwitchBody.setScaleType(ImageView.ScaleType.FIT_CENTER);
        textViewSwitchBody.setText(getResources().getString(ViewNames[1]));
        //btnSwitchBody.setBackgroundTintList(ColorStateList.valueOf(color));
        btnSwitchBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                currentBodyThumbnail = (currentBodyThumbnail + 1)%2;
                btnSwitchBody.setImageBitmap(activity.getThumbImage(currentBodyThumbnail));
                textViewSwitchBody.setText(getResources().getString(ViewNames[currentBodyThumbnail]));
                v.setEnabled(false);
                hiddenBodyView.setAlpha(0f);
                hiddenBodyView.setVisibility(View.VISIBLE);

                // Animate the content view to 100% opacity, and clear any animation
                // listener set on the view.
                hiddenBodyView.animate()
                        .alpha(1f)
                        .setDuration(mLongAnimationDuration)
                        .setListener(null);

                // Animate the loading view to 0% opacity. After the animation ends,
                // set its visibility to GONE as an optimization step (it won't
                // participate in layout passes, etc.)
                currentBodyView.animate()
                        .alpha(0f)
                        .setDuration(mLongAnimationDuration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                currentBodyView.setVisibility(View.GONE);
                                hiddenBodyView.setBrush(currentBrush);
                                hiddenBodyView.setIntensity(currentIntensity);

                                BodyDrawingView tmp = hiddenBodyView;
                                hiddenBodyView = currentBodyView;
                                currentBodyView = tmp;
                                v.setEnabled(true);
                            }
                        });

            }
        });

        // init choices list
        int padding = Math.round(dp2px(8));
        int margin = Math.round(dp2px(4));

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(margin, margin, margin, margin);

        LinearLayout choicesContainer = (LinearLayout) detail.findViewById(R.id.choices_container);

        // create choice buttons
        View.OnClickListener choiceClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sensation.getChoices().size() == 0) {
                    ((LinearLayout) detail.findViewById(R.id.tagContainer)).removeAllViews();
                }

                int bid = ((ViewGroup) v.getParent()).indexOfChild(v);
                String choice = activity.getChoices().get(bid);
                sensation.toggleChoice(choice);
                int index = sortedChoices.indexOf(choice);
                if (index < 0) {
                    v.setBackgroundTintList(ColorStateList.valueOf(dampenedColor));
                    sortedChoices.add(choice);
                    TextView txt = new TextView(getContext());
                    txt.setTextColor(Color.BLACK);
                    txt.setText(choice);
                    txt.setPadding(6, 6, 6, 6);
                    tagContainer.addView(txt);
                } else {
                    v.setBackgroundTintList(null);
                    sortedChoices.remove(choice);
                    tagContainer.removeViewAt(index);
                }

                if (sensation.getChoices().size() == 0) {
                    ((LinearLayout) detail.findViewById(R.id.tagContainer)).addView(txtEmptyTags);
                }

            }
        };

        List<String> choices = activity.getChoices();
        for (String choice : choices) {
            ToggleButton b = new ToggleButton(getContext());
            b.setBackground(getContext().getDrawable(R.drawable.custom_radio));
            //b.setId(choices.indexOf(choice));
            b.setTextOn(choice);
            b.setTextOff(choice);
            b.setText(choice);
            b.setPadding(padding, padding, padding, padding);
            b.setOnClickListener(choiceClickListener);
            choicesContainer.addView(b, lp);
        }

        // init tool bar
        LinearLayout toolContainer = (LinearLayout)detail.findViewById(R.id.toolsContainer);

        int marginSmall = Math.round(dp2px(4));
        int marginLarge = Math.round(dp2px(24));
        int width = Math.round(dp2px(80));
        int height = Math.round(dp2px(102));

        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(width, height);
        lp1.setMargins(marginSmall, 0, marginSmall, 0);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(width, height);
        lp2.setMargins(marginLarge, 0, marginSmall, 0);

        // tool buttons
        final List<ImageButton> toolsBtns = new ArrayList<>();
        final List<Brush> brushes = activity.getBrushes();

        View.OnTouchListener keepSelectedListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int bid = ((ViewGroup) v.getParent()).indexOfChild(v);
                for (ImageButton ib : toolsBtns) {
                    ib.setPressed(bid == ((ViewGroup) ib.getParent()).indexOfChild(ib));
                    if (bid != eraserId) {
                        lastBrushId = bid;
                    }
                    currentBrushId = bid;
                    currentBrush = brushes.get(bid);
                    currentBodyView.setBrush(currentBrush);
                }
                return true;
            }
        };

        for (Brush brush : brushes) {
            ImageButton btn = new ImageButton(getContext());
            btn.setBackground(getContext().getDrawable(R.drawable.listitem_selector));
            btn.setImageDrawable(brush.icon);
            btn.setCropToPadding(false);
            btn.setScaleType(ImageButton.ScaleType.FIT_CENTER);
            //btn.setId(brushes.indexOf(brush));
            //btn.setPadding(padding1, 0, padding1, 0);
            btn.setOnTouchListener(keepSelectedListener);
            toolContainer.addView(btn, lp1);
            toolsBtns.add(btn);
            if (brush.name.equals(getString(R.string.brush_erase))) {
                eraserId = brushes.indexOf(brush);
            }
        }

        // select first body view
        currentBodyView = bodyViewFront;
        hiddenBodyView = bodyViewBack;
        hiddenBodyView.setVisibility(View.GONE);

        // select first button
        currentBrushId = 0;
        lastBrushId = 0;
        currentBrush = brushes.get(0);
        currentIntensity = -1;
        toolsBtns.get(0).setPressed(true);
        currentBodyView.setBrush(currentBrush);

        // undo button
        ImageButton btnUndo = new ImageButton(getContext());
        btnUndo.setBackground(getContext().getDrawable(R.drawable.listitem_selector));
        btnUndo.setImageDrawable(getContext().getDrawable(R.drawable.icon_undo));
        btnUndo.setCropToPadding(false);
        btnUndo.setScaleType(ImageButton.ScaleType.FIT_CENTER);
        //btnUndo.setId(brushes.size());
        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentBodyView.undoLastStep();
            }
        });
        toolContainer.addView(btnUndo, lp2);

        // init scale
        ComboSeekBar intensityScale = (ComboSeekBar) detail.findViewById(R.id.intensityScale);

        TextView t;
        t = (TextView) detail.findViewById(R.id.txtIntensityScaleMin);
        t.setText(activity.getIntensityScaleMin());
        t = (TextView) detail.findViewById(R.id.txtIntensityScaleMax);
        t.setText(activity.getIntensityScaleMax());

        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);        // TODO: WHY
        // intensity scale
        intensityScale.setThumb(transparentDrawable);
        //intensityScale.setTag(R.id.TAG_SEEKBAR_SELECTED, "false");

        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[1] = 0.1f;
        int color1 = Color.HSVToColor(hsv);
        hsv[1] = 1.0f;
        int color2 = Color.HSVToColor(hsv);
        int[] gcolor = {color1, color2};
        intensityScale.setColors(gcolor);

        final Drawable cursor = getResources().getDrawable(R.drawable.text_cursor, null);

        intensityScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //seekBar.setTag(R.id.TAG_SEEKBAR_SELECTED, "selected");
                // TODO move to onStopTrackingTouch ?
                currentIntensity = i;
                currentBodyView.setIntensity(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.setThumb(cursor);

                if (currentBrushId != lastBrushId) {
                    toolsBtns.get(eraserId).setPressed(false);
                    toolsBtns.get(lastBrushId).setPressed(true);
                    currentBrushId = lastBrushId;
                    currentBrush = brushes.get(currentBrushId);
                    currentBodyView.setBrush(currentBrush);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        return detail;
    }

/*
    // on hidden not called first time
    @Override
    public void onStart() {
        super.onStart();
        DrawOverviewFragment d = (DrawOverviewFragment)getActivity().getSupportFragmentManager().findFragmentByTag("overview");
        thumbnailOverview.setImageBitmap(d.getScreenshot());
        thumbnailOverview.invalidate();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            DrawOverviewFragment d = (DrawOverviewFragment)getActivity().getSupportFragmentManager().findFragmentByTag("overview");
            thumbnailOverview.setImageBitmap(d.getScreenshot());
            thumbnailOverview.invalidate();
        }
    }
*/

    // PUBLIC GETTER/SETTER

/*    public Pair<Bitmap, Bitmap> getSnapshots() {
        return new Pair<>(bodyViewFront.getSnapshot(), bodyViewBack.getSnapshot());
    }*/

/*
    private Bitmap screenshot = null;

    public Bitmap getScreenshot() {
        return screenshot;
    }

    private void onOverviewClicked(View v) {
        if (screenshot != null) {
            screenshot.recycle();
        }
        screenshot = DrawActivity.takeScreenshot(getView());

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.scale_from_top_right, R.anim.scale_to_top_left)
                .hide(this)
                .show(fragmentManager.findFragmentByTag("overview"))
                .commit();
    }
*/


    float dp2px(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }


}

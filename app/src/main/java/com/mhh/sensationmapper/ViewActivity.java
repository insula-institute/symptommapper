package com.mhh.sensationmapper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.SensationManager;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;
import com.mhh.sensationmapper.utilities.XMLDOMParser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViewActivity extends AppCompatActivity {

    private Patient patient;
    private ArrayList<Recording> recordings;

    private int mCurrentBodyfigureView;
    private ViewingView mViewingView;
    private Element mBodyfigureElement;
    public int mSelectedBodyfigure;
    public boolean isSpenFeatureEnabled = false;

    protected class GetNext {
        private int cid = -1;

        public int color(List<Integer> colors) {
            cid++;
            return colors.get(cid % colors.size());
        }

        public void reset() {
            cid = -1;
        }
    }

    protected GetNext getNext = new GetNext();

    //region bodyfigure xml constants
    public static final String NODE_BODYFIGURE = "bodyfigure";
    public static final String NODE_BODYFIGURE_THUMBNAIL = "thumbnail";
    public static final String NODE_BODYFIGURE_IMAGE = "image";
    public static final String NODE_BODYFIGURE_MASK = "maskimage";
    public static final String NODE_BODYFIGURE_EDGEMASK = "maskedge";
    public static final String NODE_BODYFIGURE_REALWIDTH = "realwidth";
    public static final String NODE_BODYFIGURE_REALHEIGHT = "realheight";
    public static final String NODE_BODYFIGURE_PIXELWIDTH = "pixelwidth";
    public static final String NODE_BODYFIGURE_PIXELHEIGHT = "pixelheight";
    public static final String NODE_BODYFIGURE_PARTS = "parts";
    public static final String NODE_BODYFIGURE_PARTS_PART = "part";
    public static final String NODE_BODYFIGURE_PARTS_ID = "id";
    public static final String NODE_BODYFIGURE_PARTS_NAME = "name";
    public static final String NODE_BODYFIGURE_PARTS_IMAGE = "image";
    public static final String NODE_BODYFIGURE_PARTS_MASKCOLOR = "maskcolor";
    public static final String NODE_BODYFIGURE_PARTS_LEFT = "left";
    public static final String NODE_BODYFIGURE_PARTS_RIGHT = "right";
    public static final String NODE_BODYFIGURE_PARTS_TOP = "top";
    public static final String NODE_BODYFIGURE_PARTS_BOTTOM = "bottom";
    public static final String NODE_BODYFIGURE_EDGES_EDGE = "edge";
    public static final String NODE_BODYFIGURE_EDGES_IMAGE = "image";
    public static final String NODE_BODYFIGURE_EDGES_MASKCOLOR = "maskcolor";
    public static final String NODE_BODYFIGURE_VIEW = "view";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public Patient getPatient() {
        return patient;
    }

    public List<Recording> getVersions() {
        return recordings;
    }

    private String getPatientPath() {
        return String.format("SymptomMapper/Subjects/p%d", patient.getId());
    }

    private String getRecordingPath(Recording recording) {
        return String.format("%s/n%d_a%d", getPatientPath(), recording.getNum(), recording.getAuthor());
    }

    class CheckRunningTask extends AsyncTask<Activity, Void, Void> {
        private Activity ctx;

        // Decode image in background.
        @Override
        protected Void doInBackground(Activity... params) {
            ctx = params[0];

            SensationMapper app = (SensationMapper) ctx.getApplication();

            while (true) {
                Boolean running = false;
                for (Recording r : recordings) {
                    if (app.getRunning(r.getId()) == true) {
                        running = true;
                    }
                }
                if (!running) {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ctx.findViewById(R.id.loadingPanel).setVisibility(View.GONE);
            createLayout();
        }
    }

    public void createLayout() {
        SensationMapper app = (SensationMapper) this.getApplication();
        List<Integer> colors = app.getConfigManager().getViewingColors();

        TextView txtPatientName = (TextView) findViewById(R.id.txtPatientName);
        txtPatientName.setText(String.format(getString(R.string.txt_patient_name_fmt), patient.getName()));

        LinearLayout[] strips = {(LinearLayout)findViewById(R.id.strip1),
                (LinearLayout)findViewById(R.id.strip2)};

/*
        LinearLayout[][] texts = {{(LinearLayout)findViewById(R.id.text11), (LinearLayout)findViewById(R.id.text12)},
                {(LinearLayout)findViewById(R.id.text21), (LinearLayout)findViewById(R.id.text22)}};
*/

        LinearLayout[] boxes = {(LinearLayout)findViewById(R.id.right1),
                (LinearLayout)findViewById(R.id.right2)};


        String[] views = {R.string.frontview + ":", R.string.backview + ":"};

        if (recordings.size() < 2) {
            ((View) strips[1].getParent()).setVisibility(View.GONE);
        }
        int reorder[] = {0, 1};

        for (int j=0; j<2; j++) {
            getNext.reset();
            for (int i=0; i<recordings.size(); i++) {
                Recording r = recordings.get(i);
                int count = r.getSensationCount();
                ViewingView vv = (ViewingView) (((LinearLayout)strips[i].getChildAt(j)).getChildAt(0));
                vv.setCount(count);
                vv.setBackgroundLayer();
                for (int k=0; k<count; k++) {
                    int color = getNext.color(colors);
                    String path = String.format("%s/s%d_v%d.png", getRecordingPath(r), k, reorder[j]);
//                    String path = getRecordingPath(r) + "/Drawing_Steps_Merged/" + reorder[j] + "_" + k + ".png";
                    vv.putLayer(k, path, color);
                }
            }
        }

        // sensation info
        getNext.reset();
        LinearLayout[] layout_info = {(LinearLayout) findViewById(R.id.layout_info1), (LinearLayout) findViewById(R.id.layout_info2)};
        DBHelper dbh = app.getDBHelper();

        for (int i = 0; i < recordings.size(); i++) {
            Recording r = recordings.get(i);
            TextView txtStripTitle = new TextView(this);
            String author = app.getConfigManager().getAuthorManager().getAuthors().get(r.getAuthor()).getName();
            String datetime = dbh.formatDateTime(this, String.valueOf(r.getDate().getTime()));
            txtStripTitle.setText(String.format(getString(R.string.view_info_title), r.getId(), author, datetime));
            txtStripTitle.setTypeface(null, Typeface.BOLD);
            layout_info[i].addView(txtStripTitle);

            String path = getRecordingPath(r) + "/records.json";
            ConfigManager c = new ConfigManager(path);
            List<SensationManager.Sensation> sensations = c.getActiveMode().getSensationManager().getSensations();
            for (int j=0; j<sensations.size(); j++) {
                View spacer = new View(this);
                spacer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 16));
                boxes[i].addView(spacer);
                SensationManager.Sensation s = sensations.get(j);
                int color = getNext.color(colors);
                for (String choice: s.getChoices()) {
                    TextView txtChoice = new TextView(this);
                    txtChoice.setText(choice);
                    txtChoice.setTextColor(color);
                    boxes[i].addView(txtChoice);
                }
                //layout_info[i].addView(txtSensation);
                // TODO FIXME
                /*
                for (int k=0; k<2; k++) {
                    TextView t1 = new TextView(this);
                    t1.setText(views[k]);
                    //t1.setTextColor(color);
                    boxes[i].addView(t1);

                    TextView t = new TextView(this);
                    Double area = s.getAreaRatio(reorder[k]);
                    Double mean = s.getIntensityMean(reorder[k]);
                    Double sigma = s.getIntensitySigma(reorder[k]);
                    if (mean == null || sigma == null) {
                        t.setText(String.format(R.string.area + ": %.0f%%", 100 * area));
                    } else {
                        t.setText(String.format(R.string.area + ": %.0f%%\nVAS: %.1f±%.1f", 100 * area, 10*mean, 10*sigma));
                    }
                    t.setTextColor(color);
                    //t.setGravity(Gravity.CENTER_HORIZONTAL);
                    boxes[i].addView(t);
                    //texts[i][k].addView(t);
                }*/
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarViewActivity);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recordings = intent.getParcelableArrayListExtra("recordings");

        setContentView(R.layout.activity_view);

        setTitle(R.string.app_name);

        CheckRunningTask task = new CheckRunningTask();
        task.execute(this);


            //done button
        Button btnDone = (Button) findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDonePressed();
            }
        });

        //s-pen
/*        Spen spenPackage = new Spen();
        try {
            spenPackage.initialize(this);
            isSpenFeatureEnabled = spenPackage.isFeatureEnabled(Spen.DEVICE_PEN);
        } catch (SsdkUnsupportedException e) {
            Toast.makeText(this, R.string.no_spen, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            finish();
        } catch (Exception e1) {
            Toast.makeText(this, R.string.spen_error, Toast.LENGTH_SHORT).show();
            e1.printStackTrace();
            finish();
        }*/
    }

    //endregion

/*
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }
*/

    private void onDonePressed() {
        finish();
    }

    private void getAppBodyfigures() {
        String locale = new String(Locale.getDefault().getLanguage().toString());
        String extStorage = Environment.getExternalStorageDirectory().toString();
        File extFile = new File(extStorage, "SymptomMapper/Config/" + locale + "/bodyfigures.xml");

        XMLDOMParser parser = new XMLDOMParser();
        InputStream stream;
        try {
            stream = new FileInputStream(extFile);
        } catch (FileNotFoundException ex) {
            stream = getResources().openRawResource(R.raw.bodyfigures);
        }

        Document document = parser.getDocument(stream);

        NodeList nodeList = document.getElementsByTagName(NODE_BODYFIGURE);
        mBodyfigureElement = (Element) nodeList.item(mSelectedBodyfigure);

    }

    public int getBodyfigureViewsSum() {
        return mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).getLength();

    }

    public String getBGImageName(int viewID) {
        return ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                .getElementsByTagName(NODE_BODYFIGURE_IMAGE)
                .item(0).getTextContent();

    }

    public int getBGResourceId(int viewID) {
        return getResources().getIdentifier(
                ((Element) mBodyfigureElement.getElementsByTagName(NODE_BODYFIGURE_VIEW).item(viewID))
                        .getElementsByTagName(NODE_BODYFIGURE_IMAGE)
                        .item(0).getTextContent(),
                "drawable", getPackageName()
        );

    }

}
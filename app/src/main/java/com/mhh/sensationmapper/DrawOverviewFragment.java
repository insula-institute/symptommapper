/*
package com.mhh.sensationmapper;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by sbrink on 10.03.17.
 *//*


public class DrawOverviewFragment extends Fragment {

    List<DrawDetailFragment> detailFragments = new ArrayList<>();

    List<Bitmap> snapshotsFront = new ArrayList<>();
    List<Bitmap> snapshotsBack = new ArrayList<>();

    BodyDrawingView bodyFront, bodyBack;

    LinearLayout thumbnailList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final DrawActivity activity = (DrawActivity)getActivity();

        // Inflate the layout for this fragment
        View overview = inflater.inflate(R.layout.fragment_draw_overview, container, false);

*/
/*
        // FIXME make this better
        FloatingActionButton fabSwitch = (FloatingActionButton)overview.findViewById(R.id.btnNewSensation);
        fabSwitch.setBackgroundTintList(ColorStateList.valueOf(activity.getCurrentColor()));

*//*

*/
/*
        ImageView btnDetailPlus = (ImageView)overview.findViewById(R.id.btnDetailPlus);
        btnDetailPlus.setImageDrawable(getActivity().getDrawable(R.drawable.sym_action_add));
        int color = activity.getNextColor();
        btnDetailPlus.setColorFilter(color);
*//*
*/
/*


        // thumbnailList
        thumbnailList = (LinearLayout)overview.findViewById(R.id.thumbnail_list);


        // init BodyDrawingViews
        bodyFront = (BodyDrawingView)overview.findViewById(R.id.drawingViewFront);
        bodyFront.setBGImage(activity.getBackgroundImage(0));
        bodyFront.setMaskImage(activity.getMaskImage(0));
        bodyFront.setSnapshots(snapshotsFront);

        bodyBack = (BodyDrawingView)overview.findViewById(R.id.drawingViewBack);
        bodyBack.setBGImage(activity.getBackgroundImage(1));
        bodyBack.setMaskImage(activity.getMaskImage(1));
        bodyBack.setSnapshots(snapshotsBack);

        // init Done Button
        Button btnDone = (Button)overview.findViewById(R.id.btnDrawingDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.showDrawingDoneDialog();
            }
        });

        //
        ImageView thumbnailPlus = (ImageView)overview.findViewById(R.id.thumbnail_plus);
        thumbnailPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlusClicked(v);
            }
        });

        // create new detail fragment (underneath plus button)
        //DrawDetailFragment fragment = new DrawDetailFragment();

        // Add the fragment to the 'fragment_container_new_detail' FrameLayout
        //getChildFragmentManager().beginTransaction()
                //.add(R.id.fragment_container_new_detail, fragment).commit();
*//*


        return overview;
    }

*/
/*
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            snapshotsFront.clear();
            snapshotsBack.clear();
            for (int i=0; i < detailFragments.size(); i++) {
                DrawDetailFragment d = detailFragments.get(i);
                Pair<Bitmap, Bitmap> p = d.getSnapshots();
                snapshotsFront.add(p.first);
                snapshotsBack.add(p.second);
                ((ImageView)thumbnailList.getChildAt(i)).setImageBitmap(d.getScreenshot());
            }

            thumbnailList.invalidate();
            bodyFront.invalidate();
            bodyBack.invalidate();
        }

    }

    //
    // PUBLIC GETTER/SETTER
    //

    private Bitmap screenshot = null;

    public Bitmap getScreenshot() {
        return screenshot;
    }

    private void onPlusClicked(View v) {
        if (screenshot != null) {
            screenshot.recycle();
        }
        screenshot = DrawActivity.takeScreenshot(getView());

        DrawActivity activity = (DrawActivity)getActivity();
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        DrawDetailFragment detailFragment = new DrawDetailFragment();
        detailFragments.add(detailFragment);

        // FIXME make this better
        int color = activity.getNextColor();
        FloatingActionButton fabSwitch = (FloatingActionButton)getView().findViewById(R.id.btnNewSensation);
        fabSwitch.setBackgroundTintList(ColorStateList.valueOf(color));       // new sensation has been created

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(dp2px(160)), LinearLayout.LayoutParams.MATCH_PARENT);
        ImageView thumbnail = new ImageView(activity);
        thumbnail.setId(thumbnailList.getChildCount());
        thumbnail.setBackgroundColor(activity.getCurrentColor());       // FIXME make this better
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onThumbnailClicked(v);
            }
        });
        thumbnail.setPadding(4, 4, 4, 4);
        thumbnailList.addView(thumbnail, layoutParams);

        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.scale_from_top_right, R.anim.scale_to_top_right)
                .add(R.id.fragment_container, detailFragment)
                .hide(this)
                .show(detailFragment)
                .commitNow();
    }

    private void onThumbnailClicked(View v) {
        if (screenshot != null) {
            screenshot.recycle();
        }
        screenshot = DrawActivity.takeScreenshot(getView());

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        DrawDetailFragment detailFragment = detailFragments.get(v.getId());
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.scale_from_top_left, R.anim.scale_to_top_right)
                .hide(this)
                .show(detailFragment)
                .commit();

    }

    float dp2px(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
*//*

}
*/

package com.mhh.sensationmapper;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.Placeholder;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.mhh.sensationmapper.Questionnaires.Pain_Questionnaire_1;
import com.mhh.sensationmapper.Questionnaires.Pain_Questionnaire_2;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_ADS;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_PHQ_9;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_Data;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_MIDAS;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_MPSS;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_Medication;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_PDI;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_PDS_1;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_SCL_27;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_PHQ_15;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_PHQ_Stress;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_GAD_7;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_STAI_X2;
import com.mhh.sensationmapper.Questionnaires.Questionnaire_VAS;
import com.mhh.sensationmapper.config.QuestionnaireJson;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
//This activity contains the questionnaire tool including the several pain questionnaires that are displayed as fragments (files in Questionnaires folder).
public class QuestionnairePainActivity extends AppCompatActivity
    implements Questionnaire_GAD_7.OnFragmentInteractionListener, Questionnaire_PHQ_9.OnFragmentInteractionListener, Questionnaire_PDI.OnFragmentInteractionListener, Questionnaire_VAS.OnFragmentInteractionListener, Questionnaire_PHQ_15.OnFragmentInteractionListener, Questionnaire_PHQ_Stress.OnFragmentInteractionListener, Questionnaire_MPSS.OnFragmentInteractionListener, Questionnaire_PDS_1.OnFragmentInteractionListener, Questionnaire_MIDAS.OnFragmentInteractionListener, Questionnaire_STAI_X2.OnFragmentInteractionListener, Questionnaire_ADS.OnFragmentInteractionListener, Questionnaire_SCL_27.OnFragmentInteractionListener, Questionnaire_Data.OnFragmentInteractionListener, Questionnaire_Medication.OnFragmentInteractionListener{
    ScrollView mScrollView;
    TextView updateText;
    Object ADS;
    Object PHQ_9;
    Object STAI_X2;
    Object GAD_7;
    Object PDI;
    Object VAS;
    Object SCL_27;
    Object PHQ_15;
    Object PHQ_Stress;
    Object MPSS;
    Object PDS_1;
    Object MIDAS;
    Object Data;
    Object Medication;
    Boolean agreement;
    Patient patient;
    Recording recording;
    String mode;
    static String patgroup;
    Button saveBtn;
    TextView Questionnaire_finished;
    ArrayList questionnaires;
    Fragment fragment_GAD_7 = new Questionnaire_GAD_7();
    Fragment fragment_ADS = new Questionnaire_ADS();
    Fragment fragment_PHQ_9 = new Questionnaire_PHQ_9();
    Fragment fragment_PDI = new Questionnaire_PDI();
    Fragment fragment_VAS = new Questionnaire_VAS();
    Fragment fragment_PHQ_15 = new Questionnaire_PHQ_15();
    Fragment fragment_PHQ_Stress = new Questionnaire_PHQ_Stress();
    Fragment fragment_MPSS = new Questionnaire_MPSS();
    Fragment fragment_PDS_1 = new Questionnaire_PDS_1();
    Fragment fragment_MIDAS = new Questionnaire_MIDAS();
    Fragment fragment_STAI_X2 = new Questionnaire_STAI_X2();
    Fragment fragment_SCL_27 = new Questionnaire_SCL_27();
    Fragment fragment_Data = new Questionnaire_Data();
    Fragment fragment_Medication = new Questionnaire_Medication();
    Fragment [] fragments = {fragment_Data, fragment_Medication, fragment_PHQ_9, fragment_MIDAS, fragment_MPSS, fragment_PDI, fragment_PDS_1, fragment_PHQ_15, fragment_PHQ_Stress, fragment_GAD_7, fragment_VAS, fragment_STAI_X2, fragment_ADS, fragment_SCL_27};
    int Qcount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire_pain);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mScrollView = (ScrollView)findViewById(R.id.ScrollView_Questionnaire);
        //updateText = (TextView)findViewById(R.id.text_update);
        //transaction.replace(R.id.placeholder_q1, fragment1);
        //transaction.replace(R.id.placeholder_q2, fragment2);
        //transaction.commit();
        Intent intent = getIntent();
//        Boolean q1 = intent.getBooleanExtra("q1", false);
//        Boolean q2 = intent.getBooleanExtra("q2", false);
//        Boolean q3 = intent.getBooleanExtra("q3", false);
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");
        mode = intent.getStringExtra("mode");
        patgroup = intent.getStringExtra("patgroup");
        questionnaires = intent.getParcelableArrayListExtra("questionnaires");
        agreement = intent.getExtras().getBoolean("agreement");

        Log.i("QuestArray postintent", questionnaires.toString());
        updateView();
/*        if (patgroup.equals("btn_pgrp_n")){
            transaction.replace(R.id.placeholder_q1, fragment1);}
        if (patgroup.equals("btn_pgrp_r")){
            transaction.replace(R.id.placeholder_q1, fragment_STAI_X2);}
        if (patgroup.equals("btn_pgrp_h")){
            transaction.replace(R.id.placeholder_q1, fragment_ADS);
            }
        transaction.commit();*/

        //Floating Button scroll down
        FloatingActionButton fab_down = (FloatingActionButton) findViewById(R.id.fab_down);
        fab_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                mScrollView.pageScroll(view.FOCUS_DOWN);
                //mScrollView.scrollBy(0, +100);

                }

        });
        //Floating Button scroll up
        FloatingActionButton fab_up = (FloatingActionButton) findViewById(R.id.fab_up);
        fab_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mScrollView.pageScroll(view.FOCUS_UP);

            }


        });
        Questionnaire_finished = (TextView)findViewById(R.id.Questionnaire_finished);
        /*saveBtn = (Button) findViewById(R.id.button_save);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeJsonFile(mode, patgroup, STAI_X2, ADS, PDI, VAS, SCL_27, MPSS, PDS_1, MIDAS, Data, Medication);
                startDrawActivity(patient, recording, mode, patgroup);
            }
            });*/
        }


   /* public void updateTextField(String newText){
        updateText.setText(newText);}*/

    private void updateView(){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(Qcount);
        //old questionnaire to fragment
        /*if (patgroup.equals("btn_pgrp_n")){
            progressBar.setMax(9);
            if (Qcount == 0) {
                transaction.replace(R.id.placeholder_q1, fragment_Data);
                Qcount++;
            } else if (Qcount == 1) {
                transaction.replace(R.id.placeholder_q1, fragment_Medication);
                Qcount++;
            } else if (Qcount == 2) {
                transaction.replace(R.id.placeholder_q1, fragment_VAS);
                Qcount++;
            } else if (Qcount == 3) {
                transaction.replace(R.id.placeholder_q1, fragment_ADS);
                Qcount++;
            } else if (Qcount == 4) {
                transaction.replace(R.id.placeholder_q1, fragment_STAI_X2);
                Qcount++;
            }else if (Qcount == 5) {
                transaction.replace(R.id.placeholder_q1, fragment_SCL_27);
                Qcount++;
            }else if (Qcount == 6) {
                transaction.replace(R.id.placeholder_q1, fragment_PDI);
                Qcount++;
            }else if (Qcount == 7) {
                transaction.replace(R.id.placeholder_q1, fragment_MPSS);
                Qcount++;
            }else if (Qcount == 8) {
                transaction.replace(R.id.placeholder_q1, fragment_PDS_1);
                Qcount++;
            } else if (Qcount == 9) {
                writeJsonFile(mode, patgroup, STAI_X2, ADS, PDI, VAS, SCL_27, MPSS, PDS_1, MIDAS, Data, Medication);
                startDrawActivity(patient, recording, mode, patgroup);
*//*                FrameLayout placeholder = (FrameLayout) findViewById(R.id.placeholder_q1);
                placeholder.setVisibility(View.GONE);
                Questionnaire_finished.setVisibility(View.VISIBLE);
                saveBtn.setVisibility(View.VISIBLE);*//*
            }
        }
        if (patgroup.equals("btn_pgrp_r")) {
            progressBar.setMax(7);
            if (Qcount == 0) {
                transaction.replace(R.id.placeholder_q1, fragment_Data);
                Qcount++;
            } else if (Qcount == 1) {
                transaction.replace(R.id.placeholder_q1, fragment_Medication);
                Qcount++;
            } else if (Qcount == 2) {
                transaction.replace(R.id.placeholder_q1, fragment_VAS);
                Qcount++;
            } else if (Qcount == 3) {
                transaction.replace(R.id.placeholder_q1, fragment_ADS);
                Qcount++;
            } else if (Qcount == 4) {
                transaction.replace(R.id.placeholder_q1, fragment_STAI_X2);
                Qcount++;
            }else if (Qcount == 5) {
                transaction.replace(R.id.placeholder_q1, fragment_SCL_27);
                Qcount++;
            }else if (Qcount == 6) {
                transaction.replace(R.id.placeholder_q1, fragment_PDI);
                Qcount++;
            } else if (Qcount == 7) {
                writeJsonFile(mode, patgroup, STAI_X2, ADS, PDI, VAS, SCL_27, MPSS, PDS_1, MIDAS, Data, Medication);
                startDrawActivity(patient, recording, mode, patgroup);
                *//*FrameLayout placeholder = (FrameLayout) findViewById(R.id.placeholder_q1);
                placeholder.setVisibility(View.GONE);
                Questionnaire_finished.setVisibility(View.VISIBLE);
                saveBtn.setVisibility(View.VISIBLE);*//*
            }
        }

        if (patgroup.equals("btn_pgrp_h")){
            progressBar.setMax(10);
            if (Qcount == 0) {
                transaction.replace(R.id.placeholder_q1, fragment_Data);
                Qcount++;
            } else if (Qcount == 1) {
                transaction.replace(R.id.placeholder_q1, fragment_Medication);
                Qcount++;
            } else if (Qcount == 2) {
                transaction.replace(R.id.placeholder_q1, fragment_VAS);
                Qcount++;
            } else if (Qcount == 3) {
                transaction.replace(R.id.placeholder_q1, fragment_ADS);
                Qcount++;
            } else if (Qcount == 4) {
                transaction.replace(R.id.placeholder_q1, fragment_STAI_X2);
                Qcount++;
            }else if (Qcount == 5) {
                transaction.replace(R.id.placeholder_q1, fragment_SCL_27);
                Qcount++;
            }else if (Qcount == 6) {
                transaction.replace(R.id.placeholder_q1, fragment_PDI);
                Qcount++;
            }else if (Qcount == 7) {
                transaction.replace(R.id.placeholder_q1, fragment_MPSS);
                Qcount++;
            }else if (Qcount == 8) {
                transaction.replace(R.id.placeholder_q1, fragment_PDS_1);
                Qcount++;
            }else if (Qcount == 9) {
                transaction.replace(R.id.placeholder_q1, fragment_MIDAS);
                Qcount++;
            }else if (Qcount == 10){
                writeJsonFile(mode, patgroup, STAI_X2, ADS, PDI, VAS, SCL_27, MPSS, PDS_1, MIDAS, Data, Medication);
                startDrawActivity(patient, recording, mode, patgroup);
*//*                FrameLayout placeholder = (FrameLayout)findViewById(R.id.placeholder_q1);
                placeholder.setVisibility(View.GONE);
                Questionnaire_finished.setVisibility(View.VISIBLE);
                saveBtn.setVisibility(View.VISIBLE);*//*
            }
        }
        if (patgroup.equals("btn_pgrp_s")){*/
            progressBar.setMax(questionnaires.size());

            if (Qcount < questionnaires.size()) {
                Log.i("questionnairesize", String.valueOf(questionnaires.size()));
                Log.i("Qcount", String.valueOf(Qcount));
//                int newfragmentid = getResources().getIdentifier(questionnaires.get(Qcount), "id", getPackageName());
//                Fragment newfragment = (Fragment) View.findViewById(newfragmentid);
                transaction.replace(R.id.placeholder_q1, fragments[(int) questionnaires.get(Qcount)]);
                Qcount++;}
            else if (Qcount == questionnaires.size()) {
                Log.i("questionnairesize", String.valueOf(questionnaires.size()));
                Log.i("Qcount", String.valueOf(Qcount));
                writeJsonFile(mode, patgroup, GAD_7, PHQ_9, PDI, VAS, PHQ_15, PHQ_Stress, MPSS, PDS_1, MIDAS, STAI_X2, ADS, SCL_27, Data, Medication, agreement);
                startDrawActivity(patient, recording, mode, patgroup);}


        transaction.commit();
        //FrameLayout placeholder = (FrameLayout)findViewById(getResources().getIdentifier("placeholder_" + tag, "id", getPackageName()));
        //placeholder.setVisibility(View.GONE);
    }


    public void writeJsonFile(String mode, String patgroup, Object GAD_7, Object PHQ_9, Object PDI, Object VAS, Object PHQ_15, Object PHQ_Stress, Object MPSS, Object PDS_1, Object MIDAS, Object STAI_X2, Object ADS, Object SCL_27, Object Data, Object Medication, Boolean agreement) {
        QuestionnaireJson obj = new QuestionnaireJson();
        JSONObject jsonObject = obj.makeJSONObject(mode, patgroup, GAD_7, PHQ_9, PDI, VAS, PHQ_15, PHQ_Stress, MPSS, PDS_1, MIDAS, STAI_X2, ADS, SCL_27, Data, Medication, agreement);
        //MyCompositionsListActivity.buildList();

        try {
            Writer output = null;
            String extStorage = Environment.getExternalStorageDirectory().toString();
            File file = new File(extStorage, getRecordingPath() + "/questionnaire.json");
            if (!file.exists()) {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
            }//put this behind the Toast to not overwrite existing files and delete the writeJSON.. in the onFragmentInteraction()

            //File file = new File(Environment.getExternalStorageDirectory() + "/SymptomMapper/test_q.json");
            output = new BufferedWriter(new FileWriter(file));
            output.write(jsonObject.toString());
            output.close();
            //Toast.makeText(getApplicationContext(), "saved", Toast.LENGTH_LONG).show();


        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
        //finish();
    }
    private String getPatientPath() {
        return String.format("SymptomMapper/Subjects/p%d", patient.getId());
    }

    private String getRecordingPath() {
        return String.format("%s/n%d_a%d", getPatientPath(), recording.getNum(), recording.getAuthor());
    }

    @Override
    public void onFragmentInteraction(String tag, Object data) {
        //updateTextField(tag + " : " + data.toString());
        if (tag == "PHQ_9"){PHQ_9 = data;}
        if (tag == "GAD_7"){GAD_7 = data;}
        if (tag == "PDI"){PDI = data;}
        if (tag == "VAS"){VAS = data;}
        if (tag == "PHQ_15"){PHQ_15 = data;}
        if (tag == "PHQ_Stress"){PHQ_Stress = data;}
        if (tag == "MPSS"){MPSS = data;}
        if (tag == "PDS_1"){PDS_1 = data;}
        if (tag == "MIDAS"){MIDAS = data;}
        if (tag == "STAI_X2"){STAI_X2 = data;}
        if (tag == "ADS"){ADS = data;}
        if (tag == "SCL_27"){SCL_27 = data;}
        if (tag == "Data"){Data = data;}
        if (tag == "Medication"){Medication = data;}
        mScrollView.fullScroll(mScrollView.FOCUS_UP);
        writeJsonFile(mode, patgroup, GAD_7, PHQ_9, PDI, VAS, PHQ_15, PHQ_Stress, MPSS, PDS_1, MIDAS, STAI_X2, ADS, SCL_27, Data, Medication, agreement);
        updateView();
//        updateTextField(ADS.toString());
//        writeJsonFile(ADS);*/
    }
//start instruction activity
    private void startDrawActivity (Patient patient, Recording recording, String mode, String patgroup) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, InstructionActivity_new.class);

        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);
        intent.putExtra("patgroup", patgroup);
        startActivity(intent);
    }
    //pass patgroup to fragments
    public static String getPatgroup(){
        return patgroup;
    }
    //disable back button
    @Override
    public void onBackPressed() {

            //super.onBackPressed();

    }
};




/*
package com.mhh.sensationmapper;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.config.ModeManager.InstructionScreen;
import com.mhh.sensationmapper.config.ModeManager.Mode;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;
import java.io.File;

public class InstructionActivity
  extends AppCompatActivity
{
  ModeManager.InstructionScreen instruction;
  
  private void onDoneClicked()
  {
    Intent localIntent = getIntent();
    Patient localPatient = (Patient)localIntent.getParcelableExtra("patient");
    Recording localRecording = (Recording)localIntent.getParcelableExtra("recording");
    String str = localIntent.getStringExtra("mode");
    SymptomMapper localSymptomMapper = (SymptomMapper)getApplicationContext();
    Mode localMode = localSymptomMapper.getRecordsJSON(localRecording).getModeManager().getMode(str);
    localIntent = null;
    if (localMode.getFeedback2() != null)
    {
      localIntent = new Intent(this, FeedbackActivity.class);
      localIntent.putExtra("iteration", 2);
    }
    if (localMode.getDrawing2() != null)
    {
      localIntent = new Intent(this, DrawActivity.class);
      localIntent.putExtra("iteration", 2);
    }
    if (localMode.getFeedback1() != null)
    {
      localIntent = new Intent(this, FeedbackActivity.class);
      localIntent.putExtra("iteration", 1);
    }
    if (localMode.getDrawing1() != null)
    {
      localIntent = new Intent(this, DrawActivity.class);
      localIntent.putExtra("iteration", 1);
    }
    if (localIntent != null)
    {
      localIntent.putExtra("patient", localPatient);
      localIntent.putExtra("recording", localRecording);
      localIntent.putExtra("mode", str);
    }
    for (;;)
    {
      finish();
      startActivity(localIntent);
      return;
      localSymptomMapper.saveRecordingToDB(localRecording);
      localSymptomMapper.savePatientInfo(localPatient);
      localSymptomMapper.quitStorageThread(localRecording);
      localSymptomMapper.saveAndCloseRecordsJSON(localRecording, localSymptomMapper.getRecordingPath(localPatient, localRecording));
      localIntent = new Intent(this, LockScreenActivity.class);
      localIntent.addFlags(67108864);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject = getIntent();
    paramBundle = (Recording)((Intent)localObject).getParcelableExtra("recording");
    localObject = ((Intent)localObject).getStringExtra("mode");
    this.instruction = ((SymptomMapper)getApplicationContext()).getRecordsJSON(paramBundle).getModeManager().getMode((String)localObject).getInstruction();
    setContentView(2131427356);
    paramBundle = (Button)findViewById(2131296304);
    paramBundle.setText(this.instruction.getInstructionDoneButton());
    paramBundle.setOnClickListener(new OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        InstructionActivity.this.onDoneClicked();
      }
    });
    ((ImageView)findViewById(2131296376)).setImageURI(Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString(), this.instruction.getImagePath())));
  }
}


*/
/* Location:              C:\Users\Till\SymptomMapper\Rekonstruktion\dex2jar-2.0\app-debug-dex2jar.jar!\com\mhh\sensationmapper\InstructionActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */

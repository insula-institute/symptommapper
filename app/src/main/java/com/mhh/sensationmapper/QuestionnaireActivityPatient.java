/*
package com.mhh.sensationmapper;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mhh.sensationmapper.comboseekbar.ComboSeekBar;
import com.mhh.sensationmapper.config.ModeManager;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuestionnaireActivityPatient extends QuestionnaireActivity {

    private String[] sensationList;
    private String[] depthList;

    public String getInstructionsSensation() {return mode.getInstructionsSensation(); }
    ModeManager.Mode mode;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_step_patient;
    }

    @Override
    protected void setResources() {
        Resources res = getResources();
        sensationList = res.getStringArray(R.array.sensations_list);
        depthList = res.getStringArray(R.array.depth_list);
    }

    private void initToggleButton(ToggleButton btn, String text) {
        btn.setTextOn(text);
        btn.setTextOff(text);
        btn.setChecked(false);
        btn.setBackground(getResources().getDrawable(R.drawable.custom_radio));
    }

    private void initSeekBar(ComboSeekBar seekBar) {

        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        seekBar.setThumb(transparentDrawable);
        seekBar.setTag(R.id.TAG_SEEKBAR_SELECTED, "false");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBar.setThumb(getResources().getDrawable(R.drawable.text_cursor));
                seekBar.setTag(R.id.TAG_SEEKBAR_SELECTED, "selected");
                seekBar.setOnSeekBarChangeListener(null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    protected void prepareLayout() {

        // scale params
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.weight = 4;
        lp.width = 0;
        lp.gravity = Gravity.CENTER_VERTICAL;

        // sensations
        final FlowLayout sensationsBtnLayout = (FlowLayout) findViewById(R.id.sensationsBtnContainer);
        for (int i = 0; i < sensationList.length; i++) {
            ToggleButton myButton = new ToggleButton(this);
            initToggleButton(myButton, sensationList[i]);
            myButton.setId(i);
            sensationsBtnLayout.addView(myButton);
        }
        findViewById(R.id.sensationsBtnContainer).setVisibility(View.VISIBLE);

        // custom Question
        ToggleButton btnCustomSensation = (ToggleButton) findViewById(R.id.btnCustomSensation);
        initToggleButton(btnCustomSensation, getResources().getString(R.string.sensation_other));
        final CompoundButton.OnClickListener customOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.txtOtherSensation).setVisibility((((ToggleButton) view).isChecked()) ? View.VISIBLE : View.INVISIBLE);
            }
        };
        btnCustomSensation.setOnClickListener(customOnClickListener);

        // intensity
        LinearLayout intensityContainer = (LinearLayout) findViewById(R.id.intensity_scale_container);
        final ComboSeekBar intensitySeekBar = new ComboSeekBar(this);
        intensitySeekBar.setLayoutParams(lp);
        List<String> seekBarStep = Arrays.asList(getResources().getStringArray(R.array.intensity_scale));
        intensitySeekBar.setAdapter(seekBarStep);
        //initSeekBar(intensitySeekBar);
        intensityContainer.addView(intensitySeekBar, 1);

        // depths
        final FlowLayout depthBtnLayout = (FlowLayout) findViewById(R.id.depthBtnContainer);
        for (int i = 0; i < depthList.length; i++) {
            ToggleButton myButton = new ToggleButton(this);
            initToggleButton(myButton, depthList[i]);
            myButton.setId(i);
            depthBtnLayout.addView(myButton);
        }
        findViewById(R.id.depthBtnContainer).setVisibility(View.VISIBLE);

        // burden
        final LinearLayout burdenContainer = (LinearLayout) findViewById(R.id.burden_scale_container);
        final ComboSeekBar burdenSeekBar = new ComboSeekBar(this);
        burdenSeekBar.setLayoutParams(lp);
        List<String> burdenSeekBarStep = Arrays.asList(getResources().getStringArray(R.array.intensity_scale));
        burdenSeekBar.setAdapter(burdenSeekBarStep);
        //initSeekBar(burdenSeekBar);
        burdenContainer.addView(burdenSeekBar, 1);

        // start button
        Button startBtn = (Button) findViewById(R.id.btn_start_drawing);
        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                // sensations
                ArrayList<String> sensations = new ArrayList<>();
                for (int i = 0; i < sensationList.length; i++) {
                    ToggleButton myButton = (ToggleButton) sensationsBtnLayout.findViewById(i);
                    if (myButton != null && myButton.isChecked()) {
                        sensations.add(sensationList[i]);
                    }
                }

                // custom sensation
                String custom = ((EditText) findViewById(R.id.txtOtherSensation)).getText().toString();

                // check if no sensation selected
                if (sensations.isEmpty() && custom.isEmpty()) {
                    TextView errTxt = (TextView) findViewById(R.id.err_text);
                    errTxt.setText(getText(R.string.err_select_sensation));
                    errTxt.setVisibility(View.VISIBLE);
                    return;
                }

                // intensity
                int intensity = intensitySeekBar.getProgress();

                //check if no intensity selected
                if (intensitySeekBar.getTag(R.id.TAG_SEEKBAR_SELECTED).equals("false")) {
                    TextView errTxt = (TextView) findViewById(R.id.err_text);
                    errTxt.setText(getText(R.string.err_select_intensity));
                    errTxt.setVisibility(View.VISIBLE);
                    return;
                }

                // depths
                ArrayList<String> depths = new ArrayList<>();
                for (int i = 0; i < depthList.length; i++) {
                    ToggleButton myButton = (ToggleButton) depthBtnLayout.findViewById(i);
                    if (myButton != null && myButton.isChecked()) {
                        depths.add(depthList[i]);
                    }
                }

                // burden
                int burden = -1;
                if (!burdenSeekBar.getTag(R.id.TAG_SEEKBAR_SELECTED).equals("false")) {
                    burden = burdenSeekBar.getProgress();
                }

                // AFTER ERROR
                // new sensation id
                int id = getNext.sensationId();

                // new color
                int color = getNext.color(colorList);

                // time
                long time = System.currentTimeMillis();


                QuestionPatient sensation = new QuestionPatient(id, color, time, sensations, custom, intensity, depths, burden);

                startNextActivity(sensation);
            }
        });

    }

    @Override
    protected void clearLayout() {

        // title
        TextView txtTitle = (TextView) findViewById(R.id.txt_select_sensation_title);
        txtTitle.setText(String.format(getString(R.string.txt_select_sensation), num+1));


        // sensations
        final FlowLayout sensationsBtnLayout = (FlowLayout) findViewById(R.id.sensationsBtnContainer);
        for (int i = 0; i < sensationsBtnLayout.getChildCount(); i++) {
            ((ToggleButton) sensationsBtnLayout.getChildAt(i)).setChecked(false);
        }

        // custom Question
        ToggleButton btnCustomSensation = (ToggleButton) findViewById(R.id.btnCustomSensation);
        btnCustomSensation.setChecked(false);
        EditText txtCustom = (EditText) findViewById(R.id.txtOtherSensation);
        txtCustom.setText("");
        txtCustom.setVisibility(View.INVISIBLE);

        // intensity
        final LinearLayout intensityContainer = (LinearLayout) findViewById(R.id.intensity_scale_container);
        for (int i = 0; i < intensityContainer.getChildCount(); i++) {
            View child = intensityContainer.getChildAt(i);
            if (child instanceof ComboSeekBar) {
                initSeekBar((ComboSeekBar) child);
            }
        }

        // depths
        final FlowLayout depthBtnLayout = (FlowLayout) findViewById(R.id.depthBtnContainer);
        for (int i = 0; i < depthBtnLayout.getChildCount(); i++) {
            ((ToggleButton) depthBtnLayout.getChildAt(i)).setChecked(false);
        }

        // burden
        final LinearLayout burdenContainer = (LinearLayout) findViewById(R.id.burden_scale_container);
        for (int i = 0; i < burdenContainer.getChildCount(); i++) {
            View child = burdenContainer.getChildAt(i);
            if (child instanceof ComboSeekBar) {
                initSeekBar((ComboSeekBar) child);
            }
        }

        // error
        TextView txtError = (TextView) findViewById(R.id.err_text);
        txtError.setVisibility(View.GONE);
    }

    @Override
    protected Class getNextActivityClass() {
        return DrawActivity.class;
    };

}*/

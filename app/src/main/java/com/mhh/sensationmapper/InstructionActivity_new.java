package com.mhh.sensationmapper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mhh.sensationmapper.config.AuthorManager;
import com.mhh.sensationmapper.config.ConfigManager;
import com.mhh.sensationmapper.config.ModeManager;
import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

import java.util.Date;
import java.util.List;
//@Lennard: minor priority: this activity should be edited, so that you it finds a file "instructions.png" in the Config folder, it will show it on the screen, before a new drawing is started.
public class InstructionActivity_new extends AppCompatActivity {
    Patient patient;
    Recording recording;
    String mode;
    String patgroup;
    ImageView instruction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction_new);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarInstructionActivity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");
        mode = intent.getStringExtra("mode");
        patgroup = intent.getStringExtra("patgroup");
        //Imageview preparation
        instruction = (ImageView) findViewById(R.id.imageView_instruction);
        if (patient.getSex() == 3) {
                instruction.setImageResource(R.drawable.instruction_symptoms_head);
            }
        else {
            instruction.setImageResource(R.drawable.instruction_symptoms);
        }
        //Button ok
        Button btnNewEntry = (Button) findViewById(R.id._instrbutton);
        btnNewEntry.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startDrawActivity(patient, recording, mode, patgroup);
            }
        });

        //Clicking on image will start drawing as well
        instruction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startDrawActivity(patient, recording, mode, patgroup);
            }
        });
    }
    private void startDrawActivity (Patient patient, Recording recording, String mode, String patgroup) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, DrawActivity.class);

        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);
        intent.putExtra("patgroup", patgroup);
        startActivity(intent);
    }
    //disable back button
    @Override
    public void onBackPressed() {

        //super.onBackPressed();

    }
}

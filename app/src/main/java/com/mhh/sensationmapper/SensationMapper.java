package com.mhh.sensationmapper;

import android.app.Application;
import android.os.Environment;

import com.mhh.sensationmapper.config.ConfigManager;

import java.io.File;
import java.util.HashMap;

/**
 * Created by sbrink on 10.07.16.
 */
public class SensationMapper extends Application {
    private DBHelper dbh;
    private ConfigManager c;
    final private HashMap<Long, Boolean> running = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();
        dbh = new DBHelper(this);
    }

    public DBHelper getDBHelper() {
        return dbh;
    }

    public ConfigManager getConfigManager() {
        return c;
    }

    public void setConfigManager(ConfigManager c) {
        this.c = c;
    }

    public void setRunning(Long rid, Boolean value) {
        running.put(rid, value);
    }

    public Boolean getRunning(Long rid) {
        return running.containsKey(rid) && running.get(rid);
    }

}

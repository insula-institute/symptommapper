package com.mhh.sensationmapper.comboseekbar;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.TypedValue;

import com.mhh.sensationmapper.comboseekbar.ComboSeekBar.Dot;

import java.util.List;

/**
 * seekbar background with text on it.
 *
 * @author sazonov-adm
 *
 */
public class CustomGradientDrawable extends CustomDrawable {
	private int[] gcolor;

	public CustomGradientDrawable(Drawable base, ComboSeekBar slider, List<Dot> dots, Boolean drawDots, int color, int[] gcolor, int textSize, boolean isMultiline) {
		super(base, slider, dots, drawDots, color, textSize, isMultiline);
		this.gcolor = gcolor;
	}

	public void draw(Canvas canvas) {

		Shader s = new LinearGradient(0f, 0f, (float) mySlider.getWidth(), (float) mySlider.getHeight(), gcolor, null, Shader.TileMode.CLAMP);
		selectLinePaint.setStrokeWidth(toPix(10));
		selectLinePaint.setShader(s);

		super.draw(canvas);
	}

}
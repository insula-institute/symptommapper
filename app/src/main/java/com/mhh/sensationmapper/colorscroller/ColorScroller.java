package com.mhh.sensationmapper.colorscroller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
//import android.support.compat.BuildConfig;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbrink on 28.03.17.
 */

public class ColorScroller extends View {

/*
    static private float[] itemLefts = {.000f, .068f, .178f, .356f, .644f, .822f, .932f};
    static private float[] itemRights = {.068f, .178f, .356f, .644f, .822f, .932f, 1.000f};
*/

    static private double g = .618f;
    private double l = 1f;
    private int offset = 0;

    public interface OnColorClickListener {
        void onColorClick(ColorScroller view, int index, int color);
    }

    //private OnColorClickListener listener = null;

    //private int width, height;

    private int mainColorIndex = 0;
    private List<Integer> colors = new ArrayList<>();
    private Paint paint = new Paint();

/*
    private String text = "";
    private TextPaint textPaint = new TextPaint();

    private Paint borderPaint = new Paint();
*/

    public ColorScroller(Context context) {
        this(context, null);
    }


    public ColorScroller(Context context, AttributeSet attrs) {
        super(context, attrs);

/*
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(40f);
*/

/*
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(6f);
        borderPaint.setColor(Color.BLACK);
*/



        //setOnTouchListener(this);
    }

/*
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width = w;
        this.height = h;
    }
*/


    private static int mix(int color) {
        return Color.rgb((Color.red(color) + 255)/2, (Color.green(color) + 255)/2, (Color.blue(color) + 255)/2);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth() - offset;
        int height = canvas.getHeight();

        // draw background (skip main color)
        double sumLeft = 0;
        double sumRight = 1;
        for (int i=1; i<colors.size(); i++) {
            int color = colors.get((mainColorIndex + i)%colors.size());
            paint.setColor(color);
            canvas.drawRect((float)(sumLeft*width/l), 0, (float)(sumRight*width/l), height, paint);
            sumLeft += Math.pow(g, i-1);
            sumRight += Math.pow(g, i);
        }

/*
        for (int i = -3; i <= 3; i++) {
            int color = colors.get((colors.size() + mainColorIndex + i)%colors.size());
            paint.setColor(color);
            canvas.drawRect(itemLefts[i+3]*width, 0, itemRights[i+3]*width, height, paint);
        }
*/

        // draw Text
/*
        int x = width / 2;
        int y = (int) ((height / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)) ;
        canvas.drawText(text, x, y, textPaint);
*/

        // draw Border around Text
/*
        canvas.drawRect(itemLefts[3]*width + 3, 3, itemRights[3]*width - 3, height - 3, borderPaint);
*/

    }

/*    // index: [-3, 3]
    private void onColorClicked(int index) {
        mainColorIndex = (colors.size() + mainColorIndex + index)%colors.size();
        if (index == 0 && listener != null) {
            listener.onColorClick(this, mainColorIndex, colors.get(mainColorIndex));
        }
        invalidate();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_UP) {
            float x = event.getX();
            int width = v.getWidth();
            int[] lt = new int[2];
            getLocationOnScreen(lt);
            float percentage = (x - lt[0])/width;
            int i = -4;
            while (i < 3 && percentage > itemLefts[i+4]) {
                //percentage -= itemWidth[i+3];
                i++;
            }
            Log.d("INDEXINDEXINDEX", String.valueOf(i));
            if (i > 3) {
                return false;
            }
            onColorClicked(i);

        }

        return true;
    }*/

/*
    public void setOnColorClickListener(OnColorClickListener listener) {
        this.listener = listener;
    }
*/

    public void setLeftOffset(int offset) {
        this.offset = offset;
    }

    public void setColors(List<Integer> colors) {
        //if (BuildConfig.DEBUG && !(colors.size() > 6)) { throw new AssertionError(); }
        this.colors = colors;
        this.l = (1-Math.pow(g, colors.size()))/(1-g);
        invalidate();
    }

/*
    public void setMainColorIndex(Integer index) {
        this.mainColorIndex = index;
        invalidate();
    }
*/

    public void shift() {
        mainColorIndex = (mainColorIndex + 1)%colors.size();
        invalidate();
    }

/*
    public void setText(String text) {
        this.text = text;
        invalidate();
    }
*/

}

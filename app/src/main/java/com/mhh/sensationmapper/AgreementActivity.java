package com.mhh.sensationmapper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mhh.sensationmapper.model.Patient;
import com.mhh.sensationmapper.model.Recording;

import java.util.ArrayList;

public class AgreementActivity extends AppCompatActivity {
    Patient patient;
    Recording recording;
    String mode;
    String patgroup;
    ArrayList questionnaires;
    Boolean agreement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAgreementActivity);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent intent = getIntent();
        patient = intent.getParcelableExtra("patient");
        recording = intent.getParcelableExtra("recording");
        mode = intent.getStringExtra("mode");
        patgroup = intent.getStringExtra("patgroup");
        questionnaires = intent.getParcelableArrayListExtra("questionnaires");
        //Button ok
        Button btnNewEntry = (Button) findViewById(R.id._instrbutton);
        final RadioGroup Radiogroup_agreement = (RadioGroup) findViewById(R.id.Radiogroup_agreement);
        final RadioButton agreement_yes = (RadioButton) findViewById(R.id.agreement_yes);
        final RadioButton agreement_no = (RadioButton) findViewById(R.id.agreement_no);

        btnNewEntry.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int QID = Radiogroup_agreement.getCheckedRadioButtonId();
                if (QID == agreement_yes.getId()) {
                    agreement = true;
                }
                else if (QID == agreement_no.getId()) {
                    agreement = false;
                }else{
                    Toast.makeText(AgreementActivity.this, R.string.err_answer_required, Toast.LENGTH_LONG).show();
                    return;
                }
                startQuestionnaireActivity(patient, recording, mode, patgroup, questionnaires, agreement);
            }
        });
    }
    private void startQuestionnaireActivity (Patient patient, Recording recording, String mode, String patgroup, ArrayList questionnaires, Boolean agreement) {
        //Class cls = (recording.getAuthor() == 0) ? StartActivityPatient.class : StartActivityDoctor.class;
        Intent intent = new Intent(this, QuestionnairePainActivity.class);

        intent.putExtra("patient", patient);
        intent.putExtra("recording", recording);
        intent.putExtra("mode", mode);
        intent.putExtra("patgroup", patgroup);
        intent.putParcelableArrayListExtra("questionnaires", questionnaires);
        intent.putExtra("agreement", agreement);
        startActivity(intent);
    }
    //disable back button
    @Override
    public void onBackPressed() {

        //super.onBackPressed();

    }
}

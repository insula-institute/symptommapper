package com.mhh.sensationmapper;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;

/**
 * Created by Tofeeq on 30/07/14.
 *
 * Brushes = region, line, point...
 */

public class Brush {

    public String name;
    public String type;
    public Paint paint;
    public boolean drawByMove;
    public Drawable icon;
    public int thickness;

    public Brush() {

    }

}

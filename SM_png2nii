#!/usr/bin/env python3

import PIL.Image
import numpy as np
import sys
import warnings  # To omit warning when loading Nifti1Image...
warnings.filterwarnings("ignore")
from nibabel import Nifti1Image as Nifti1Image
from nibabel import save as nibsave
import io, os

#-----------------------------------------------------------------------
# Information
#-----------------------------------------------------------------------
Author='Jorge Manuel'
Name='SM_png2nii'
Version='3.0 (07.02.2020)'

def Usage():
	print('Converts png to nii (SymptomMapper)')
	print('Usage:')
	print('\t${Name} <input.png> <output.nii.gz> [--V/VS]')
	print('--V')
	print('\tEncodes value and not saturation in intensity.')
	print('--VS')
	print('\tSame as --V, but additionally splits image according to values.')
	print('\t<Output> will then be treated as prefix.')
	print('\tIt will also binarise the output')
	print('Image orientation')
	print('\tImages will be stored without orientation (qform_code=0). Hence,')
	print('\torientation will be calculated according the old ANALYZE standard.')


#-----------------------------------------------------------------------
# Parse arguments
#-----------------------------------------------------------------------
Trafo = 'S'
Split = False
if len(sys.argv) > 4:
	Usage()
	exit()
elif (len(sys.argv)==1) or (sys.argv[1]=='-h') or (sys.argv[1]=='--help'):
	Usage()
	exit()
elif (len(sys.argv)==4) and (sys.argv[3]=='--V'):
	Trafo = 'V'
elif (len(sys.argv)==4) and (sys.argv[3]=='--VS'):
	Trafo = 'V'
	Split = True
Input = sys.argv[1]
Output = sys.argv[2]

#-----------------------------------------------------------------------
# Conversion to nifti
#(The intensity is encoded into the saturation with a slight correction,
#in order to make symptoms without intensity visible. This means that we
#cannot simply do Drawing_s = Drawing_S/255*100, but have to correct it 
#a little bit and then threshold everything below zero.)
#-----------------------------------------------------------------------
Drawing = PIL.Image.open(Input)
Drawing = Drawing.convert('RGBA')
_, _, _, A = Drawing.split()  # Get transparency mask
Drawing_HSV = Drawing.convert('HSV')
H, S, V = Drawing_HSV.split()

if Trafo=='S':
	Drawing_X = np.array(S)
	Drawing_x = (Drawing_X/255-0.1)/0.9*1
	Drawing_x[Drawing_x<0] = 0
elif Trafo=='V':
	Drawing_X = np.array(V)
	Drawing_x = Drawing_X/255*100
A = np.array(A)
#n = A.sum()/255
#print('Area = %.0f' %n)

Drawing_x *= np.array(A)/255  # Multiply with transparency mask
Drawing_x = np.rot90(Drawing_x,3)  # Rotate

# Convert to nifti
sform = None
if Split:
	values = np.unique(Drawing_x)
	values = np.delete(values, 0)  # Delete 0
	for i in range(len(values)):
		Drawing_y = Drawing_x.copy()
		Drawing_y[Drawing_y!=values[i]] = 0
		Drawing_y /= values[i]
		Drawing_nii = Nifti1Image(Drawing_y, sform)
		nibsave(Drawing_nii, '%s%02i.nii.gz' %(Output, i))
		os.system('${FSLDIR}/bin/fslmaths %s%02i.nii.gz %s%02i.nii.gz' %(Output, i, Output, i))  # Compress images a bit better
else:
	Drawing_nii = Nifti1Image(Drawing_x, sform)
	nibsave(Drawing_nii, Output)
	os.system('${FSLDIR}/bin/fslmaths ' + Output + ' ' + Output)
